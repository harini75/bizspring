package com.desi.bank.creditcard.service;


import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.desi.bank.creditcard.service.model.CreditCardApplicationVO;
@Service
@Transactional()
public interface CreditCardApplicationService {
	public String save(CreditCardApplicationVO entity,MultipartFile ppImage);

	public void sendApplicationConfirmationEmail(CreditCardApplicationVO creditCardApplicationVO,String appRef);

	public List<CreditCardApplicationVO> findAll();

	public CreditCardApplicationVO findByAppRef(String appRef);

	public byte[] findPassportImageByAppRef(String appRef);

	public void approveCreditCardApplication(String appRef);

	public void sendCreditCardApplicationApprovalEmail(String appRef) throws Exception;

	public void sendRejectEmail(String appRef, String rejectionMessage);

	public void rejectAppliaction(String appRef);


	
	
}
