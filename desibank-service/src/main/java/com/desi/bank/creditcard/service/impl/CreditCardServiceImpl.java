package com.desi.bank.creditcard.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desi.bank.creditcard.dao.CreditCardDaoRepository;
import com.desi.bank.creditcard.dao.entity.CreditCardEntity;
import com.desi.bank.creditcard.service.CreditCardService;
import com.desi.bank.creditcard.service.model.CreditCardVO;

import ch.qos.logback.core.joran.util.beans.BeanUtil;

@Service("CreditCardServiceImpl")
@Transactional
public class CreditCardServiceImpl implements CreditCardService{
	
	@Autowired
	private CreditCardDaoRepository creditCardDaoRepository;
	
	public CreditCardServiceImpl(){
		System.out.println("_@)@)@)@)CreditCardServiceImpl@)@)@");
		System.out.println("_@)@)@)@)CreditCardServiceImpl@)@)@"); 
			
	}

	@Override
	public CreditCardVO findById(long cardNumber) {
		Optional<CreditCardEntity> cardOptional= creditCardDaoRepository.findById(cardNumber);
		CreditCardVO creditCardVO=new CreditCardVO();
		if(cardOptional.isPresent()) {
			CreditCardEntity entity=cardOptional.get();
			BeanUtils.copyProperties(entity, creditCardVO);
		
		creditCardVO.setCardTypeName(entity.getCardType().getCardTypeName());
		if(entity.getCustomer()!=null)
		creditCardVO.setCustomerId(entity.getCustomer().getId());
		}
		return creditCardVO;
	}

	@Override
	public List<CreditCardVO> findByCustomerId(int customerId) {
		List<CreditCardEntity> cardList=creditCardDaoRepository.findByCustomerId(customerId);
		List<CreditCardVO> cardVOList=new ArrayList<>();
		for(CreditCardEntity card:cardList) {
			CreditCardVO cardVO=new CreditCardVO();
			BeanUtils.copyProperties(card, cardVO);
			cardVO.setCardTypeName(card.getCardType().getCardTypeName());
			cardVOList.add(cardVO);
		}
		return cardVOList;
	}

}
