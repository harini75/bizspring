package com.desi.bank.creditcard.service.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.multipart.MultipartFile;

public class CreditCardApplicationVO {
	private String cardType;
	private String appRef;
	private String applicationStatus;
	//personal info
	private String firstName;
	private String lastName;
	@DateTimeFormat(iso=ISO.DATE)
	private Date dob;
	private String citizenship;
	private long phoneNumber;
	private String email;
	private long ssn;
	
	private long zip;
	
//	private MultipartFile passportPhoto;
	
	
	//housing info
	private String address;
	private int monthLived;
	private String housingStatus;
	private double monthlyPayment;
	//income
	private String employmentStatus;
	private double annualIncome;
	
	private Date ApplyDate;
	
	private long cardNumber;
	

	
	
	
	
	public String getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	public Date getApplyDate() {
		return ApplyDate;
	}
	public void setApplyDate(Date applyDate) {
		ApplyDate = applyDate;
	}
	public long getZip() {
		return zip;
	}
	public void setZip(long zip) {
		this.zip = zip;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getCitizenship() {
		return citizenship;
	}
	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}
	public long getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getSsn() {
		return ssn;
	}
	public void setSsn(long ssn) {
		this.ssn = ssn;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getMonthLived() {
		return monthLived;
	}
	public void setMonthLived(int monthLived) {
		this.monthLived = monthLived;
	}
	public String getHousingStatus() {
		return housingStatus;
	}
	public void setHousingStatus(String housingStatus) {
		this.housingStatus = housingStatus;
	}
	public double getMonthlyPayment() {
		return monthlyPayment;
	}
	public void setMonthlyPayment(double monthlyPayment) {
		this.monthlyPayment = monthlyPayment;
	}
	
	public long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getEmploymentStatus() {
		return employmentStatus;
	}
	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}
	public double getAnnualIncome() { 
		return annualIncome;
	}
	public void setAnnualIncome(double annualIncome) {
		this.annualIncome = annualIncome;
	}


	
	public CreditCardApplicationVO() {
		super();
	}

	
	
	
	public String getAppRef() {
		return appRef;
	}
	public void setAppRef(String appRef) {
		this.appRef = appRef;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	@Override
	public String toString() {
		return "CreditCardApplicationVO [cardType=" + cardType + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", dob=" + dob + ", citizenship=" + citizenship + ", phoneNumber=" + phoneNumber + ", email=" + email
				+ ", ssn=" + ssn +  ", address=" + address
				+ ", monthLived=" + monthLived + ", housingStatus=" + housingStatus + ", monthlyPayment="
				+ monthlyPayment + ", employmentStatus=" + employmentStatus + ", annualIncome=" + annualIncome + "]";
	}

	
	
	
}
