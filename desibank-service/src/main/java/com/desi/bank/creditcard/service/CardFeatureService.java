package com.desi.bank.creditcard.service;

import com.desi.bank.creditcard.dao.entity.CardFeatureEntity;

public interface CardFeatureService {
	public void save(CardFeatureEntity feature);
}
