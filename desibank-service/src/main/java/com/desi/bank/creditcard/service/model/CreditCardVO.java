package com.desi.bank.creditcard.service.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.desi.bank.common.dao.entity.Customer;
import com.desi.bank.creditcard.dao.entity.CardTypeEntity;

public class CreditCardVO {
	private long cardNumber;
	
	@DateTimeFormat
	private Date expDate;
	private int customerId;
	private String cardTypeName; //PRIME, Apollo, Prime Advance etc. Many to One to cardType and cardType is many to many to features
	private String network; //Visa/Master	
	private int gracePeriodInDay;	//period that interest not counted for late payment
	private double currentBalance;	//all unpaid charges on an account, up to the date of your inquiry. 
	private double statementBalance; //amount you owe on your credit card as of the latest billing cycle
	private Date statementDueDate;	
	private double apr;	
	private double monthlyMinPayment; 	//lowest amount of money that one is required to pay on a credit card statement each month, usually 10 to 15 dollars.
	private double cashBack; //in percent
	private double floorLimit; //A floor limit is the amount of money above which credit card transactions must be authorized.
	private long creditScore; //If paid on time, score up, otherwise, score down.
							  //If higher credit score, then lower APR, otherwise high APR.
	private long rewardPoint; //Reward point can be earned when shopping on specific places etc.
	private String ccv;
	public long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}
	public Date getExpDate() {
		return expDate;
	}
	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCardTypeName() {
		return cardTypeName;
	}
	public void setCardTypeName(String cardTypeName) {
		this.cardTypeName = cardTypeName;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public int getGracePeriodInDay() {
		return gracePeriodInDay;
	}
	public void setGracePeriodInDay(int gracePeriodInDay) {
		this.gracePeriodInDay = gracePeriodInDay;
	}
	public double getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}
	public double getStatementBalance() {
		return statementBalance;
	}
	public void setStatementBalance(double statementBalance) {
		this.statementBalance = statementBalance;
	}
	public Date getStatementDueDate() {
		return statementDueDate;
	}
	public void setStatementDueDate(Date statementDueDate) {
		this.statementDueDate = statementDueDate;
	}
	public double getApr() {
		return apr;
	}
	public void setApr(double apr) {
		this.apr = apr;
	}
	public double getMonthlyMinPayment() {
		return monthlyMinPayment;
	}
	public void setMonthlyMinPayment(double monthlyMinPayment) {
		this.monthlyMinPayment = monthlyMinPayment;
	}
	public double getCashBack() {
		return cashBack;
	}
	public void setCashBack(double cashBack) {
		this.cashBack = cashBack;
	}
	public double getFloorLimit() {
		return floorLimit;
	}
	public void setFloorLimit(double floorLimit) {
		this.floorLimit = floorLimit;
	}
	public long getCreditScore() {
		return creditScore;
	}
	public void setCreditScore(long creditScore) {
		this.creditScore = creditScore;
	}
	public long getRewardPoint() {
		return rewardPoint;
	}
	public void setRewardPoint(long rewardPoint) {
		this.rewardPoint = rewardPoint;
	}
	public String getCcv() {
		return ccv;
	}
	public void setCcv(String ccv) {
		this.ccv = ccv;
	}
	public CreditCardVO() {
		super();
	}
	

}
