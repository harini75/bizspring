package com.desi.bank.creditcard.service;

import java.util.List;

import com.desi.bank.creditcard.service.model.CreditCardVO;

public interface CreditCardService {

	public CreditCardVO findById(long cardNumber);

	public List<CreditCardVO> findByCustomerId(int customerId);
}
