package com.desi.bank.creditcard.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.desi.bank.creditcard.dao.CreditCardApplicationDaoRepository;
import com.desi.bank.creditcard.dao.CreditCardDaoRepository;
import com.desi.bank.creditcard.dao.CreditCardTypeDaoRepository;
import com.desi.bank.creditcard.dao.entity.CreditCardApplicationEntity;
import com.desi.bank.creditcard.dao.entity.CreditCardEntity;
import com.desi.bank.creditcard.service.CardTypeService;
import com.desi.bank.creditcard.service.CreditCardApplicationService;
import com.desi.bank.creditcard.service.CreditCardService;
import com.desi.bank.creditcard.service.model.CreditCardApplicationVO;
import com.desi.bank.creditcard.service.model.CreditCardVO;
import com.desi.bank.employee.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
@Service
@Transactional
public class CreditCardApplicationServiceimpl implements CreditCardApplicationService {

	@Autowired
	CreditCardApplicationDaoRepository creditCardApplicationDaoRepository;
	
	@Autowired
	CreditCardDaoRepository creditCardDaoRepository;
	
    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    @Qualifier("thyemeleafEmailTemplateEngine")
    private TemplateEngine htmlTemplateEngine;
	
    @Autowired
    private  EmployeeService employeeService;
    
    @Autowired
    private CreditCardTypeDaoRepository creditCardTypeDaoRepository;
    
	@Autowired
	private CreditCardService creditCardService;
    
    
	@Override
	public String save(CreditCardApplicationVO entity,MultipartFile passportImage) {
		String appRef="AS-"+new Date().getTime();
		CreditCardApplicationEntity sentity=new CreditCardApplicationEntity();
		BeanUtils.copyProperties(entity, sentity);
		
		sentity.setAppRef(appRef);
		try {
			if(!passportImage.isEmpty())
			sentity.setPassportPhoto(passportImage.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
		sentity.setApplyDate(new Date());
		sentity.setApplicationStatus("Pending");
		creditCardApplicationDaoRepository.save(sentity);
		
		return appRef;
	}

	@Override
	@Async("asyncExecutor")
	public void sendApplicationConfirmationEmail(CreditCardApplicationVO creditCardApplicationVO,String appRef) {

		try {
			// Here write code for sending email using the template
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			InternetAddress fromAddress = new InternetAddress("synergisticit2020@gmail.com", "DesiBank PVT. LTD.");
			message.setFrom(fromAddress);
			message.setTo(creditCardApplicationVO.getEmail());
			message.setSubject("Regarding to Your Credit Card Application");
			message.setSentDate(new Date());

			// set the locale and put variable to placeholder
			final Context ctx = new Context(Locale.ENGLISH);
			ctx.setVariable("name", creditCardApplicationVO.getFirstName()+" "+creditCardApplicationVO.getLastName());
			ctx.setVariable("appNo", appRef);
			ctx.setVariable("cardType", creditCardApplicationVO.getCardType());
			ctx.setVariable("bankIcon", "bankIcon");
			ctx.setVariable("mobile", "9999897878");

			// Create the HTML body using Thymeleaf
			final String htmlContent = htmlTemplateEngine.process("html/apply-creditcard-confirmation", ctx);
			message.setText(htmlContent, true /* isHtml */);

			byte[] bytes;
			bytes = sun.misc.IOUtils.readFully(new ClassPathResource("images/bank-icon.png").getInputStream(), -1,
					true);
			InputStreamSource imageSource = new ByteArrayResource(bytes);
			message.addInline("bankIcon", imageSource, "image/png");

			mailSender.send(mimeMessage);
		} catch (Exception exe) {
			exe.printStackTrace();
		}

	}

	@Override
	public List<CreditCardApplicationVO> findAll() {
		List<CreditCardApplicationEntity> appList=creditCardApplicationDaoRepository.findAll();
		List<CreditCardApplicationVO> appVOlist=new ArrayList<>();
		appList.stream().forEach(entity->{
			CreditCardApplicationVO temp=new CreditCardApplicationVO();
			BeanUtils.copyProperties(entity, temp);
			appVOlist.add(temp);
		});
		return appVOlist;
	}

	@Override
	public CreditCardApplicationVO findByAppRef(String appRef) {
		CreditCardApplicationEntity entity=creditCardApplicationDaoRepository.findByAppRef(appRef);
		CreditCardApplicationVO sentity=new CreditCardApplicationVO();
		BeanUtils.copyProperties(entity, sentity);
		return sentity;
	}

	@Override
	public byte[] findPassportImageByAppRef(String appRef) {
		byte[]photo=creditCardApplicationDaoRepository.findPassportPhotoByAppRef(appRef);
		byte[] nphoto=new byte[0];
		
		
		if(photo!=null&&photo.length>0) {
			System.out.println("hi");
			nphoto=Arrays.copyOf(photo,photo.length);
		}
		return nphoto;
	}

	@Async(value="asyncExecutor")
	@Override
	public void sendCreditCardApplicationApprovalEmail(String appRef) throws Exception {
		CreditCardApplicationVO creditCardApplicationVO= findByAppRef(appRef);
		
		
		ObjectMapper oMapper=new ObjectMapper();
		CreditCardVO creditCardVO=creditCardService.findById(creditCardApplicationVO.getCardNumber());
			// Here write code for sending email using the template
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			InternetAddress fromAddress = new InternetAddress("synergisticit2020@gmail.com", "DesiBank PVT. LTD.");
			message.setFrom(fromAddress);
			message.setTo(creditCardApplicationVO.getEmail());
			message.setSubject("Your Credit Card Application Has Been Approved");
			message.setSentDate(new Date());

			// set the locale and put variable to placeholder
			final Context ctx = new Context(Locale.ENGLISH);
			ctx.setVariable("name", creditCardApplicationVO.getFirstName()+" "+creditCardApplicationVO.getLastName());
			ctx.setVariable("appNo", appRef);
			ctx.setVariable("cardType", creditCardApplicationVO.getCardType());
			ctx.setVariable("bankIcon", "bankIcon");
			ctx.setVariable("mobile", "9999897878");
			ctx.setVariable("creditCard", creditCardVO);
//			ctx.setVariable("bankIcon", "bankIcon");
			// Create the HTML body using Thymeleaf
			final String htmlContent = htmlTemplateEngine.process("html/apply-creditcard-approval", ctx);
			message.setText(htmlContent, true /* isHtml */);

			byte[] bytes;
			bytes = sun.misc.IOUtils.readFully(new ClassPathResource("images/bank-icon.png").getInputStream(), -1,
					true);
			InputStreamSource imageSource = new ByteArrayResource(bytes);
			message.addInline("bankIcon", imageSource, "image/png");
			
			imageSource = new ByteArrayResource(employeeService.generatedFrontCreditCardInBytes(Long.toString(creditCardApplicationVO.getCardNumber()),appRef));			
			message.addInline("cardFrontImageAttachment", imageSource, "image/png");
//			message.addAttachment(appRef+"cardFrontSide.jpg", imageSource);
			
			imageSource = new ByteArrayResource(employeeService.generateBackCreditCardInBytes(creditCardVO.getCcv()));
			message.addInline("cardBackImageAttachment", imageSource, "image/png");
//			message.addAttachment(appRef+"cardBackSide.jpg", imageSource);
			
			mailSender.send(mimeMessage);
			System.out.println("done sending email");
		
		
		
	}
	
	@Override
	public void approveCreditCardApplication(String appRef) {
		CreditCardApplicationVO application=findByAppRef(appRef);
		if(application.getCardNumber()>0)
			throw new IllegalArgumentException("credit card already created and saved for this application");

		
		CreditCardEntity entity=new CreditCardEntity();
		Long cardNumber=Long.valueOf(employeeService.generateCreditCardNumber());
		entity.setCardNumber(cardNumber);
		entity.setExpDate(generateExpDate());
		entity.setCcv(employeeService.generateCCVNumber());
		entity.setCardType(creditCardTypeDaoRepository.findByCardTypeName(application.getCardType()));
		entity.setCashBack(generateCashBack());
		entity.setApr(generateApr()); 
		entity.setCreditScore(0);
		entity.setCurrentBalance(0);
		entity.setFloorLimit(generateFloorLimit());
		entity.setGracePeriodInDay(generateGracePeriodInDay());
		entity.setMonthlyMinPayment(generateMonthlyMinPayment());
		entity.setNetwork(generateNetwork());
		entity.setRewardPoint(generateRewardPoint());
		entity.setStatementBalance(0);
		entity.setStatementDueDate(generateStatementDueDate());
		creditCardDaoRepository.save(entity);
		
		//update the application records with the newly created credit card number
//		application.setCardNumber(cardNumber);
		CreditCardApplicationEntity appEntity=creditCardApplicationDaoRepository.findByAppRef(application.getAppRef());
		appEntity.setCardNumber(cardNumber);
		appEntity.setApplicationStatus("Approved");
		
		creditCardApplicationDaoRepository.save(appEntity);
	}


	
	private Date generateStatementDueDate() {
		Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.MONTH, 1);
        date = c.getTime();
		return date;
	}

	private long generateRewardPoint() {
		return 0;
	}

	private String generateNetwork() {
		return "MASTER";
	}

	private double generateMonthlyMinPayment() {
		return 200;
	}



	private Date generateExpDate() {
		Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.YEAR, 3);
        date = c.getTime();
		return date;
	}
	
	private double generateApr() {
		return 0.15;
	}
	
	private double generateCashBack() {
		return 0.05;
	}
	/*
	 * A floor limit is the amount of money above which credit card transactions must be authorized.
	 */
	private double generateFloorLimit() {
		return 3000;
	}
	/*
	 * The period of time after any due date that the borrower has to fulfil its obligations before a default (failure to pay) is deemed to have occurred.
	 */
	private int generateGracePeriodInDay() {
		return 5;
	}

	@Override
	@Async("asyncExecutor")
	public void sendRejectEmail(String appRef,String rejectionMessage) {
		

		CreditCardApplicationVO creditCardApplicationVO=findByAppRef(appRef);
		if(creditCardApplicationVO.getEmail().length()<1)
			return;
		try {
			// Here write code for sending email using the template
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			InternetAddress fromAddress = new InternetAddress("synergisticit2020@gmail.com", "DesiBank PVT. LTD.");
			message.setFrom(fromAddress);
			message.setTo(creditCardApplicationVO.getEmail());
			message.setSubject("We Are Sorry that Your Credit Card Application is Rejected");
			message.setSentDate(new Date());

			// set the locale and put variable to placeholder
			final Context ctx = new Context(Locale.ENGLISH);
			ctx.setVariable("name", creditCardApplicationVO.getFirstName()+" "+creditCardApplicationVO.getLastName());
			ctx.setVariable("appNo", appRef);
			ctx.setVariable("cardType", creditCardApplicationVO.getCardType());
			ctx.setVariable("bankIcon", "bankIcon");
			ctx.setVariable("mobile", "9999897878");
			ctx.setVariable("rejectionMessage", rejectionMessage);
			// Create the HTML body using Thymeleaf
			final String htmlContent = htmlTemplateEngine.process("html/apply-creditcard-rejection", ctx);
			message.setText(htmlContent, true /* isHtml */);

			byte[] bytes;
			bytes = sun.misc.IOUtils.readFully(new ClassPathResource("images/bank-icon.png").getInputStream(), -1,
					true);
			InputStreamSource imageSource = new ByteArrayResource(bytes);
			message.addInline("bankIcon", imageSource, "image/png");

			mailSender.send(mimeMessage);
		} catch (Exception exe) {
			exe.printStackTrace();
		}
		
		
		
	}

	@Override
	public void rejectAppliaction(String appRef) {
		CreditCardApplicationEntity creditCardApplicationEntity=creditCardApplicationDaoRepository.findByAppRef(appRef);
		creditCardApplicationEntity.setApplicationStatus("Rejected");
		creditCardApplicationDaoRepository.save(creditCardApplicationEntity);
	}

	
	
}
