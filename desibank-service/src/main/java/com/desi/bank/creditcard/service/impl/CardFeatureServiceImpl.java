package com.desi.bank.creditcard.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desi.bank.creditcard.dao.CreditCardFeatureDaoRepository;
import com.desi.bank.creditcard.dao.entity.CardFeatureEntity;
import com.desi.bank.creditcard.service.CardFeatureService;
@Transactional
@Service
public class CardFeatureServiceImpl implements CardFeatureService {

	@Autowired
	CreditCardFeatureDaoRepository featureRepo;
	
	@Override
	public void save(CardFeatureEntity feature) {
		featureRepo.save(feature);
	}
	
}
