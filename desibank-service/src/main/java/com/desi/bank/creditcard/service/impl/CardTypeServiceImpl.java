package com.desi.bank.creditcard.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desi.bank.creditcard.dao.CreditCardTypeDaoRepository;
import com.desi.bank.creditcard.dao.entity.CardFeatureEntity;
import com.desi.bank.creditcard.dao.entity.CardTypeEntity;
import com.desi.bank.creditcard.service.CardTypeService;
import com.desi.bank.creditcard.service.model.CardFeatureVO;
import com.desi.bank.creditcard.service.model.CreditCardTypeVO;
@Transactional
@Service
public class CardTypeServiceImpl implements CardTypeService {

	@Autowired
	private CreditCardTypeDaoRepository cardTypeRepo;
	
	@Override
	public List<CardTypeEntity> findCardTypeByCategory(String category) {
		return cardTypeRepo.findCardTypeEntityByCategory(category);
	}

	@Override
	public List<CreditCardTypeVO>  findAll() {
		List<CreditCardTypeVO>  creditCardTypeVOs=new ArrayList<>();
		List<CardTypeEntity> cardTypeEntities=cardTypeRepo.findAll();
		for(CardTypeEntity entity:cardTypeEntities) {
			CreditCardTypeVO cardTypeVO=new CreditCardTypeVO();
			BeanUtils.copyProperties(entity, cardTypeVO);
			Set<CardFeatureVO> features=new LinkedHashSet<>();
			for(CardFeatureEntity fentity:entity.getFeatures()) {
				CardFeatureVO cardFeatureVO=new CardFeatureVO();
				BeanUtils.copyProperties(fentity, cardFeatureVO);
				features.add(cardFeatureVO);
			}
			cardTypeVO.setFeatures(features);
			
			creditCardTypeVOs.add(cardTypeVO);
		}
		
		return creditCardTypeVOs;
	}

	@Override
	public void save(CardTypeEntity cardTypeEntity) {
		cardTypeRepo.save(cardTypeEntity);
	}
	
	
	
}
