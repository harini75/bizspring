package com.desi.bank.creditcard.service;

import java.util.List;

import com.desi.bank.creditcard.dao.entity.CardTypeEntity;
import com.desi.bank.creditcard.service.model.CreditCardTypeVO;

public interface CardTypeService {
	List<CardTypeEntity> findCardTypeByCategory(String category);
	
	List<CreditCardTypeVO> findAll();

	void save(CardTypeEntity cardTypeEntity);
}
