package com.desi.bank.creditcard.service.model;

public class CardFeatureVO {

	private long featureId;
	
	private String feature;
	
	public long getFeatureId() {
		return featureId;
	}

	public void setFeatureId(long featureId) {
		this.featureId = featureId;
	}

	
	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	

	public CardFeatureVO() {
		super();
	}
	
	
}

