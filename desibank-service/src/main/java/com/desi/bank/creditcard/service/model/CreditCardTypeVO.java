package com.desi.bank.creditcard.service.model;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

public class CreditCardTypeVO {

	private long cardTypeId;
	
	private String cardTypeName;
	
	private String category;

	
	private Set<CardFeatureVO> features;
	
	public long getCardTypeId() {
		return cardTypeId;
	}

	public void setCardTypeId(long cardTypeId) {
		this.cardTypeId = cardTypeId;
	}

	public String getCardTypeName() {
		return cardTypeName;
	}

	public void setCardTypeName(String cardTypeName) {
		this.cardTypeName = cardTypeName;
	}

	
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	
	
	public Set<CardFeatureVO> getFeatures() {
		return features;
	}

	public void setFeatures(Set<CardFeatureVO> features) {
		this.features = features;
	}

	public CreditCardTypeVO() {
		super();
	}

	@Override
	public String toString() {
		return "CardTypeEntity [cardTypeId=" + cardTypeId + ", cardTypeName=" + cardTypeName + ", category=" + category
				+ " features=" + features + "]";
	}
	
}
