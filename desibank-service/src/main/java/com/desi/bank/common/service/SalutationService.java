package com.desi.bank.common.service;

import java.util.List;

public interface SalutationService {

	List<String> findSalutation();

}
