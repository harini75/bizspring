package com.desi.bank.common.service.impl;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;


/**
 *  once username and password are incorrect then this handler will come....................
 * @author Nagendra
 *
 */
public class LoginFailureUserAuthHandler extends SimpleUrlAuthenticationFailureHandler {

	private SpringSecurityService  springSecurityService;
	
	public LoginFailureUserAuthHandler(SpringSecurityService springSecurityService){
		 this.springSecurityService=springSecurityService;
	}
	
    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
            HttpServletResponse response, AuthenticationException exception)
            throws IOException{
    	    String message="";
    		String username=request.getParameter("username");
    		System.out.println("username = "+username);
			int userAttemptCount=-100;
			if(username!=null){
				if(springSecurityService.isUserExist(username)){
					 userAttemptCount=springSecurityService.findAttemptsCountForUser(username);
					if(userAttemptCount>0){
						userAttemptCount--;
						springSecurityService.updateLoginAttempt(username, userAttemptCount);
						message="Invalid password   and you have only "+userAttemptCount+" more attempts left";
					}
					if(userAttemptCount==0){
						springSecurityService.lockUser(username);
						message="Invalid login Attempt  and Your account has been disabled.";
					}
				}else{
					message="Invalid login Attempt . Please provide valid credentials to prevent your login from being disabled.";
				}
			}
			((HttpServletResponse)response).sendRedirect("/desibank-web/corp/auth?message="+message);
    }
}
          