package com.desi.bank.common.service;

import java.util.List;

import com.desi.bank.customer.service.model.AccountProcessingStepVO;

public interface AccountProcessingStepService {

	List<AccountProcessingStepVO> findAccountProcessingSteps();

}
