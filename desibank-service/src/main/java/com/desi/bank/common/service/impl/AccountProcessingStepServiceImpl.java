package com.desi.bank.common.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desi.bank.common.dao.entity.AccountProcessingStepEntity;
import com.desi.bank.common.dao.impl.AccountProcessingStepRepository;
import com.desi.bank.common.service.AccountProcessingStepService;
import com.desi.bank.customer.service.model.AccountProcessingStepVO;

@Service
public class AccountProcessingStepServiceImpl implements AccountProcessingStepService{
  
	@Autowired
	private AccountProcessingStepRepository accountProcessingStepRepository;
	

	@Override
	public List<AccountProcessingStepVO> findAccountProcessingSteps(){
		
		Iterable<AccountProcessingStepEntity> accountProcessingStepList=accountProcessingStepRepository.findAll();
		List<AccountProcessingStepVO> accountProcessingStepVOs=new ArrayList<>();
		for(AccountProcessingStepEntity entity:accountProcessingStepList) {
			AccountProcessingStepVO accountProcessingStepVO=new AccountProcessingStepVO();
			BeanUtils.copyProperties(entity, accountProcessingStepVO);
			accountProcessingStepVOs.add(accountProcessingStepVO);
		}
		return accountProcessingStepVOs;
	}

	
}
