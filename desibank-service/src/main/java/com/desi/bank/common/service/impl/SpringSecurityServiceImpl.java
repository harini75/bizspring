package com.desi.bank.common.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.admin.dao.impl.LoginDaoRepository;
import com.desi.bank.common.dao.entity.Login;
import com.desi.bank.util.DateUtils;

@Service("SpringSecurityServiceImpl")
@Transactional
public class SpringSecurityServiceImpl  implements SpringSecurityService {
	
	@Autowired
	private LoginDaoRepository loginDaoRepository;
	
	
	@Override
	public String updatePasswordByUserid(String userid,String newpassword){
		Optional<Login> optional=loginDaoRepository.findById(userid);
		if(optional.isPresent()) {
			Login login=optional.get();
			login.setPassword(newpassword);
			return "yes";
		}
		return "no";
	}
	
	
	@Override
	public boolean isUserExist(String username){
		Optional<Login> optional=loginDaoRepository.findById(username);
		return optional.isPresent();
	}
	
	@Override
	public void updateLoginAttempt(String username, int attemptCount) {
		Optional<Login> optional=loginDaoRepository.findById(username);
		if(optional.isPresent()){
			Login login=optional.get();
			login.setNoOfAttempt(attemptCount);
			login.setLwap(DateUtils.getCurrentDateTime());
		}
	}	
	
	@Override
	public int findAttemptsCountForUser(String username){
		Optional<Login> optional=loginDaoRepository.findById(username);
		Login login=new Login();
		if(optional.isPresent()){
			login=optional.get();
		}
		return login.getNoOfAttempt();
	}
	
	@Override
	public void lockUser(String username){
		Optional<Login> optional=loginDaoRepository.findById(username);
		if(optional.isPresent()){
			Login login=optional.get();
			login.setLocked("yes");
		}
	}

	@Override
	public String validateToken(String token) {
		Optional<Login> ologin=loginDaoRepository.findByToken(token);
		if(ologin.isPresent()) {
			return ologin.get().getLoginid();
		}
		return null;
	}

}
