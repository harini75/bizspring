package com.desi.bank.common.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desi.bank.common.dao.entity.SalutationEntity;
import com.desi.bank.common.dao.impl.SalutationRepository;
import com.desi.bank.common.service.SalutationService;

@Service
public class SalutationServiceImpl implements SalutationService{
  
	@Autowired
	private SalutationRepository salutationRepository;
	

	@Override
	public List<String> findSalutation(){
		
		Iterable<SalutationEntity> salutationEntity=salutationRepository.findAll();
		List<String> salutation=new ArrayList<>();
		for(SalutationEntity entity:salutationEntity) {
			salutation.add(entity.getName());
		}
		return salutation;
	}

	
}
