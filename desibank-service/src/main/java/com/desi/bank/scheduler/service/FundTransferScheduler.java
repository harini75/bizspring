package com.desi.bank.scheduler.service;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Service;

import com.desi.bank.springdata.customer.service.ScheduledFundTransferService;
import com.desi.bank.web.model.ScheduledFundTransferVO;

/*@Service("FundTransferScheduler")
public class FundTransferScheduler {

	@Autowired
	private ScheduledFundTransferService fundTransferService;

	private TaskScheduler scheduler;

	private class ScheduledFundTransferTask implements Runnable {

		private ScheduledFundTransferVO fundTransfer;

		public ScheduledFundTransferVO getFundTransfer() {
			return fundTransfer;
		}

		public void setFundTransfer(ScheduledFundTransferVO fundTransfer) {
			this.fundTransfer = fundTransfer;
		}

		@Override
		public void run() {
			fundTransferService.accomplishScheduledFundTransfer(fundTransfer);
		}

	}
	
	@Async
	public void scheduleTask(ScheduledFundTransferVO fundTransfer) {
		ScheduledFundTransferTask task = new ScheduledFundTransferTask();
		task.setFundTransfer(fundTransfer);

		ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
		scheduler = new ConcurrentTaskScheduler(executorService);
		scheduler.schedule(task, fundTransfer.getDate());
	}

}*/
