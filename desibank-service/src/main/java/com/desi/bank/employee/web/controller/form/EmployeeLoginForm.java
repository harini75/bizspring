package com.desi.bank.employee.web.controller.form;

public class EmployeeLoginForm {
	private String loginid;
	private String currentPassword;
	private String confirmPassword;
	private String question1;
	private String answer1;
	private String question2;
	private String answer2;


	public String getLoginid() {
		return loginid;
	}

	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getQuestion1() {
		return question1;
	}

	public void setQuestion1(String question1) {
		this.question1 = question1;
	}

	public String getAnswer1() {
		return answer1;
	}

	public void setAnswer1(String answer1) {
		this.answer1 = answer1;
	}

	public String getQuestion2() {
		return question2;
	}

	public void setQuestion2(String question2) {
		this.question2 = question2;
	}

	public String getAnswer2() {
		return answer2;
	}

	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	@Override
	public String toString() {
		return "EmployeeLoginForm [currentPassword=" + currentPassword + ", confirmPassword=" + confirmPassword
				+ ", question1=" + question1 + ", answer1=" + answer1 + ", question2=" + question2 + ", answer2="
				+ answer2 + "]";
	}

}
