package com.desi.bank.employee.service.impl;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.imageio.ImageIO;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.admin.dao.impl.AccountNumberGeneratorRepository;
import com.desi.bank.admin.dao.impl.CustomerDaoRepository;
import com.desi.bank.admin.dao.impl.LoginDaoRepository;
import com.desi.bank.common.advice.AuditTrail;
import com.desi.bank.common.dao.AppDaoConstant;
import com.desi.bank.common.dao.entity.AccountNumberGenerator;
import com.desi.bank.common.dao.entity.Customer;
import com.desi.bank.common.dao.entity.CustomerAccountInfo;
import com.desi.bank.common.dao.entity.CustomerQuestionAnswer;
import com.desi.bank.common.dao.entity.CustomerSavingEntity;
import com.desi.bank.common.dao.entity.Login;
import com.desi.bank.constant.DesiBankApplicationRole;
import com.desi.bank.constant.DesiBankConstant;
import com.desi.bank.creditcard.dao.CreditCardTypeDaoRepository;
import com.desi.bank.creditcard.dao.entity.CardTypeEntity;
import com.desi.bank.creditcard.service.CreditCardApplicationService;
import com.desi.bank.creditcard.service.CreditCardService;
import com.desi.bank.creditcard.service.model.CreditCardApplicationVO;
import com.desi.bank.creditcard.service.model.CreditCardVO;
import com.desi.bank.customer.web.controller.form.CustomerAccountInfoVO;
import com.desi.bank.customer.web.controller.form.CustomerForm;
import com.desi.bank.customer.web.controller.form.CustomerSavingForm;
import com.desi.bank.email.service.ICustomerEmailService;
import com.desi.bank.employee.dao.entity.RegistrationLinksEntity;
import com.desi.bank.employee.dao.entity.RejectSavingRequestEntity;
import com.desi.bank.employee.dao.impl.CustomerAccountInfoDaoRepository;
import com.desi.bank.employee.dao.impl.CustomerSavingDaoRepository;
import com.desi.bank.employee.dao.impl.RegistrationLinksRepository;
import com.desi.bank.employee.dao.impl.RejectSavingRequestRepository;
import com.desi.bank.employee.service.EmployeeService;
import com.desi.bank.employee.web.controller.form.CustomerAccountRegistrationVO;
import com.desi.bank.employee.web.controller.form.EmployeeLoginForm;
import com.desi.bank.employee.web.controller.form.RegistrationLinksForm;
import com.desi.bank.employee.web.controller.form.RejectSavingRequestForm;

@Service("EmployeeServiceImpl")
@Scope("singleton")
@Transactional
public class EmployeeServiceImpl  implements EmployeeService{

/*	@Autowired
	@Qualifier("EmployeeDaoImpl")
	private EmployeeDao employeeDao;*/
	
	@Autowired
	private CustomerDaoRepository customerDaoRepository;
	
	@Autowired
	private RejectSavingRequestRepository rejectSavingRequestRepository;
	
	@Autowired
	private AccountNumberGeneratorRepository accountNumberGeneratorRepository;
	
	@Autowired
	private CustomerAccountInfoDaoRepository customerAccountInfoDaoRepository;
	
	@Autowired
	private CustomerSavingDaoRepository customerSavingDaoRepository;
	
	@Autowired
	private LoginDaoRepository loginDaoRepository;
	
	@Autowired
	private RegistrationLinksRepository registrationLinksRepository;
	
	@Autowired
	private CreditCardService creditCardService;
	
	@Autowired
	private CreditCardApplicationService creditCardApplicationService;
	
	@Autowired
	@Qualifier("CustomerEmailService")
	private ICustomerEmailService customerEmailService;
	
	@Autowired
	private CreditCardTypeDaoRepository creditCardTypeDaoRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	
	@Override
	public String sendAccountSummaryEmail(CustomerAccountRegistrationVO customerAccountRegistrationVO){
		return customerEmailService.sendAccountCreationEmail(customerAccountRegistrationVO);
	}
	
	
	@Override
	public String validateEmployeeInfo(String userid,String email,String mobile) {
		StringBuilder builder=new StringBuilder();
		try {
			Customer customer=customerDaoRepository.findCustomerByLoginid(userid);
			if(customer!=null) {
				builder.append("userid is already used by someone..");
			}
			if(customerDaoRepository.findByEmail(email)!=null) {
				builder.append("<br/>Email is already in use....");
			}
			if(customerDaoRepository.findByMobile(mobile)!=null) {
				builder.append("<br/>Mobile is already in use....");
			}	
		}catch (Exception e) {
		}
		return builder.toString();
	}
	@Override
	public Optional<Login>  validateToken(String token) {
		Optional<Login> optional=loginDaoRepository.findByToken(token);
		return optional;
	}

	@Override
	public String createEmployeeLogin(EmployeeLoginForm employeeLoginForm) {
		Optional<Login> ologin=loginDaoRepository.findById(employeeLoginForm.getLoginid());
		if(ologin.isPresent()) {
			Login dlogin=ologin.get();
			dlogin.setPassword(passwordEncoder.encode(employeeLoginForm.getCurrentPassword()));	
			dlogin.setToken(null);
			List<CustomerQuestionAnswer> customerQuestionAnswers=dlogin.getCustomerQuestionAnswers();
			if(customerQuestionAnswers.size()==2) {
				customerQuestionAnswers.get(0).setQuestion(employeeLoginForm.getQuestion1());
				customerQuestionAnswers.get(0).setAnswer(employeeLoginForm.getAnswer1());
				customerQuestionAnswers.get(1).setQuestion(employeeLoginForm.getQuestion2());
				customerQuestionAnswers.get(1).setAnswer(employeeLoginForm.getAnswer2());
			}
			loginDaoRepository.save(dlogin);
		}
		return "created";
	}
	
	/**
	 *  This is should be in the transaction......................
	 */
	
	@Override
	public CustomerAccountInfoVO  createCustomerAccount(String userid) {
				CustomerAccountInfoVO customerAccountInfoVO=new CustomerAccountInfoVO();
				//CustomerAccountInfo customerAccountInfo	=employeeDao.createCustomerAccount(userid);
				//Step - Loading the customer detail
				//userid is not a primary key ..it is a candidate key!
				//loading the customer details
				Customer cutomer=customerDaoRepository.findCustomerByLoginid(userid);
				cutomer.setApproved("yes");
				//write logic to generate account number
				Optional<AccountNumberGenerator> accountNumberGeneratorOptional=accountNumberGeneratorRepository.findById(1L);
				AccountNumberGenerator accountNumberGenerator=null;
				if(accountNumberGeneratorOptional.isPresent()){
					accountNumberGenerator=accountNumberGeneratorOptional.get();	
				}
				long newAccountNumber =accountNumberGenerator.getAccountNumber()+1;
				//change the state of 
				accountNumberGenerator.setAccountNumber(newAccountNumber);
				//Creating customer account details
				CustomerAccountInfo customerAccountInfo=new CustomerAccountInfo();
				customerAccountInfo.setAccountNumber(DesiBankConstant.PREFIX_ACCOUNT_NUMBER+""+newAccountNumber);
				customerAccountInfo.setAccountType("Saving");
				customerAccountInfo.setAvBalance(10000.00F);
				customerAccountInfo.setBranch("Fremont");
				customerAccountInfo.setCurrency("$");
				customerAccountInfo.setCustomerId(userid);
				customerAccountInfo.setStatusAsOf(new Date());
				customerAccountInfo.setTavBalance(10000.00F);
				customerAccountInfoDaoRepository.save(customerAccountInfo);
				accountNumberGeneratorRepository.save(accountNumberGenerator);
				customerDaoRepository.save(cutomer);
		
		BeanUtils.copyProperties(customerAccountInfo, customerAccountInfoVO);
		//customerAccountInfoVO.setAvBalance(customerAccountInfo.getAvBalance());
		//customerAccountInfoVO.setTavBalance(customerAccountInfo.getTavBalance());
		return  customerAccountInfoVO;
	}
	
	@Override
	public String  rejectSavingAccountRequests(RejectSavingRequestForm rejectSavingRequestForm) {
		Optional<CustomerSavingEntity> ocustomerSavingEntity=customerSavingDaoRepository.findById(rejectSavingRequestForm.getCsaid() );
		CustomerSavingEntity customerSavingEntity=new CustomerSavingEntity();
		if(ocustomerSavingEntity.isPresent()){
			customerSavingEntity=ocustomerSavingEntity.get();
		}
		RejectSavingRequestEntity rejectSavingRequestEntity=new RejectSavingRequestEntity();
		rejectSavingRequestEntity.setDoa(new Date());
		rejectSavingRequestEntity.setLocation(customerSavingEntity.getLocation());
		rejectSavingRequestEntity.setMobile(customerSavingEntity.getMobile());
		customerSavingDaoRepository.delete(customerSavingEntity);
		rejectSavingRequestRepository.save(rejectSavingRequestEntity);
		return "success";
	}
	
	@Override
	@AuditTrail(action="APPROVED",entity="CUSTOMER")
	public String  savingApproveAccountRequests(RejectSavingRequestForm approvalSavingRequestForm) {
		Optional<CustomerSavingEntity> optional=customerSavingDaoRepository.findById(approvalSavingRequestForm.getCsaid());
		if(optional.isPresent()){
			CustomerSavingEntity customerSavingEntity=optional.get();
			customerSavingEntity.setStatus(AppDaoConstant.APPROVED_STATUS);
		}
		//IOException  ->> it will --- -Only for Runtime 
		return approvalSavingRequestForm.getActionBy();
		
	}
	
	@Override
	public String  saveRegistrationLink(RegistrationLinksForm registrationLinksForm) {
		RegistrationLinksEntity linksEntity=new RegistrationLinksEntity();
		BeanUtils.copyProperties(registrationLinksForm, linksEntity);
		registrationLinksRepository.save(linksEntity);
		return "success";
	}
	
	
	@Override
	public String  updateProfilePicByUserid(String userid,byte[] image) {
		 Customer entity =customerDaoRepository.findCustomerByLoginid(userid);
		 if(image!=null && image.length>10)
		 entity.setImage(image);
		 return "success";
	
	}
	
	@Override
	public String lockUnlockCustomer(String loginid, String status){
		 String resultStatus="success";
		 Optional<Login> optional=loginDaoRepository.findById(loginid);
			if(optional.isPresent()){
					Login login=optional.get();
				   if("lock".equals(status)){
					   login.setLocked("yes");
				   }else if("unlock".equals(status)){
					   login.setLocked("no");
				   }else{
						resultStatus="fail";
				   }
			}else{
				resultStatus="fail";
			}
			return resultStatus;
	} 
	
	@Override
	public List<CustomerSavingForm>  findPendingSavingAccountRequests() {

		List<CustomerSavingEntity> customerSavingEntityList =customerSavingDaoRepository.findPendingCustomerSavingEnquiry();
		//Create list with size zero and it is not null
		List<CustomerSavingForm> customerSavingFormList=new ArrayList<CustomerSavingForm>();
		if(customerSavingEntityList!=null) {
				for(CustomerSavingEntity customerSavingEntity: customerSavingEntityList){
						CustomerSavingForm customerSavingForm=new CustomerSavingForm();
						BeanUtils.copyProperties(customerSavingEntity, customerSavingForm);
						customerSavingFormList.add(customerSavingForm);
				}
		}
		return customerSavingFormList;
		
	}
	
	@Override
	public List<CustomerForm> findPendingSavingAccountApprovalRequests(){
		List<Customer> customerAccountApprovalList=customerDaoRepository.findPendingSavingAccountApprovalRequests();
		List<CustomerForm> customerFormAccountApprovalList=new ArrayList<CustomerForm>();
		if(customerAccountApprovalList!=null) {
				for(Customer customer: customerAccountApprovalList){
					CustomerForm customerSavingForm=new CustomerForm();
						BeanUtils.copyProperties(customer, customerSavingForm);
						customerSavingForm.setUserid(customer.getLogin().getLoginid());
						customerFormAccountApprovalList.add(customerSavingForm);
				}
		}
		return customerFormAccountApprovalList;
	}
	
	@Override
	public List<CustomerForm> findSavingApprovedAccount(){
		
		List<Customer>  customerApprovedAccountList=customerDaoRepository.findSavingApprovedAccount();
		 Iterator<Customer>  it=customerApprovedAccountList.iterator();
		while(it.hasNext()){
			Customer customer=it.next();
				if(DesiBankApplicationRole.CUSTOMER.getValue().equalsIgnoreCase(customer.getLogin().getRole())){
					if(customer.getLogin().getLocked().equalsIgnoreCase(AppDaoConstant.NO_STATUS)) {
							customer.setIslocked(false);
					}
					else {
						customer.setIslocked(true);
					}		
				}else{
					it.remove();
				}
		}
		
		List<CustomerForm> customerFormAppovedAccountList=new ArrayList<CustomerForm>();
		if(customerApprovedAccountList!=null) {
				for(Customer customer: customerApprovedAccountList){
					CustomerForm customerSavingForm=new CustomerForm();
						BeanUtils.copyProperties(customer, customerSavingForm);
						customerSavingForm.setUserid(customer.getLogin().getLoginid());
						customerFormAppovedAccountList.add(customerSavingForm);
						
				}
		}
		return customerFormAppovedAccountList;
	}
			

	@Override
	public int findPendingSavingAccountRequestsCount() {
		List<CustomerSavingEntity> customerSavingEntities=customerSavingDaoRepository.findPendingCustomerSavingEnquiry();
		return customerSavingEntities.size();
		
	}
	
	@Override
	public String generateCreditCardNumber() {
		Random random = new Random();

		StringBuilder number = new StringBuilder();
		number.append(String.format("%04d", random.nextInt(10000)));
		number.append(String.format("%04d", random.nextInt(10000)));
		number.append(String.format("%04d", random.nextInt(10000)));
		number.append(String.format("%04d", random.nextInt(10000)));

		return number.toString();
	}

	@Override
	public String generateCreditCardExpireDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/yy");

		LocalDate date = LocalDate.now();
		date = date.plusYears(3);

		return formatter.format(date);
	}
	
	@Override
	public String generateCCVNumber() {
		Random random = new Random();

		StringBuilder number = new StringBuilder();
		number.append(String.format("%03d", random.nextInt(1000)));

		return number.toString();
	}
	

	@Override
	public byte[] generatedCreditCardByCcid(int ccid) {
		byte[] photo = new byte[]{};

	    Optional<CardTypeEntity> cardTypeEntity=creditCardTypeDaoRepository.findById((long) ccid);
	    CardTypeEntity entity=new CardTypeEntity();
		if(cardTypeEntity.isPresent()){
			entity=cardTypeEntity.get();
			}
		
		Resource resource = new ClassPathResource("images/credit-card-front-template.jpg");
		
		try {
			InputStream resourceInputStream = resource.getInputStream();
			
			Image src = ImageIO.read(resourceInputStream);

			int wideth = src.getWidth(null);
			int height = src.getHeight(null);

			BufferedImage tag = new BufferedImage(wideth, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = tag.createGraphics();

			g.setBackground(new Color(200, 250, 200));
			g.clearRect(0, 0, wideth, height);
			g.setColor(Color.WHITE);
			g.drawImage(src, 0, 0, wideth, height, null);
			
			// credit card number
			g.setFont(new Font("Lucida Console", Font.BOLD, 36));
			g.drawString(entity.getDefaultCardNumber().substring(0, 4), 40, 207);
			g.drawString(entity.getDefaultCardNumber().substring(4, 8), 150, 207);
			g.drawString(entity.getDefaultCardNumber().substring(8, 12), 260, 207);
			g.drawString(entity.getDefaultCardNumber().substring(12, 16), 370, 207);
			
			// exp date
			g.setFont(new Font("Lucida Console", Font.PLAIN, 24));
			g.drawString(entity.getDefaultExp(), 65, 250);

			// customer name
			g.setFont(new Font("Tahoma", Font.PLAIN, 28));
			g.drawString(entity.getDefaultName().toUpperCase(), 30, 290);
			
			//cardType
			g.setFont(new Font("Lucida Console",Font.ITALIC,20));
			g.drawString(entity.getCardTypeName(), 120, 20);
			
			//load new image
			Resource simage = new ClassPathResource("images/logo.png");
			InputStream simageInputStream = simage.getInputStream();
			Image img = ImageIO.read(simageInputStream);
			//Draw image on given card
			g.drawImage(img, 304, 255, 91, 45, null);

			g.dispose();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(tag, "jpg", baos);
			baos.flush();
			photo= baos.toByteArray();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return photo;
	}

	@Override
	public String generateFrontCreditCard(String name, String number, String expireDate) {
		String base64Img = null;

		Resource resource = new ClassPathResource("images/credit-card-front-template.jpg");
		
		try {
			InputStream resourceInputStream = resource.getInputStream();
			
			Image src = ImageIO.read(resourceInputStream);

			int wideth = src.getWidth(null);
			int height = src.getHeight(null);

			BufferedImage tag = new BufferedImage(wideth, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = tag.createGraphics();

			g.setBackground(new Color(200, 250, 200));
			g.clearRect(0, 0, wideth, height);
			g.setColor(Color.WHITE);
			g.drawImage(src, 0, 0, wideth, height, null);
			
			// credit card number
			g.setFont(new Font("Lucida Console", Font.BOLD, 36));
			g.drawString(number.substring(0, 4), 40, 207);
			g.drawString(number.substring(4, 8), 150, 207);
			g.drawString(number.substring(8, 12), 260, 207);
			g.drawString(number.substring(12, 16), 370, 207);
			
			// exp date
			g.setFont(new Font("Lucida Console", Font.PLAIN, 24));
			g.drawString(expireDate, 65, 250);

			// customer name
			g.setFont(new Font("Tahoma", Font.PLAIN, 28));
			g.drawString(name.toUpperCase(), 30, 290);

			g.dispose();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(tag, "jpg", baos);
			baos.flush();

			byte[] encodedBytes = Base64.getEncoder().encode(baos.toByteArray());
			base64Img = new String(encodedBytes);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return base64Img;
	}
	
	
	
	
	
	
	@Override
	public String generateBackCreditCard(String ccv) {
		String base64Img = null;

		Resource resource = new ClassPathResource("images/credit-card-back-template.jpg");
		
		try {
			InputStream resourceInputStream = resource.getInputStream();
			
			Image src = ImageIO.read(resourceInputStream);

			int wideth = src.getWidth(null);
			int height = src.getHeight(null);

			BufferedImage tag = new BufferedImage(wideth, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = tag.createGraphics();

			g.setBackground(new Color(200, 250, 200));
			g.clearRect(0, 0, wideth, height);
			g.setColor(Color.BLACK);
			g.drawImage(src, 0, 0, wideth, height, null);
			
			g.setFont(new Font("Lucida Console", Font.BOLD, 18));
			g.drawString(ccv, 360, 135);

			g.dispose();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(tag, "jpg", baos);
			baos.flush();

			byte[] encodedBytes = Base64.getEncoder().encode(baos.toByteArray());
			base64Img = new String(encodedBytes);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return base64Img;
	}
	
	
	
	@Override
	public byte[] generateBackCreditCardInBytes(String ccv) {
		byte[] bytes=new byte[0];

		Resource resource = new ClassPathResource("images/credit-card-back-template.jpg");
		
		try {
			InputStream resourceInputStream = resource.getInputStream();
			
			Image src = ImageIO.read(resourceInputStream);

			int wideth = src.getWidth(null);
			int height = src.getHeight(null);

			BufferedImage tag = new BufferedImage(wideth, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = tag.createGraphics();

			g.setBackground(new Color(200, 250, 200));
			g.clearRect(0, 0, wideth, height);
			g.setColor(Color.BLACK);
			g.drawImage(src, 0, 0, wideth, height, null);
			
			g.setFont(new Font("Lucida Console", Font.BOLD, 18));
			g.drawString(ccv, 360, 135);

			g.dispose();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(tag, "jpg", baos);
			baos.flush();

			bytes = baos.toByteArray();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return bytes;
	}
	
	
	
	@Override
	public byte[] generatedFrontCreditCardInBytes(String ccid,String appRef) {
		byte[] photo = new byte[]{};

	    CreditCardVO creditCardVO=creditCardService.findById(Long.parseLong(ccid));
	    CreditCardApplicationVO creditCardApplicationVO=creditCardApplicationService.findByAppRef(appRef);
	    String cardNumber=Long.toString(creditCardVO.getCardNumber());
	    if(creditCardVO.getCardNumber()==0)
	    	throw new IllegalArgumentException("credit card number is not generated yet.");
	    else if(cardNumber.length()<16) {
	    	//if not 16 length, left padding with 0
	    	cardNumber=String.format("%016d", creditCardVO.getCardNumber());	
	    }
	    	

		Resource resource = new ClassPathResource("images/credit-card-front-template.jpg");
		
		try {
			InputStream resourceInputStream = resource.getInputStream();
			
			Image src = ImageIO.read(resourceInputStream);

			int wideth = src.getWidth(null);
			int height = src.getHeight(null);

			BufferedImage tag = new BufferedImage(wideth, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = tag.createGraphics();

			g.setBackground(new Color(200, 250, 200));
			g.clearRect(0, 0, wideth, height);
			g.setColor(Color.WHITE);
			g.drawImage(src, 0, 0, wideth, height, null);
			
			// credit card number
			g.setFont(new Font("Lucida Console", Font.BOLD, 36));
			g.drawString(cardNumber.substring(0, 4), 40, 207);
			g.drawString(cardNumber.substring(4, 8), 150, 207);
			g.drawString(cardNumber.substring(8, 12), 260, 207);
			g.drawString(cardNumber.substring(12, 16), 370, 207);
			
			// exp date
			g.setFont(new Font("Lucida Console", Font.PLAIN, 24));
			DateFormat dateFormat = new SimpleDateFormat("MM/yy");
			g.drawString(dateFormat.format(creditCardVO.getExpDate()), 65, 250);

			// customer name
			g.setFont(new Font("Tahoma", Font.PLAIN, 28));
			String customerName=creditCardApplicationVO.getFirstName()+" "+creditCardApplicationVO.getLastName();
			g.drawString(customerName.toUpperCase(), 30, 290);
			
			//cardType
			g.setFont(new Font("Lucida Console",Font.ITALIC,20));
			g.drawString(creditCardApplicationVO.getCardType(), 120, 20);
			
			//load new image
			Resource simage = new ClassPathResource("images/logo.png");
			InputStream simageInputStream = simage.getInputStream();
			Image img = ImageIO.read(simageInputStream);
			//Draw image on given card
			g.drawImage(img, 304, 255, 91, 45, null);

			g.dispose();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(tag, "jpg", baos);
			baos.flush();
			photo= baos.toByteArray();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return photo;
	}
	
	
}
