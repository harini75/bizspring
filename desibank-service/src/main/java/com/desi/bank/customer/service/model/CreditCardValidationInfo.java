package com.desi.bank.customer.service.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class CreditCardValidationInfo {
	private long cardNumber;
	@DateTimeFormat
	private String expDateMonth;
	private String expDateYear;
	private String ccv;
	public long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpDateMonth() {
		return expDateMonth;
	}
	public void setExpDateMonth(String expDateMonth) {
		this.expDateMonth = expDateMonth;
	}
	public String getExpDateYear() {
		return expDateYear;
	}
	public void setExpDateYear(String expDateYear) {
		this.expDateYear = expDateYear;
	}
	public String getCcv() {
		return ccv;
	}
	public void setCcv(String ccv) {
		this.ccv = ccv;
	}
	public CreditCardValidationInfo() {
		super();
	}
	@Override
	public String toString() {
		return "CreditCardValidationInfo [cardNumber=" + cardNumber + ", expDateMonth=" + expDateMonth
				+ ", expDateYear=" + expDateYear + ", ccv=" + ccv + "]";
	}
	
	
	
	
	
}
