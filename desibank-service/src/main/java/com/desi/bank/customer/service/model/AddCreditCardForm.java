package com.desi.bank.customer.service.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class AddCreditCardForm {
	private long cardNumber;
	@DateTimeFormat
	private String expDateMonth;
	private String expDateYear;
	private String ccv;
	private String otp;
	public long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpDateMonth() {
		return expDateMonth;
	}
	public void setExpDateMonth(String expDateMonth) {
		this.expDateMonth = expDateMonth;
	}
	public String getExpDateYear() {
		return expDateYear;
	}
	public void setExpDateYear(String expDateYear) {
		this.expDateYear = expDateYear;
	}
	public String getCcv() {
		return ccv;
	}
	public void setCcv(String ccv) {
		this.ccv = ccv;
	}

	
	
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}

	
	
	@Override
	public String toString() {
		return "AddCreditCardForm [cardNumber=" + cardNumber + ", expDateMonth=" + expDateMonth + ", expDateYear="
				+ expDateYear + ", ccv=" + ccv + ", otp=" + otp + "]";
	}
	public AddCreditCardForm() {
		super();
	}
	
	
	
	
	
	
	
}
