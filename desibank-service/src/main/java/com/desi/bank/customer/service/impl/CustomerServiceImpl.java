package com.desi.bank.customer.service.impl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.admin.dao.impl.CustomerDaoRepository;
import com.desi.bank.admin.dao.impl.LoginDaoRepository;
import com.desi.bank.common.advice.MessageLogger;
import com.desi.bank.common.dao.entity.Customer;
import com.desi.bank.common.dao.entity.CustomerAccountInfo;
import com.desi.bank.common.dao.entity.CustomerQuestionAnswer;
import com.desi.bank.common.dao.entity.CustomerSavingEntity;
import com.desi.bank.common.dao.entity.CustomerTransactionHistory;
import com.desi.bank.common.dao.entity.Login;
import com.desi.bank.common.dao.entity.RoleEntity;
import com.desi.bank.common.dao.entity.SecurityQuestions;
import com.desi.bank.common.dao.impl.CustomerTransactionHistoryRepository;
import com.desi.bank.common.dao.impl.RoleRepository;
import com.desi.bank.constant.DesiBankApplicationRole;
import com.desi.bank.creditcard.dao.CreditCardApplicationDaoRepository;
import com.desi.bank.creditcard.dao.CreditCardDaoRepository;
import com.desi.bank.creditcard.dao.entity.CreditCardApplicationEntity;
import com.desi.bank.creditcard.dao.entity.CreditCardEntity;
import com.desi.bank.creditcard.service.model.CreditCardVO;
import com.desi.bank.customer.dao.CustomerQuestionAnswerRepository;
import com.desi.bank.customer.dao.SecurityQuestionsRepository;
import com.desi.bank.customer.service.CustomerService;
import com.desi.bank.customer.service.model.AddCreditCardForm;
import com.desi.bank.customer.service.model.CreditCardValidationInfo;
import com.desi.bank.customer.service.model.CustomerQuestionAnswerVO;
import com.desi.bank.customer.service.model.TransactionHistoryVO;
import com.desi.bank.customer.web.controller.TransferMoneyForm;
import com.desi.bank.customer.web.controller.form.CustomerAccountInfoVO;
import com.desi.bank.customer.web.controller.form.CustomerForm;
import com.desi.bank.customer.web.controller.form.CustomerSavingForm;
import com.desi.bank.customer.web.controller.form.MiniStatementVO;
import com.desi.bank.email.service.ICustomerEmailService;
import com.desi.bank.employee.dao.entity.RegistrationLinksEntity;
import com.desi.bank.employee.dao.impl.CustomerAccountInfoDaoRepository;
import com.desi.bank.employee.dao.impl.CustomerSavingDaoRepository;
import com.desi.bank.employee.dao.impl.RegistrationLinksRepository;
import com.desi.bank.employee.web.controller.form.RegistrationLinksForm;
import com.spring.model.UserSessionVO;

@Service("CustomerServiceImpl")
@Scope("singleton")
@Transactional
public class CustomerServiceImpl implements CustomerService {

//	@Autowired
//	@Qualifier("CustomerDaoImpl")
//	private CustomerDao customerDao;
	
	@Autowired
	private CreditCardDaoRepository creditCardDaoRepository;
	
	@Autowired
	private CreditCardApplicationDaoRepository creditCardApplicationDaoRepository;
	
	@Autowired
	private CustomerTransactionHistoryRepository customerTransactionHistoryRepository;
	
	@Autowired
	private ICustomerEmailService customerEmailService;
	
	@Autowired
	private SecurityQuestionsRepository securityQuestionsRepository;
	
	@Autowired
	private CustomerSavingDaoRepository customerSavingDaoRepository;
	
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private LoginDaoRepository loginDaoRepository;
	
	@Autowired
	private CustomerDaoRepository customerDaoRepository;
	
	@Autowired
	private CustomerAccountInfoDaoRepository customerAccountInfoDaoRepository;
	
	@Autowired
	private CustomerQuestionAnswerRepository customerQuestionAnswerRepository;
	
	@Autowired
	RegistrationLinksRepository registrationLinksRepository;
	
	@Override
	public String savingAccountRequest(CustomerSavingForm customerSavingForm) {
		//
		CustomerSavingEntity customerSavingEntity = new CustomerSavingEntity();
		BeanUtils.copyProperties(customerSavingForm, customerSavingEntity);
		customerSavingDaoRepository.save(customerSavingEntity);
		return "success";
	}

	//Spring Data JPA+HIBERNATE
	@Override
	public String updatePassword(String userid, String newpassword) {
		Optional<Login> tlogin=loginDaoRepository.findById(userid);
		//List<Login> logins = (List<Login>) getHibernateTemplate().find("from Login l where l.loginid = ?", new Object[] { userid });
		if(tlogin.isPresent()){
			Login login=tlogin.get();
			//Automatically dirty checking mechanism
			//updating the password
			login.setPassword(newpassword);
			//update last login time
			login.setLlt(new Timestamp(new Date().getTime()));
			//update it again
			loginDaoRepository.save(login);
		}
		return "success";
		//return customerDao.updatePassword(userid, newpassword);
	}

	@Override
	public UserSessionVO validateCustomerByUserId(String userid) {
		UserSessionVO userSessionVO =new UserSessionVO();
		Optional<Login> tlogin=loginDaoRepository.findById(userid);
		if(tlogin.isPresent()){
			Login login=tlogin.get();
			String role="";
			Set<RoleEntity> roles=login.getRoles();
			Customer customer=customerDaoRepository.findCustomerByLoginid(login.getLoginid());
			String email=""; 
			email=customer.getEmail();
			for(RoleEntity re:roles){
				role=role+re.getName()+",";
			}
			if(role.length()>0){
				role=role.substring(0, role.length()-1);
			}
			BeanUtils.copyProperties(login, userSessionVO);
			userSessionVO.setEmail(email);
			userSessionVO.setRole(role);
		}	
		return userSessionVO;
	}

	@Override
	public List<CustomerForm> findCustomersExpirePassWithInSevenDays() {
		//TDD
		return null;
	
	}
	
	@Override
	public String updateResetPasswordToken(String email,String token) {
		Customer customer=customerDaoRepository.findByEmail(email);
		customer.getLogin().setToken(token);
		return customer.getName();
	}

	@Override
	public String persistCustomer(CustomerForm customer) {
		
			Customer pcustomer = new Customer();
			BeanUtils.copyProperties(customer, pcustomer);
			Login login = new Login();
			login.setNoOfAttempt(3);
			login.setLoginid(customer.getUserid());
			login.setName(customer.getName());
			login.setPassword(customer.getPassword());
			login.setToken(customer.getToken());
			if (customer.getRole()==null) {
				login.setRole(DesiBankApplicationRole.CUSTOMER.getValue());
				
			} else {
				login.setRole(customer.getRole());
			}	
			login.setLocked("no");
			
			CustomerQuestionAnswer questionanswer = new CustomerQuestionAnswer();
			questionanswer.setQuestion(customer.getQuestion1());
			questionanswer.setAnswer(customer.getAnswer1());
			questionanswer.setLogin(login);
			
			CustomerQuestionAnswer questionanswer2 = new CustomerQuestionAnswer();
			questionanswer2.setQuestion(customer.getQuestion2());
			questionanswer2.setAnswer(customer.getAnswer2());
			questionanswer2.setLogin(login);
			
			List<CustomerQuestionAnswer> customerQuestionAnswersList=new ArrayList<CustomerQuestionAnswer>();
			customerQuestionAnswersList.add(questionanswer);
			customerQuestionAnswersList.add(questionanswer2);
			
			login.setCustomerQuestionAnswers(customerQuestionAnswersList);
			RoleEntity entity=new RoleEntity();
			if(login.getRole().equalsIgnoreCase(DesiBankApplicationRole.CUSTOMER.getValue())) {
				entity=roleRepository.findById(3).get();
			}else {
				entity=roleRepository.findById(2).get();
			}
			Set<RoleEntity> roles=new HashSet<>();
			roles.add(entity);
			login.setRoles(roles);
			pcustomer.setLogin(login);
			customerDaoRepository.save(pcustomer);
			return "success"; // returning the control
	}

	@Override
	public List<CustomerForm> findCustomers() {
		Iterator<Customer> it = customerDaoRepository.findAll().iterator();
		List<CustomerForm> customerForms = new ArrayList<CustomerForm>();
		while(it.hasNext()){
			CustomerForm customerForm = new CustomerForm();
			Customer customer=it.next();
			BeanUtils.copyProperties(customer, customerForm);
			customerForm.setUserid(customer.getLogin().getLoginid());
			customerForms.add(customerForm);
		}
		return customerForms;
	}

	public byte[] findImageByUserid(String userid) {
		byte[] image=customerDaoRepository.findCustomerByLoginid(userid).getImage();
		if (image==null)
			image=new byte[0];
		return image;
	}

	@MessageLogger(message = "executing method validateCustomer")
	@Override
	public UserSessionVO validateCustomer(String userid, String password) {
		//How many records will come in case of user is valid... 
		Optional<Login> optional=loginDaoRepository.findByLoginidAndPassword(userid, password);
		String email="";
		if(optional.isPresent()){
			Login login=optional.get();
			Customer customer=customerDaoRepository.findCustomerByLoginid(userid);
			email=customer.getEmail();
			login.setEmail(email);
			UserSessionVO userSessionVO = new UserSessionVO();
			BeanUtils.copyProperties(login, userSessionVO);
			return userSessionVO;
		}else{
			return null;
		}
	}

	@Override
	public boolean deleteCustomer(int id) {
		//TDD
		//return customerDao.deleteCustomer(id);
		return false;
	}

	@Override
	public CustomerForm getCustomer(int id) {
		Customer customer= customerDaoRepository.findById(id).get();
		CustomerForm customerForm=new CustomerForm();
		BeanUtils.copyProperties(customer, customerForm);
		return customerForm;
	}

	@Override
	public CustomerForm getCustomer(String userid) {
		Customer customer= customerDaoRepository.findCustomerByLoginid(userid);
		CustomerForm customerForm=new CustomerForm();
		BeanUtils.copyProperties(customer, customerForm);
		customerForm.setUserid(customer.getLogin().getLoginid());
		return customerForm;
	}

	@Override
	public String updateCustomer(CustomerForm customer) {
		Customer customer2=new Customer();
		BeanUtils.copyProperties(customer, customer2);
		customerDaoRepository.save(customer2);
		return "success";
	}

	@Override
	public List<CustomerAccountInfoVO> getAccountDetails(String userid) {
		List<CustomerAccountInfo> customerAccountInfos = customerAccountInfoDaoRepository.getCustomerAccountsDetail(userid);
		List<CustomerAccountInfoVO> accountInfoVOs = new ArrayList<CustomerAccountInfoVO>();
		for (CustomerAccountInfo accountInfo : customerAccountInfos) {
			CustomerAccountInfoVO accountInfoVO = new CustomerAccountInfoVO();
			BeanUtils.copyProperties(accountInfo, accountInfoVO);
			accountInfoVOs.add(accountInfoVO);
		}
		return accountInfoVOs;
	}

	@Override
	public CustomerAccountInfoVO getAccountByAccountNumber(String accountNumber) {
		CustomerAccountInfo customerAccount =customerAccountInfoDaoRepository.findAccountByAccountNumber(accountNumber);
		CustomerAccountInfoVO accountVO = new CustomerAccountInfoVO();
		BeanUtils.copyProperties(customerAccount, accountVO);
		return accountVO;
	}

	@Override
	public List<CustomerAccountInfo> getAccount(String accountNum) {
		//return customerDao.getAccount(accountNum);
		//TDD
		return null;
	}

	@Override
	public List<MiniStatementVO> getTransactionDetails(String userid) {
		List<CustomerTransactionHistory> trans = customerTransactionHistoryRepository.findByLoginId(userid);
		List<MiniStatementVO> miniStatementVO = new ArrayList<MiniStatementVO>(
				trans.size());
		for (CustomerTransactionHistory tx : trans) {
			MiniStatementVO miniStatement = BeanUtils
					.instantiate(MiniStatementVO.class);
			BeanUtils.copyProperties(tx, miniStatement);
			miniStatementVO.add(miniStatement);
		}
		return miniStatementVO;
		
	}

	@Override
	public List<MiniStatementVO> getRecentTransactions(String userid, int maxResults) {
		
		List<CustomerTransactionHistory> tranHistory=customerTransactionHistoryRepository.findByLoginId(userid);
		List<MiniStatementVO> miniStatementVO = new ArrayList<MiniStatementVO>(tranHistory.size());
		
		for (CustomerTransactionHistory tx : tranHistory) {
			MiniStatementVO miniStatement = BeanUtils
					.instantiate(MiniStatementVO.class);
			BeanUtils.copyProperties(tx, miniStatement);
			
			// manipulation to fetch the customer id along with account number
			/*
			 * String fromAcctNum =miniStatement.getFromAccountNumber(); CustomerAccountInfo
			 * fromAccount =
			 * customerAccountInfoDaoRepository.findAccountByAccountNumber(fromAcctNum);
			 * miniStatement.setFromAccountNumber(fromAcctNum + "-" +
			 * fromAccount.getCustomerId());
			 * 
			 * String toAcctNum =miniStatement.getToAccountNumber(); CustomerAccountInfo
			 * toAccount =
			 * customerAccountInfoDaoRepository.findAccountByAccountNumber(toAcctNum);
			 * miniStatement.setToAccountNumber(toAcctNum + "-" +
			 * toAccount.getCustomerId());
			 */
			miniStatementVO.add(miniStatement);
		}
		return miniStatementVO;
	}

	@Override
	public List<MiniStatementVO> getMiniStatementByAccountNo(String accountNo, int maxResults) {
		
		/*
		 * 
		 * 	Query query = getSessionFactory().getCurrentSession().createQuery("from CustomerTransactionHistory t where t.fromAccountNumber=:accountNo or t.toAccountNumber=:accountNo order by t.date desc").setString("accountNo", accountNo);
		query.setFirstResult(0);
		query.setMaxResults(maxResults);
		
		List<CustomerTransactionHistory> trans = (List<CustomerTransactionHistory>) query.list();
		List<MiniStatementVO> miniStatementVO = new ArrayList<MiniStatementVO>(trans.size());
		
		for (CustomerTransactionHistory tx : trans) {
			MiniStatementVO miniStatement = BeanUtils
					.instantiate(MiniStatementVO.class);
			BeanUtils.copyProperties(tx, miniStatement);
			miniStatementVO.add(miniStatement);
		}
		
		return miniStatementVO;
		 */
		
		
		List<CustomerTransactionHistory> tranHistory=customerTransactionHistoryRepository.findByFromAccountNumberOrToAccountNumber(accountNo, accountNo);
		List<MiniStatementVO> miniStatementVO = new ArrayList<MiniStatementVO>(tranHistory.size());
		
		for (CustomerTransactionHistory tx : tranHistory) {
			MiniStatementVO miniStatement = BeanUtils
					.instantiate(MiniStatementVO.class);
			BeanUtils.copyProperties(tx, miniStatement);
			miniStatementVO.add(miniStatement);
		}
		return miniStatementVO;
		
		
		
		//return customerDao.getMiniStatementByAccountNo(accountNo, maxResults);
		//TDD
//		return null;

	}

	@Override
	public String persistCustomerTransaction(TransferMoneyForm transaction) {
		CustomerTransactionHistory customerTransactionHistory = new CustomerTransactionHistory();
		customerTransactionHistory.setAmount(transaction.getAmount());
		customerTransactionHistory.setDescription(transaction.getDescription());
		customerTransactionHistory.setDate(new Date());
		customerTransactionHistory.setFromAccountNumber(transaction.getFromAccount());
		customerTransactionHistory.setLoginId(transaction.getLoginid());
		customerTransactionHistory.setToAccountNumber(transaction.getSelectedPayeeName());
		customerTransactionHistory.setTransactionMode(transaction.getTransactionMode());
		customerTransactionHistoryRepository.save(customerTransactionHistory);
		return "success";
	}

	@Cacheable(value="desibank-cache")
	@Override
	public List<SecurityQuestions> securityQns() {
		System.out.println(".............SecurityQuestions is cached..........................at ." + new Date());
//		List<SecurityQuestions> sqList = (List<SecurityQuestions>) customerDao.securityQns();
		List<SecurityQuestions> sqList=securityQuestionsRepository.findByQidLessThan(6);
		return sqList;
		//TDD
	//	return null;

	}

	public List<SecurityQuestions> securityQns2() {
//		List<SecurityQuestions> sqList = (List<SecurityQuestions>) customerDao.securityQns2();
		List<SecurityQuestions> sqList=securityQuestionsRepository.findByQidGreaterThan(5);
		return sqList;
		//TDD
//		return null;
	}

	@CachePut(value = "desibank-cache", key = "#userid")
	@Override
	public CustomerForm getUserDetail(String userid) {
		//CustomerForm detail = (CustomerForm) customerDao.getCustomerDetail(userid);
		//return detail;
		//TDD
				return null;
		
	}

	@Override
	public byte[] findPhotoById(int id) {
		// TODO Auto-generated method stub
		byte[] photo = customerDaoRepository.findById(id).get().getImage();
		return photo;
	}

	@Override
	public List<CustomerQuestionAnswerVO> getSecurityQn(String loginid) {
		List<CustomerQuestionAnswerVO> customerQuestionAnswerVOs=new ArrayList<>();
		List<CustomerQuestionAnswer> customerQuestionAnswers=customerQuestionAnswerRepository.findCustomerQuestionsByUserid(loginid);
		customerQuestionAnswers.forEach(cqa->{
			CustomerQuestionAnswerVO customerQuestionAnswerVO=new CustomerQuestionAnswerVO();	
			BeanUtils.copyProperties(cqa, customerQuestionAnswerVO);
			customerQuestionAnswerVOs.add(customerQuestionAnswerVO);
		});
		return customerQuestionAnswerVOs;	
	}

	@Override
	public String checkPassword(String userid, String password, String qn1, String qn2, String ans1, String ans2) {
		/*String updatePass = (String) customerDao.updatePass(userid, password, qn1, qn2, ans1, ans2);
		return updatePass;*/
		//TDD
		return null;
	}

	// concurrent
	/// userid - return value
	@CachePut(value = "desibank-cache")
	@Override
	public String findEmailByUserid(String userid) {
		System.out.println("_#_##))#(#(#(#(#(#(#(#(##*");
		String email = customerDaoRepository.findCustomerByLoginid(userid).getEmail();
		return email;
	}

	@Override
	public RegistrationLinksForm findLinkDetailByCuid(String cuid) {
		Optional<RegistrationLinksEntity> optional=registrationLinksRepository.findByLinkurl(cuid);
		RegistrationLinksEntity linksEntity=new RegistrationLinksEntity();
		if(optional.isPresent()){
			linksEntity=optional.get();
		}
		RegistrationLinksForm registrationLinksForm = new RegistrationLinksForm();
		BeanUtils.copyProperties(linksEntity, registrationLinksForm);
		return registrationLinksForm;
	}

	@Override
	public CustomerSavingForm findCustomerSavingEnquiryByEmail(String email) {
		
		Optional<CustomerSavingEntity> optional=customerSavingDaoRepository.findByEmail(email);
		CustomerSavingEntity linksEntity=new CustomerSavingEntity();
		if(optional.isPresent()){
			 linksEntity=optional.get();
		}
		CustomerSavingForm customerSavingForm = new CustomerSavingForm();
		BeanUtils.copyProperties(linksEntity, customerSavingForm);
		return customerSavingForm;
	}

	@Override
	public CustomerSavingForm findCustomerSavingEnquiryByAppRef(String appRef) {
		Optional<CustomerSavingEntity> optional=customerSavingDaoRepository.findByAppref(appRef);
		CustomerSavingEntity linksEntity=new CustomerSavingEntity();
		if(optional.isPresent()){
			 linksEntity=optional.get();
		}
		CustomerSavingForm customerSavingForm = new CustomerSavingForm();
		BeanUtils.copyProperties(linksEntity, customerSavingForm);
		return customerSavingForm;
	}

	@Override
	public List<CustomerTransactionHistory> scheduledTransfer() {
		/*return customerDao.scheduledTransfer();
*/
		//TDD
				return null;
	}

	@Override
	public String scheduledCustomerTransaction(CustomerTransactionHistory transaction) {
		/*return customerDao.scheduledCustomerTransaction(transaction);*/
		//TDD
				return null;
	}

	@Override
	public void updateScheduledTransaction(CustomerTransactionHistory transactionhistory) {
	/*	customerDao.updateScheduledTransaction(transactionhistory);*/
		//TDD
		//return null;
	}

	@Override
	public Long countTransactionsByAccountNumber(String accountNumber) {
		int transactionsCount=customerTransactionHistoryRepository.findByFromAccountNumber(accountNumber).size();
		return (long) transactionsCount;
	}

	@Override
	public List<TransactionHistoryVO> traverseTransactionsHistoryByAccountNumber(String accountNumber, int limit,
			int offset, Date fromDate, Date toDate) {
		/*List<CustomerTransactionHistory> transactionHistories = customerDao
				.traverseTransactionsHistoryByAccountNumber(accountNumber, limit, offset, fromDate, toDate);
		
		List<TransactionHistoryVO> historyVOs = new ArrayList<TransactionHistoryVO>();
		
		for (CustomerTransactionHistory transaction : transactionHistories) {
			TransactionHistoryVO transactionVO = new TransactionHistoryVO();
			BeanUtils.copyProperties(transaction, transactionVO);
			historyVOs.add(transactionVO);
		}

		return historyVOs;*/
			//TDD
			return null;
	}

	@Override
	public Long countTraverseTransactionsByAccountNumber(String accountNumber, Date fromDate, Date toDate) {
		/*return customerDao.countTraverseTransactionsByAccountNumber(accountNumber, fromDate, toDate);*/
		//TDD
				return null;
	}

	

	@Override
	public boolean validateCreditCard(CreditCardValidationInfo creditCardValidationInfo) {
		long cardNumber=creditCardValidationInfo.getCardNumber();
		Optional<CreditCardEntity> cardOptional=creditCardDaoRepository.findById(cardNumber);
		
		

		if(!cardOptional.isPresent())
			return false;
		else {
			CreditCardEntity cardEntity=cardOptional.get();
			//check whether the card has been associated to a customer
			if(cardEntity.getCustomer()!=null)
				return false;
			
			
			String pattern = "yy";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			String dateYear = simpleDateFormat.format(cardEntity.getExpDate());
			
			pattern="MM";
			simpleDateFormat = new SimpleDateFormat(pattern);
			String dateMonth=simpleDateFormat.format(cardEntity.getExpDate());
			
			//check for ccv, expDate
			if(!cardEntity.getCcv().equals(creditCardValidationInfo.getCcv()))
				return false;
			else if(!dateYear.equals(creditCardValidationInfo.getExpDateYear()))
				return false;
			else if (!dateMonth.equals(creditCardValidationInfo.getExpDateMonth()))
				return false;
			
			return true;
		}
	}
	@Override
	public void sendCreditCardOTP(long cardNumber) {
		CreditCardApplicationEntity creditCardApplicationEntity=creditCardApplicationDaoRepository.findByCardNumber(cardNumber);
		String email=creditCardApplicationDaoRepository.findEmailByCardNumber(cardNumber);
		String otp=String.format("%04d",(int)(Math.random()*1000));
		
		//otp expire in one hour
		Date expDate=new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(expDate);
		calendar.add(Calendar.HOUR, 1);
		expDate=calendar.getTime();
		//save otp and otp expdate on database
		creditCardApplicationEntity.setOtp(otp);
		creditCardApplicationEntity.setOtpExp(expDate);
		
		if(email!=null)
			customerEmailService.sendCreditCardOTPEmail(email, otp);
		
	}

	@Override
	public boolean validateAddCreditCardOtp(AddCreditCardForm addCreditCardForm) {
		CreditCardApplicationEntity entity=creditCardApplicationDaoRepository.findByCardNumber(addCreditCardForm.getCardNumber());
		if(entity.getOtp()==null||entity.getOtpExp()==null)
			return false;
		else if(entity.getOtpExp().before(new Date()))
			return false;
		else if(Long.parseLong(entity.getOtp())==Long.parseLong(addCreditCardForm.getOtp()))
			return true;
		else 
			return false;
	}

	@Override
	public void associateCreditCard(AddCreditCardForm addCreditCardForm, String username) {
		System.out.println(username);
		Login l=loginDaoRepository.findById(username).get();
		System.out.println(l);
		Customer customer=customerDaoRepository.findCustomerByLoginid(username);
		CreditCardEntity creditCardEntity=creditCardDaoRepository.findById(addCreditCardForm.getCardNumber()).get();
		creditCardEntity.setCustomer(customer);
		creditCardDaoRepository.save(creditCardEntity);
	}

	@Override
	public List<CreditCardVO> findCreditCardByCustomerName(String username) {
		Customer customer=customerDaoRepository.findCustomerByLoginid(username);
		Set<CreditCardEntity> creditCardList=customer.getCreditCards();
		List<CreditCardVO> creditCardVOList=new ArrayList<>();
		for(CreditCardEntity cardEntity:creditCardList) {
			CreditCardVO creditCardVO=new CreditCardVO();
			BeanUtils.copyProperties(cardEntity, creditCardVO);
			creditCardVOList.add(creditCardVO);
		}
			
		
		return creditCardVOList;
	}
	
	
	
}
