package com.desi.bank.customer.service.model;

import java.sql.Timestamp;

public class AccountProcessingStepVO {
	private int sid;
	private String steps;
    private Timestamp doe;
    private Timestamp dom;
	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}
	public String getSteps() {
		return steps;
	}
	public void setSteps(String steps) {
		this.steps = steps;
	}
	public Timestamp getDoe() {
		return doe;
	}
	public void setDoe(Timestamp doe) {
		this.doe = doe;
	}
	public Timestamp getDom() {
		return dom;
	}
	public void setDom(Timestamp dom) {
		this.dom = dom;
	}
    
    
}
