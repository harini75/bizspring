package com.desi.bank.customer.report.model;

import java.util.List;

import com.desi.bank.customer.service.model.TransactionHistoryVO;
import com.desi.bank.customer.web.controller.form.CustomerAccountInfoVO;

public class BankStatement {
	private CustomerAccountInfoVO account;
	private List<TransactionHistoryVO> transactions;

	public CustomerAccountInfoVO getAccount() {
		return account;
	}

	public void setAccount(CustomerAccountInfoVO account) {
		this.account = account;
	}

	public List<TransactionHistoryVO> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionHistoryVO> transactions) {
		this.transactions = transactions;
	}

}
