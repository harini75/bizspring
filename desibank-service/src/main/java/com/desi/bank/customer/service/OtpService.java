package com.desi.bank.customer.service;

public interface OtpService {

	int generateOTP(String key);
	int getOtp(String key);
	void clearOTP(String key);

}
