package com.desi.bank.web.model;

import java.util.Date;

public class ScheduledFundTransferVO {

	public static String PENDING = "PENDING";
	public static String TRANSFERRED = "TRANSFERRED";
	
	private Long cid;
	private String userId;
	private Date date;
	private String description;
	private float amount;
	private String fromAccountNumber;
	private String toAccountNumber;
	private String status;

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getFromAccountNumber() {
		return fromAccountNumber;
	}

	public void setFromAccountNumber(String fromAccountNumber) {
		this.fromAccountNumber = fromAccountNumber;
	}

	public String getToAccountNumber() {
		return toAccountNumber;
	}

	public void setToAccountNumber(String toAccountNumber) {
		this.toAccountNumber = toAccountNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ScheduledFundTransferVO [cid=" + cid + ", userId=" + userId + ", date=" + date + ", description="
				+ description + ", amount=" + amount + ", fromAccountNumber=" + fromAccountNumber + ", toAccountNumber="
				+ toAccountNumber + ", status=" + status + "]";
	}

}
