package com.desi.bank.web.model;

public class LocationVO {

	private Long cid;
	private String name;

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Location [cid=" + cid + ", name=" + name + "]";
	}

}
