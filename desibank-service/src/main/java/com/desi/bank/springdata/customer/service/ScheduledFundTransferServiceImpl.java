/*package com.desi.bank.springdata.customer.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.desi.bank.customer.service.CustomerService;
import com.desi.bank.customer.web.controller.TransferMoneyForm;
import com.desi.bank.scheduler.service.FundTransferScheduler;
import com.desi.bank.springdata.dao.entity.ScheduledFundTransfer;
import com.desi.bank.springdata.dao.repository.ScheduledFundTransferDaoRepository;
import com.desi.bank.web.model.ScheduledFundTransferVO;

@Service("ScheduledFundTransferServiceImpl")
public class ScheduledFundTransferServiceImpl implements ScheduledFundTransferService {

	private static final Log logger = LogFactory.getLog(ScheduledFundTransferServiceImpl.class);
	
	@Autowired
	@Qualifier("ScheduledFundTransferDaoRepository")
	private ScheduledFundTransferDaoRepository scheduledFundTransferDao;

	@Autowired
	@Qualifier("FundTransferScheduler")
	private FundTransferScheduler fundTransferScheduler;

	@Autowired
	@Qualifier("CustomerServiceImpl")
	public CustomerService customerService;

	@Transactional(value="jpaTransactionManager", propagation=Propagation.REQUIRED)
	@Override
	public void scheduleFundTransfer(ScheduledFundTransferVO fundTransferVO) {
		ScheduledFundTransfer fundTransfer = new ScheduledFundTransfer();
		BeanUtils.copyProperties(fundTransferVO, fundTransfer);
		fundTransfer = scheduledFundTransferDao.save(fundTransfer);
		
		fundTransferVO.setCid(fundTransfer.getCid());

		fundTransferScheduler.scheduleTask(fundTransferVO);
	}

	@Transactional(value="jpaTransactionManager", propagation=Propagation.REQUIRED)
	@Override
	public void accomplishScheduledFundTransfer(ScheduledFundTransferVO fundTransferVO) {
		fundTransferVO.setStatus(ScheduledFundTransferVO.TRANSFERRED);

		ScheduledFundTransfer fundTransfer = new ScheduledFundTransfer();
		BeanUtils.copyProperties(fundTransferVO, fundTransfer);
		scheduledFundTransferDao.save(fundTransfer);

		TransferMoneyForm transaction = new TransferMoneyForm();
		transaction.setAmount(fundTransferVO.getAmount());
		transaction.setDescription(fundTransferVO.getDescription());
		transaction.setFromAccount(fundTransferVO.getFromAccountNumber());
		transaction.setSelectedPayeeName(fundTransferVO.getToAccountNumber());
		transaction.setLoginid(fundTransferVO.getUserId());
		transaction.setTransactionMode("transferred");

		customerService.persistCustomerTransaction(transaction);
	}

	@Transactional(value="jpaTransactionManager", propagation=Propagation.REQUIRED)
	@Override
	public void schedulePendingFundTransfers() {
		List<ScheduledFundTransfer> fundTransfers = scheduledFundTransferDao
				.findByStatus(ScheduledFundTransferVO.PENDING);

		for (ScheduledFundTransfer fundTransfer : fundTransfers) {
			ScheduledFundTransferVO fundTransferVO = new ScheduledFundTransferVO();
			BeanUtils.copyProperties(fundTransfer, fundTransferVO);
			
			// TODO: check a situation when server is down, and some records have scheduled date during the
			// downtime. Maybe, we have to set the new schedule date to new Date()
			
			fundTransferScheduler.scheduleTask(fundTransferVO);
		}
	}
}
*/