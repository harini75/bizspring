package com.desi.bank.springdata.common.service;

import java.util.List;

import com.desi.bank.web.model.LocationVO;

public interface LocationService {

	List<LocationVO> findAll();
	
}
