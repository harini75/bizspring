package com.desi.bank.springdata.common.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.springdata.dao.entity.Location;
import com.desi.bank.springdata.dao.repository.LocationDaoRepository;
import com.desi.bank.web.model.LocationVO;

/**
 * 
 * @author Nagendra
 *
 */
@Transactional
@Service("LocationServiceImpl")
public class LocationServiceImpl implements LocationService {

	@Autowired
	@Qualifier("LocationDaoRepository")
	private LocationDaoRepository locationDao;

	@Override
	public List<LocationVO> findAll() {
		List<Location> list = locationDao.findAll();
		List<LocationVO> locations = new ArrayList<LocationVO>();
		for (Location l : list) {
			LocationVO location = new LocationVO();
			BeanUtils.copyProperties(l, location);
			locations.add(location);
		}
		return locations;
	}

}
