package com.desi.bank.springdata.customer.service;

import java.util.List;

import com.desi.bank.web.model.ScheduledFundTransferVO;

public interface ScheduledFundTransferService {

	void scheduleFundTransfer(ScheduledFundTransferVO fundTransferVO);

	void accomplishScheduledFundTransfer(ScheduledFundTransferVO fundTransferVO);

	void schedulePendingFundTransfers();

}
