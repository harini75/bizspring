package com.desi.bank.admin.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.admin.dao.impl.AccountNumberGeneratorRepository;
import com.desi.bank.admin.dao.impl.CustomerDaoRepository;
import com.desi.bank.admin.dao.impl.LoginDaoRepository;
import com.desi.bank.admin.dao.impl.TransactionIdGeneratorRepository;
import com.desi.bank.admin.service.AdminService;
import com.desi.bank.common.dao.entity.AccountNumberGenerator;
import com.desi.bank.common.dao.entity.Customer;
import com.desi.bank.common.dao.entity.Login;
import com.desi.bank.common.dao.entity.RoleEntity;
import com.desi.bank.common.dao.entity.TransactionIdGenerator;
import com.desi.bank.common.dao.impl.RoleRepository;
import com.desi.bank.constant.DesiBankConstant;
import com.desi.bank.customer.web.controller.form.CustomerForm;

@Service
@Transactional(propagation=Propagation.REQUIRED)
public class AdminServiceImpl implements AdminService {

/*	@Autowired
	private AdminDao adminDao;*/
	
	public AdminServiceImpl(){
		System.out.println("_____________AdminServiceImpl______________");
		System.out.println("_____________SpringBoot______________");
		System.out.println("_____________AdminServiceImpl______________");
	}
	
	@Autowired
	private LoginDaoRepository loginDaoRepository;
	
	@Autowired
	private CustomerDaoRepository customerDaoRepository;
	
	@Autowired
	AccountNumberGeneratorRepository accountNumberGeneratorRepository;
	
	@Autowired
	private TransactionIdGeneratorRepository transactionIdGeneratorRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Override
	public String updateRole(String loginid,List<Integer> roleids) {
		Optional<Login> optional=loginDaoRepository.findById(loginid);
		if(optional.isPresent()){
			Login login=optional.get();
			Set<RoleEntity> roles=login.getRoles();
			Set<RoleEntity> newroles=new HashSet<>();
			for(Integer rid:roleids) {
				RoleEntity entity=roleRepository.findById(rid).get();
				newroles.add(entity);
			}
			login.setRoles(newroles);
		}
		return "success";
	}
	
	@Override
	public String updateLockStatus(String userid,String status){
		Optional<Login> optional=loginDaoRepository.findById(userid);
		if(optional.isPresent()){
			Login login=optional.get();
			login.setLocked(status);
		}
		return "success";
	}
	

	@Cacheable(value="desibank-cache")
	@Override
	public List<CustomerForm> findCustomersByRole(String roleName) {
		System.out.println("data is loaded from the caching by cachemanager");
		Iterable<Customer> customers=customerDaoRepository.findAll();
		Iterator<Customer> iterator=customers.iterator();
		List<CustomerForm> customerForms=new ArrayList<CustomerForm>();
		while(iterator.hasNext()) {
			Customer customer=iterator.next();
			RoleEntity roleEntity=new RoleEntity();
			roleEntity.setRid(Integer.parseInt(roleName));
			if(customer.getLogin().getRoles().contains(roleEntity)) {
				CustomerForm customerForm=new CustomerForm();
				BeanUtils.copyProperties(customer, customerForm);
				customerForm.setRoleids(getCustomerRoles(customer.getLogin().getRoles()));
				customerForm.setUserid(customer.getLogin().getLoginid());
				if(customer.getLogin().getLocked().equalsIgnoreCase("yes")){
					customerForm.setLocked("yes");
				}else{
					customerForm.setLocked("no");
				}
				customerForms.add(customerForm);
			}
		}
		return customerForms;
	}
	
	private List<Integer> getCustomerRoles(Set<RoleEntity> roleEntities){
		List<Integer> integers=new ArrayList<>();
		for(RoleEntity entity:roleEntities) {
			integers.add(entity.getRid());
		}
		return integers;
	}
	
	@Override
	public List<CustomerForm> listUnapprovedCustomers() {
		List<Customer> customers=customerDaoRepository.listUnapprovedCustomers();
		List<CustomerForm> customerForms=new ArrayList<CustomerForm>(customers.size());
		for(Customer customer:customers) {
			CustomerForm customerForm=new CustomerForm();
			BeanUtils.copyProperties(customer, customerForm);
			customerForms.add(customerForm);
		}
		return customerForms;
	}

	@Override
	public String approveCustomers(String[] id) {
		return null;
		//PKK
		//return adminDao.approveCustomers(id);
	}
	
	@Override
	public boolean addDefaultAccountNumber(long id){
		Optional<AccountNumberGenerator> oaccountNumberGenerator=accountNumberGeneratorRepository.findById(id);
		if(!oaccountNumberGenerator.isPresent()){
			AccountNumberGenerator accountNumberGen=new AccountNumberGenerator();
			accountNumberGen.setAccountNumber(DesiBankConstant.DEFAULT_ACCOUNT_NUMBER);
			accountNumberGeneratorRepository.save(accountNumberGen);
			return true;
		}
		return false;
	}

	@Override
	public String lockCustomers(String[] id) {
				//return adminDao.lockCustomers(id);
				for(String x:id) {
					   if(x==null)
						  continue;
					   System.out.println("id " +x);
					   int uid = Integer.parseInt(x) ;
					   Optional<Customer> ocustomer=customerDaoRepository.findById(uid) ;
					 if(ocustomer.isPresent()){
						 Optional<Login> ologin=loginDaoRepository.findById(ocustomer.get().getLogin().getLoginid());
						 Login login=null;
						 if(ologin.isPresent()){
							 	login=ologin.get();
						 }
						 login.setLocked("yes");
						 //getHibernateTemplate().update(login);
					 }
					  
				}	
				return "success";
	}

	@Override
	public List<CustomerForm> listUnlockedCustomers() {
		List<Login> unlockCust = loginDaoRepository.listUnlockedCustomers();
		System.out.println(unlockCust);
		List<CustomerForm> customerForms=new ArrayList<CustomerForm>();
		for (Login ln : unlockCust) {
				Customer customer=customerDaoRepository.findCustomerByLoginid(ln.getLoginid());
				CustomerForm customerForm=new CustomerForm();
				BeanUtils.copyProperties(customer, customerForm);
				customerForms.add(customerForm);
		}
		return customerForms;
	}

	@Override
	public String unlockCustomers(String[] id) {
		Customer pcust = new Customer();
		Login login = new Login();
		for (String x : id) {
			if (x == null)
				continue;
			System.out.println("id " + x);
			int uid = Integer.parseInt(x);
			Optional<Customer> optional = customerDaoRepository.findById(uid);
			if (optional.isPresent()) {
				pcust = optional.get();
			}

			Optional<Login> loptional = loginDaoRepository.findById(pcust.getLogin().getLoginid());
			if (loptional.isPresent()) {
				login = loptional.get();
			}
			login.setLocked("no");
			login.setNoOfAttempt(0);
			loginDaoRepository.save(login);

		}
		return "success";
	}
	
	@Override
	public String unlockCustomer(String loginid) {
		Optional<Login> loptional = loginDaoRepository.findById(loginid);
		Login login = null;
		if (loptional.isPresent()) {
			login = loptional.get();
			login.setLocked("no");
			login.setNoOfAttempt(0);
			loginDaoRepository.save(login);
		} else {
			return "fail";
		}
		return "success";
	}

	@Override
	public List<CustomerForm> listlockedCustomers() {
		//return adminDao.listLockedCustomers();
		//PKK
		return null;
	}

	@Override
	public List<CustomerForm> showCustomers() {
		//PKK
		//return adminDao.showCustomers();
		return null;
	}

	@Override
	public String changePasword(String userid, String password) {
		Optional<Login> loptional = loginDaoRepository.findById(userid);
		Login login = null;
		if (loptional.isPresent()) {
			login=loptional.get();
			login.setPassword(password);
			loginDaoRepository.save(login);
		}
		return "success";
	}

	@Override
	public boolean addDefaultTransactionID(long id) {
		Optional<TransactionIdGenerator> loptional = transactionIdGeneratorRepository.findById(id);
		TransactionIdGenerator transactionIdGenerator = new TransactionIdGenerator();
		if (loptional.isPresent()) {
			transactionIdGenerator=loptional.get();
			transactionIdGenerator.setTransactionId(DesiBankConstant.DEFAULT_TX_ID);
			transactionIdGeneratorRepository.save(transactionIdGenerator);
			return true;
		}
		return false;
	}

	@Override
	public List<CustomerForm> listPaginatedCustomers(int page) {
		//return adminDao.listPaginatedCustomers(page);
		//PKK
		return null;
	}

	@Override
	public List<CustomerForm> searchUnapprovedCustomers(String keyword) {
		//return adminDao.searchUnapprovedCustomers(keyword);
		//PKK
				return null;
	}

}
