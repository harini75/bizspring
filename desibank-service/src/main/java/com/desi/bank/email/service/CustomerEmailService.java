package com.desi.bank.email.service;

import java.util.Date;
import java.util.Locale;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.desi.bank.employee.web.controller.form.CustomerAccountRegistrationVO;

@Service("CustomerEmailService")
@Scope("singleton")
public class CustomerEmailService  implements ICustomerEmailService{
	


    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    @Qualifier("thyemeleafEmailTemplateEngine")
    private TemplateEngine htmlTemplateEngine;
    
    private final static String SAVING_ACCOUNT_CONFIRMATION_TEMPLATE="html/saving-account-confirmation";


	
	@Value("${app.contextpath}")
	private String appContextpath;
	
	@Async("asyncExecutor")
	@Override
	public String sendAccountCreationEmail(CustomerAccountRegistrationVO customerAccountRegistrationVO) {
		 //Here write code for sending email using the template
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		
		  try {
			  	MimeMessageHelper message=new MimeMessageHelper(mimeMessage,true,"UTF-8");
			    InternetAddress fromAddress = new InternetAddress("synergisticit2020@gmail.com", "DesiBank PVT. LTD.");
				message.setFrom(fromAddress);
				// message.setSender(new InternetAddress(from));
				
				message.setTo(customerAccountRegistrationVO.getTo());
//				message.setTo(Message.RecipientType.TO,InternetAddress.parse());
				message.setSubject(customerAccountRegistrationVO.getSubject());
				message.setSentDate(new Date());
				//compose the content of the email
				
				
				//set the locale and put variable to placeholder
			    final Context ctx = new Context(Locale.ENGLISH);
			    ctx.setVariable("account", customerAccountRegistrationVO);

				
		        final String htmlContent = htmlTemplateEngine.process("html/customer-account-creation-summary", ctx);
		        message.setText(htmlContent, true /* isHtml */);
		        
			    // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
		        byte[] bytes;
		        bytes = sun.misc.IOUtils.readFully(new ClassPathResource("images/thanks.png").getInputStream(), -1, true);
			    InputStreamSource imageSource =new ByteArrayResource(bytes);
			    message.addInline("bankIcon", imageSource, "image/png");

				mailSender.send(mimeMessage);
			    
				
				// This mail has 2 part, the BODY and the embedded image
				//multipart is a body of the email
//				MimeMultipart multipart = new MimeMultipart("related");
				
				// first part (the html)
				//creating first of of the body of the email
//				BodyPart messageBodyPart = new MimeBodyPart();
				//loading vm template
//				Template template = velocityEngine.getTemplate("./templates/customer-account-creation-summary.vm");
				
//				//below is used to send data from java to vm template
//				VelocityContext velocityContext = new VelocityContext();
//				velocityContext.put("account", customerAccountRegistrationVO);
//				//now merge velocityeContext & VM
//				StringWriter stringWriter = new StringWriter();
//				template.merge(velocityContext, stringWriter);
//				System.out.println(" :-"+stringWriter.toString());
//		        messageBodyPart.setContent(stringWriter.toString(), "text/html");
//		        multipart.addBodyPart(messageBodyPart);
//		        
//		        //Now creating another BodyPart for img
//		        messageBodyPart = new MimeBodyPart();
//		        String imageURL=appContextpath+"/images/thanks.png";
//		         messageBodyPart.setDataHandler(new DataHandler(new URL(imageURL)));
//		         messageBodyPart.setHeader("Content-ID", "<thanks>");
//		         multipart.addBodyPart(messageBodyPart);
//		         
//				// put everything together
//				message.setContent(multipart);
//				mailSender.send(message);
			
		  } catch (Exception e) {
			  e.printStackTrace();
		  }
		 return "success";
    }
	
	private final static String CUSTOMER_EMAIL_MESSAGE_TEMPLATE="html/customer-email-message";
	@Override
	@Async("asyncExecutor")
	public String sendCustomerMessageEmail(EmailVO mail) {
	
	try {
		//Here write code for sending email using the template
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper message=new MimeMessageHelper(mimeMessage,true,"UTF-8");
		InternetAddress fromAddress = new InternetAddress("synergisticit2020@gmail.com", "DesiBank PVT. LTD.");
		message.setFrom(fromAddress);
		message.setTo(mail.getTo());
		message.setSubject("Important message from the bank !!!!!!");
		message.setSentDate(new Date());
		
		//set the locale and put variable to placeholder
	    final Context ctx = new Context(Locale.ENGLISH);

	    ctx.setVariable("name", mail.getName());
	    ctx.setVariable("bankIcon", "bankIcon");
	    ctx.setVariable("mobile", "9999897878");
	    ctx.setVariable("message", mail.getMessage());
	    
	    // Create the HTML body using Thymeleaf
        final String htmlContent = htmlTemplateEngine.process(CUSTOMER_EMAIL_MESSAGE_TEMPLATE, ctx);
        message.setText(htmlContent, true /* isHtml */);
        
	    // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
        byte[] bytes;
        bytes = sun.misc.IOUtils.readFully(new ClassPathResource("images/bank-icon.png").getInputStream(), -1, true);
	    InputStreamSource imageSource =new ByteArrayResource(bytes);
	    message.addInline("bankIcon", imageSource, "image/png");

		mailSender.send(mimeMessage);
	} catch (Exception exe) {
		exe.printStackTrace();
	}
	 return "success";
	}
	
	@Override
	@Async("asyncExecutor")
	public String sendEnquiryConfirmation(String email,String name,String imageUrl,String appref,String salutation) {
	
	try {
		//Here write code for sending email using the template
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper message=new MimeMessageHelper(mimeMessage,true,"UTF-8");
		InternetAddress fromAddress = new InternetAddress("synergisticit2020@gmail.com", "DesiBank PVT. LTD.");
		message.setFrom(fromAddress);
		message.setTo(email);
		message.setSubject("Regarding Saving Account Opening Request!");
		message.setSentDate(new Date());
		
		//set the locale and put variable to placeholder
	    final Context ctx = new Context(Locale.ENGLISH);
	    ctx.setVariable("name",salutation+" "+ name);
	    ctx.setVariable("appNo", appref);
	    ctx.setVariable("bankIcon", "bankIcon");
	    ctx.setVariable("mobile", "9999897878");
	    
	    // Create the HTML body using Thymeleaf
        final String htmlContent = htmlTemplateEngine.process(SAVING_ACCOUNT_CONFIRMATION_TEMPLATE, ctx);
        message.setText(htmlContent, true /* isHtml */);
        
	    // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
        byte[] bytes;
        bytes = sun.misc.IOUtils.readFully(new ClassPathResource("images/bank-icon.png").getInputStream(), -1, true);
	    InputStreamSource imageSource =new ByteArrayResource(bytes);
	    message.addInline("bankIcon", imageSource, "image/png");

		mailSender.send(mimeMessage);
	} catch (Exception exe) {
		exe.printStackTrace();
	}
	 return "success";
	}
	
	
	@Async
	@Override
	public void sendResetPasswordEmail(EmailVO mail) {

	MimeMessage mimeMessage = mailSender.createMimeMessage();
	try {
		MimeMessageHelper message=new MimeMessageHelper(mimeMessage,true,"UTF-8");
	    InternetAddress fromAddress = new InternetAddress(mail.getFrom(), "DesiBank Admin");
		message.setFrom(fromAddress);
		message.setTo(mail.getTo());
		message.setSubject(mail.getSubject());
		message.setSentDate(new Date());
		
		//compose the content of the email
		//set the locale and put variable to placeholder
	    final Context ctx = new Context(Locale.ENGLISH);
	    ctx.setVariable("cname", mail.getName());
	    ctx.setVariable("resetpasswordlink", mail.getLink());
		
        final String htmlContent = htmlTemplateEngine.process("html/customer-reset-password", ctx);
        message.setText(htmlContent, true /* isHtml */);
	    // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
        byte[] bytes;
        bytes = sun.misc.IOUtils.readFully(new ClassPathResource("images/bank-icon.png").getInputStream(), -1, true);
	    InputStreamSource imageSource =new ByteArrayResource(bytes);
	    message.addInline("bankIcon", imageSource, "image/png");
		mailSender.send(mimeMessage);
	} catch (Exception exe) {
		exe.printStackTrace();
	}
}
	
	
	@Async
	@Override
	public void sendRegistrationEmail(EmailVO mail) {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		try {
			MimeMessageHelper message=new MimeMessageHelper(mimeMessage,true,"UTF-8");
		    InternetAddress fromAddress = new InternetAddress(mail.getFrom(), "DesiBank Admin");
			message.setFrom(fromAddress);
			// message.setSender(new InternetAddress(from));
			
			message.setTo(mail.getTo());
//			message.setTo(Message.RecipientType.TO,InternetAddress.parse());
			message.setSubject(mail.getSubject());
			message.setSentDate(new Date());
			
			
			//compose the content of the email
			//set the locale and put variable to placeholder
		    final Context ctx = new Context(Locale.ENGLISH);
		    ctx.setVariable("cname", mail.getName());
		    ctx.setVariable("registrationlink", mail.getLink());
	
			
	        final String htmlContent = htmlTemplateEngine.process("html/customer-registration", ctx);
	        message.setText(htmlContent, true /* isHtml */);
	        
		    // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
	        byte[] bytes;
	        bytes = sun.misc.IOUtils.readFully(new ClassPathResource("images/bank-icon.png").getInputStream(), -1, true);
		    InputStreamSource imageSource =new ByteArrayResource(bytes);
		    message.addInline("bankIcon", imageSource, "image/png");

			mailSender.send(mimeMessage);
			
			
			
			
			
//			// This mail has 2 part, the BODY and the embedded image
//			MimeMultipart multipart = new MimeMultipart("related");
//			// first part (the html)
//			BodyPart messageBodyPart = new MimeBodyPart();
//			
//			 //Classpath - >>> /WEB-INF/classes
//			//this code is creating html template with dynamic data
//			Template template = velocityEngine.getTemplate("./templates/customer-registration.vm");
//			VelocityContext velocityContext = new VelocityContext();
//			velocityContext.put("cname", mail.getName());
//			velocityContext.put("registrationlink", mail.getLink());
//			StringWriter stringWriter = new StringWriter();
//			template.merge(velocityContext, stringWriter);
//			System.out.println(" :-"+stringWriter.toString());
//
//			messageBodyPart.setContent(stringWriter.toString(), "text/html");
//			multipart.addBodyPart(messageBodyPart);
//			
//			 messageBodyPart = new MimeBodyPart();
//			// DataSource fds = new FileDataSource("D:/sss.jpg");
//			 System.out.println("_)#_)#  = URL = "+mail.getBaseUrl()+"/images/banklogo.png");
//	         messageBodyPart.setDataHandler(new DataHandler(new URL(mail.getBaseUrl()+"/images/banklogo.png")));
//	         messageBodyPart.setHeader("Content-ID", "<banklogo>");
//	         multipart.addBodyPart(messageBodyPart);
//		
//	         // put everything together
//			message.setContent(multipart);
//			mailSender.send(message);
		} catch (Exception exe) {
			exe.printStackTrace();
		}
	}
/*
	@Async
	@Override
	public void sendRejectSavingRequestEmail(EmailVO mail, String reason) {
		MimeMessage message = mailSender.createMimeMessage();
		try {
			InternetAddress fromAddress = new InternetAddress(mail.getFrom(), "DesiBank Admin");
			message.setFrom(fromAddress);
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getTo()));
			message.setSubject(mail.getSubject());
			message.setSentDate(new Date());

			MimeMultipart multipart = new MimeMultipart("related");
			BodyPart messageBodyPart = new MimeBodyPart();

			Template template = velocityEngine.getTemplate("./templates/customer-rejection.vm");
			VelocityContext velocityContext = new VelocityContext();
			velocityContext.put("cname", mail.getName());
			velocityContext.put("reason", reason);
			StringWriter stringWriter = new StringWriter();
			template.merge(velocityContext, stringWriter);

			messageBodyPart.setContent(stringWriter.toString(), "text/html");
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();

			messageBodyPart.setDataHandler(new DataHandler(new URL(mail.getBaseUrl() + "/images/banklogo.png")));
			messageBodyPart.setHeader("Content-ID", "<banklogo>");
			multipart.addBodyPart(messageBodyPart);

			message.setContent(multipart);
			mailSender.send(message);
		} catch (Exception exe) {
			exe.printStackTrace();
		}
	}
	
	@Async
	@Override
	public void sendCreditCardEmail(EmailVO mail, String frontImage, String backImage) {
		MimeMessage message = mailSender.createMimeMessage();
		try {
			InternetAddress fromAddress = new InternetAddress(mail.getFrom(), "DesiBank Admin");
			message.setFrom(fromAddress);
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getTo()));
			message.setSubject(mail.getSubject());
			message.setSentDate(new Date());

			MimeMultipart multipart = new MimeMultipart("related");
			BodyPart messageBodyPart = new MimeBodyPart();

			Template template = velocityEngine.getTemplate("./templates/credit-card.vm");
			VelocityContext velocityContext = new VelocityContext();
			velocityContext.put("cname", mail.getName());
//			velocityContext.put("frontImage", frontImage);
//			velocityContext.put("backImage", backImage);
			
			StringWriter stringWriter = new StringWriter();
			template.merge(velocityContext, stringWriter);

			messageBodyPart.setContent(stringWriter.toString(), "text/html");
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(Base64.getDecoder().decode(frontImage), "image/jpeg")));
			messageBodyPart.setHeader("Content-ID", "<frontImage>");
			multipart.addBodyPart(messageBodyPart);
			
			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(Base64.getDecoder().decode(backImage), "image/jpeg")));
			messageBodyPart.setHeader("Content-ID", "<backImage>");
			multipart.addBodyPart(messageBodyPart);
			
			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setDataHandler(new DataHandler(new URL(mail.getBaseUrl() + "/images/banklogo.png")));
			messageBodyPart.setHeader("Content-ID", "<banklogo>");
			multipart.addBodyPart(messageBodyPart);

			message.setContent(multipart);
			mailSender.send(message);
		} catch (Exception exe) {
			exe.printStackTrace();
		}
	}
	
	@Async
	@Override
	public void sendUnlockOPTEmail(EmailVO mail, int opt) {
		MimeMessage message = mailSender.createMimeMessage();
		try {
			InternetAddress fromAddress = new InternetAddress(mail.getFrom(), "DesiBank Admin");
			message.setFrom(fromAddress);
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getTo()));
			message.setSubject(mail.getSubject());
			message.setSentDate(new Date());

			MimeMultipart multipart = new MimeMultipart("related");
			BodyPart messageBodyPart = new MimeBodyPart();

			Template template = velocityEngine.getTemplate("./templates/customer-unlock-otp.vm");
			VelocityContext velocityContext = new VelocityContext();
			velocityContext.put("cname", mail.getName());
			velocityContext.put("opt", opt);
			StringWriter stringWriter = new StringWriter();
			template.merge(velocityContext, stringWriter);

			messageBodyPart.setContent(stringWriter.toString(), "text/html");
			multipart.addBodyPart(messageBodyPart);

			messageBodyPart = new MimeBodyPart();

			messageBodyPart.setDataHandler(new DataHandler(new URL(mail.getBaseUrl() + "/images/banklogo.png")));
			messageBodyPart.setHeader("Content-ID", "<banklogo>");
			multipart.addBodyPart(messageBodyPart);

			message.setContent(multipart);
			mailSender.send(message);
		} catch (Exception exe) {
			exe.printStackTrace();
		}
	}*/


	@Async("asyncExecutor")
	@Override
	public void sendCreditCardOTPEmail(String toEmail, String otp) {
		

		try {
			//Here write code for sending email using the template
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper message=new MimeMessageHelper(mimeMessage,true,"UTF-8");
			InternetAddress fromAddress = new InternetAddress("synergisticit2020@gmail.com", "DesiBank PVT. LTD.");
			message.setFrom(fromAddress);
			message.setTo(toEmail);
			message.setSubject("Regarding The One Time Password for Associating Credit Card!");
			message.setSentDate(new Date());
			
			//set the locale and put variable to placeholder
		    final Context ctx = new Context(Locale.ENGLISH);
		    ctx.setVariable("otp", otp);

		    
		    // Create the HTML body using Thymeleaf
	        final String htmlContent = htmlTemplateEngine.process("html/customer-credit-card-otp", ctx);
	        message.setText(htmlContent, true /* isHtml */);
	        
		    // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
	        byte[] bytes;
	        bytes = sun.misc.IOUtils.readFully(new ClassPathResource("images/bank-icon.png").getInputStream(), -1, true);
		    InputStreamSource imageSource =new ByteArrayResource(bytes);
		    message.addInline("bankIcon", imageSource, "image/png");

			mailSender.send(mimeMessage);
		} catch (Exception exe) {
			exe.printStackTrace();
		}
	
		
	}
	
	
}

