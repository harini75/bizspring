package com.desi.bank.email.service;

import java.util.Date;
import java.util.Locale;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class EmployeeEmailService {
	
	 private final  String CREATE_EMPLOYEE_EMAIL_TEMPLATE="html/create-employee-email";

	/**
	 * below bean is automatically created by spring boot
	 */
    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    @Qualifier("thyemeleafEmailTemplateEngine")
    private TemplateEngine htmlTemplateEngine;
    
    
    @Async("asyncExecutor")
    public String sendEmployeeAccountEmail(String name,String toemail,String registrationlink){
    	 //Here write code for sending email using the template
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		try {
			MimeMessageHelper message=new MimeMessageHelper(mimeMessage,true,"UTF-8");
		    InternetAddress fromAddress = new InternetAddress("synergisticit2020@gmail.com", "DesiBank PVT. LTD.");
			message.setFrom(fromAddress);
			message.setTo(toemail);
			message.setSubject("Regarding your account creation as an employee.");
			message.setSentDate(new Date());
			//compose the content of the email
			//set the locale and put variable to placeholder
		    final Context ctx = new Context(Locale.ENGLISH);
		    ctx.setVariable("cname", name);
		    ctx.setVariable("registrationlink", registrationlink);
		    ctx.setVariable("bankemail", "synergisticit2020@gmail.com");
		    
		    final String htmlContent = htmlTemplateEngine.process("html/create-employee-email", ctx);
	        message.setText(htmlContent, true /* isHtml */);
	        
	        byte[] bytes;
	        bytes = sun.misc.IOUtils.readFully(new ClassPathResource("images/bank-image.png").getInputStream(), -1, true);
		    InputStreamSource imageSource =new ByteArrayResource(bytes);
		    message.addInline("bankIcon", imageSource, "image/png");
		    
		    mailSender.send(mimeMessage);
		}catch(Exception ex){
			ex.printStackTrace();
		}
    	
    	return "success";
    }
    
	
}
