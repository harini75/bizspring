package com.desi.bank.email.service.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

@Configuration
public class ThymeleafMailConfig {
	
	public static final String EMAIL_TEMPLATE_ENCODING = "UTF-8";
	
	//for i18n   
		@Bean
	    public ResourceBundleMessageSource emailMessageSource() {
	        final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
	        messageSource.setBasename("mail/MailMessages");
	        return messageSource;
	    }
		
		//<bean id="thyemeleafEmailTemplateEngine" class/>
	    @Bean(name="thyemeleafEmailTemplateEngine")
	    public TemplateEngine emailTemplateEngine() {
	        final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
	        // Resolver for HTML emails (except the editable one)
	        templateEngine.addTemplateResolver(htmlTemplateResolver());
            /*Resolver for HTML editable emails (which will be treated as a String)
        	templateEngine.addTemplateResolver(stringTemplateResolver());*/
	        // Message source, internationalization specific to emails
	        templateEngine.setTemplateEngineMessageSource(emailMessageSource());
	        return templateEngine;
	    }
	
	
	    
	    private ITemplateResolver htmlTemplateResolver() {
	        final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	        templateResolver.setOrder(Integer.valueOf(1));
	        templateResolver.setResolvablePatterns(Collections.singleton("html/*"));
	        //resources/mail/xxx.html
	        templateResolver.setPrefix("/mail/");
	        templateResolver.setSuffix(".html");
	        templateResolver.setTemplateMode(TemplateMode.HTML);
	        templateResolver.setCharacterEncoding(EMAIL_TEMPLATE_ENCODING);
	        templateResolver.setCacheable(false);
	        return templateResolver;
	    }
	    
}
