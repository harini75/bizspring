package com.desi.bank.email.service;

import com.desi.bank.employee.web.controller.form.CustomerAccountRegistrationVO;

/**
 * 
 * @author VC1
 *
 */
public interface ICustomerEmailService {

	
	public String sendEnquiryConfirmation(String email, String name, String imageUrl,String appref,String salutation);
	public String sendAccountCreationEmail(CustomerAccountRegistrationVO customerAccountRegistrationVO);
	public void sendRegistrationEmail(EmailVO mail);
	/*
	void sendRejectSavingRequestEmail(EmailVO mail, String reason);
	void sendCreditCardEmail(EmailVO mail, String frontImage, String backImage);
	void sendUnlockOPTEmail(EmailVO mail, int opt);*/
	public void sendCreditCardOTPEmail(String toEmail, String otp) ;
	void sendResetPasswordEmail(EmailVO mail);
	public String sendCustomerMessageEmail(EmailVO mail);
}
