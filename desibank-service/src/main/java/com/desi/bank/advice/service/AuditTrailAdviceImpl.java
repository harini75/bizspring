package com.desi.bank.advice.service;

import java.lang.reflect.Method;
import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desi.bank.common.advice.AuditTrail;
import com.desi.bank.springdata.dao.entity.AuditTrailDocument;
import com.desi.bank.springdata.dao.repository.AuditTrailDao;

@Service
@Aspect
public class AuditTrailAdviceImpl {

	@Autowired
	private AuditTrailDao auditTrailDao;
	
	 @Around("auditTrailPointcut()")
	    public Object interceptAction(ProceedingJoinPoint proceedingJoinPoint){
	        MethodSignature signature = (MethodSignature) proceedingJoinPoint.getSignature();
	        Method method = signature.getMethod();
	        String methodName=method.getName();
	        AuditTrail auditTrailor = method.getAnnotation(AuditTrail.class);
	        String action=auditTrailor.action();
	        String entity=auditTrailor.entity();
	        AuditTrailDocument document=new AuditTrailDocument();
	        document.setAction(action);
	        document.setDetails(entity+" is  "+action);
	        document.setDoe(new Date());
	        Object   response=null;
	      try {
			   response=proceedingJoinPoint.proceed();
			document.setUser((String)response);
			auditTrailDao.save(document);
		} catch (Throwable e) {
			e.printStackTrace();
		} // here we are calling actual method
	        return response;
	 }  
	

	    @Pointcut("@annotation(com.desi.bank.common.advice.AuditTrail)")
	    private void auditTrailPointcut() {
	    }
}
