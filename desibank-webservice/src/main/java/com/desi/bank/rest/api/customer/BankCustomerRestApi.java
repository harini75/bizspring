package com.desi.bank.rest.api.customer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.desi.bank.admin.service.AdminService;
import com.desi.bank.common.dao.entity.CustomerQuestionAnswer;
import com.desi.bank.common.service.AccountProcessingStepService;
import com.desi.bank.constant.DesiBankConstant;
import com.desi.bank.customer.service.CustomerService;
import com.desi.bank.customer.service.OtpService;
import com.desi.bank.customer.service.model.AccountProcessingStepVO;
import com.desi.bank.customer.service.model.CreditCardValidationInfo;
import com.desi.bank.customer.service.model.CustomerQuestionAnswerVO;
import com.desi.bank.customer.service.model.TransactionHistoryVO;
import com.desi.bank.customer.web.controller.form.CustomerForm;
import com.desi.bank.customer.web.controller.form.CustomerSavingForm;
import com.desi.bank.customer.web.controller.form.MiniStatementVO;
import com.desi.bank.email.service.EmailVO;
import com.desi.bank.employee.web.controller.form.ApplicationMessageResponse;
import com.desi.bank.rest.api.customer.model.CustomerSQAnswers;
import com.desi.bank.rest.api.customer.model.CustomerUnlockOTP;
import com.desi.bank.rest.api.customer.response.CustomerQuestionResponse;
import com.desi.bank.rest.api.customer.response.TransactionsHistoryResponse;
import com.desi.bank.springdata.common.service.LocationService;
import com.desi.bank.web.model.LocationVO;

@Component
@Path("/customer")
public class BankCustomerRestApi {
	private static final Log logger = LogFactory.getLog(BankCustomerRestApi.class);

	@Autowired
	@Qualifier("CustomerServiceImpl")
	private CustomerService customerService;

/*	@Autowired
	@Qualifier("CustomerEmailService")
	private ICustomerEmailService customerEmailService;*/

	@Autowired
	private LocationService locationService;

	@Autowired
	@Qualifier("OtpServiceImpl")
	private OtpService otpService;

	@Autowired
	private AdminService adminService;

	@Autowired
	private AccountProcessingStepService accountProcessingStepService;
	
	@Path("/creditCards/validation")
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public String validateCreditcardAndSendOTP(CreditCardValidationInfo creditCardValidationInfo) {
		System.out.println(creditCardValidationInfo);
		if(customerService.validateCreditCard(creditCardValidationInfo)) {
			
			customerService.sendCreditCardOTP(creditCardValidationInfo.getCardNumber());
		return "success";
		}
		else 
			return "failed";
		
	}

	@Path("/cities")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<LocationVO> loadCities() {
		return locationService.findAll();
	}

	@Path("/steps")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<AccountProcessingStepVO> loadSteps() {
		return accountProcessingStepService.findAccountProcessingSteps();
	}
	
	// URL for this resource
	// contentpath/v1/customer/app/status?appRefNo=AS0292022
	@Path("/app/status")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public SCustomerSavingForm findCustomerAppStatus(@QueryParam("appRefNo") String appRefNo) {
		CustomerSavingForm customerSavingForm = customerService.findCustomerSavingEnquiryByAppRef(appRefNo);
		SCustomerSavingForm customerSavingForm2 = new SCustomerSavingForm();
		BeanUtils.copyProperties(customerSavingForm, customerSavingForm2);
		return customerSavingForm2;
	}

	// web.xml - v1
	// $.getJSON(context+"/v1/customer/find/age?pdate=sdate",function(data){
	@Path("/find/age")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ApplicationMessageResponse calculateAge(@QueryParam("pdate") String pdate) {
		//// {"age" : 10}
		// pdate =2018-07-19
		String tokens[] = pdate.split("-");
		int dobyear = Integer.parseInt(tokens[0]);
		int year = Calendar.getInstance().get(Calendar.YEAR);
		int finalAge = year - dobyear;
		if (finalAge < 0) {
			finalAge = 0;
		}
		ApplicationMessageResponse applicationMessageResponse = new ApplicationMessageResponse();
		applicationMessageResponse.setStatus(DesiBankConstant.SUCCESS);
		applicationMessageResponse.setMessage(finalAge + "");
		return applicationMessageResponse;
	}

	@Path("/savingRequest")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ApplicationMessageResponse rejectSavingSavingAccountRequest(CustomerSavingForm customerSavingForm) {
		System.out.println(customerSavingForm);
		customerService.savingAccountRequest(customerSavingForm);
		ApplicationMessageResponse applicationMessageResponse = new ApplicationMessageResponse();
		applicationMessageResponse.setStatus(DesiBankConstant.SUCCESS);
		return applicationMessageResponse;
	}

	@Path("/temp")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public CustomerSavingForm showTemp() {
		System.out.println("_@_magic000000000000000");
		CustomerSavingForm customerSavingForm2 = new CustomerSavingForm();
		customerSavingForm2.setEmail("nagen@gmail.com");
		customerSavingForm2.setLocation("India");
		customerSavingForm2.setMobile("+202929292");
		customerSavingForm2.setName("Nagendra!");
		return customerSavingForm2;
	}

	// rootpath/v1/customer/questions/{userId}
	@Path("/questions/{userId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CustomerQuestionResponse> findCustomerSecurityQuestions(@PathParam("userId") String userId) {
		// TODO: it uses Java Class from dao project..... should we change?
		List<CustomerQuestionAnswerVO> questionAnswers = customerService.getSecurityQn(userId);

		List<CustomerQuestionResponse> responses = new ArrayList<CustomerQuestionResponse>();
		for (CustomerQuestionAnswerVO customerQuestionAnswer : questionAnswers) {
			CustomerQuestionResponse question = new CustomerQuestionResponse();
			question.setId(customerQuestionAnswer.getId());
			question.setQuestion(customerQuestionAnswer.getQuestion());
			responses.add(question);
		}

		return responses;
	}

	// rootpath/v1/customer/unlock/verification
	@Path("/unlock/verification")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ApplicationMessageResponse unlockAccountVerification(CustomerSQAnswers answers) {
		ApplicationMessageResponse applicationMessageResponse = new ApplicationMessageResponse();
		applicationMessageResponse.setStatus(DesiBankConstant.SUCCESS);

		List<CustomerQuestionAnswerVO> questionAnswers = customerService.getSecurityQn(answers.getUserId());
		for (CustomerQuestionAnswerVO customerQuestionAnswer : questionAnswers) {
			String customerAnswer = answers.getAnswers().get(customerQuestionAnswer.getId());
			if (!customerQuestionAnswer.getAnswer().equals(customerAnswer)) {
				applicationMessageResponse.setStatus(DesiBankConstant.FAILURE);
				applicationMessageResponse.setMessage("Some answers are incorrect");
				return applicationMessageResponse;
			}
		}

		// send OPT email here
		this.sendUnlockOTPEmail(answers.getUserId());

		return applicationMessageResponse;
	}

	@Path("/unlock/otp/{userId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ApplicationMessageResponse requestSendingUnlockOTPEmail(@PathParam("userId") String userId) {
		ApplicationMessageResponse applicationMessageResponse = new ApplicationMessageResponse();
		applicationMessageResponse.setStatus(DesiBankConstant.SUCCESS);

		try {
			this.sendUnlockOTPEmail(userId);

			applicationMessageResponse.setMessage("New OTP has been sent to your email.");
		} catch (Exception e) {
			applicationMessageResponse.setStatus(DesiBankConstant.FAILURE);
			applicationMessageResponse.setMessage("Error occured. Please Retry!");
		}

		return applicationMessageResponse;
	}

	@Path("/unlock/otp")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ApplicationMessageResponse unlockOTPVerification(CustomerUnlockOTP unlockOTP) {
		ApplicationMessageResponse applicationMessageResponse = new ApplicationMessageResponse();
		applicationMessageResponse.setStatus(DesiBankConstant.SUCCESS);

		int serverOtp = otpService.getOtp(unlockOTP.getUserId());
		if (unlockOTP.getOtp() == serverOtp) {
			otpService.clearOTP(unlockOTP.getUserId());

			adminService.unlockCustomer(unlockOTP.getUserId());

			applicationMessageResponse.setMessage("Congratulation!!! Your account is unlocked.");
		} else {
			applicationMessageResponse.setStatus(DesiBankConstant.FAILURE);
			applicationMessageResponse.setMessage("Entered Otp is NOT valid. Please Retry!");
		}

		return applicationMessageResponse;
	}

	private void sendUnlockOTPEmail(String uesrId) {
		CustomerForm customer = customerService.getCustomer(uesrId);
		int otp = otpService.generateOTP(uesrId);

		EmailVO mail = new EmailVO();
		// TODO: hard code but we should use image from resources not from URL
		mail.setBaseUrl("http://localhost:8080/desibank-web");
		mail.setFrom("desibankproject@gmail.com");
		mail.setName(customer.getName());
		mail.setSubject("OTP for unlocking your account");
		mail.setTo(customer.getEmail());
		//customerEmailService.sendUnlockOPTEmail(mail, otp);
	}

	// rootpath/v1/customer/mini-statement
	@Path("/mini-statement")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<MiniStatementVO> getMiniStatement(@QueryParam("accountNumber") String accountNumber) {
		List<MiniStatementVO> miniStatementVO = customerService.getMiniStatementByAccountNo(accountNumber, 10);
		return miniStatementVO;
	}

	@Path("/transations-history")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public TransactionsHistoryResponse traverseTransactions(@QueryParam("accountNumber") String accountNumber,
			@QueryParam("limit") int limit, @QueryParam("offset") int offset, @QueryParam("from") String from,
			@QueryParam("to") String to) {

		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		Date fromDate = null, toDate = null;

		try {
			if (from != null) {				
				fromDate = format.parse(from);
			}

			if (to != null) {				
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(format.parse(to));
				calendar.add(Calendar.DATE, 1);
				
				toDate = calendar.getTime();
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		List<TransactionHistoryVO> transactionHistoryVOs = customerService
				.traverseTransactionsHistoryByAccountNumber(accountNumber, limit, offset, fromDate, toDate);

		Long total = customerService.countTraverseTransactionsByAccountNumber(accountNumber, fromDate, toDate);
		
		TransactionsHistoryResponse response = new TransactionsHistoryResponse();
		response.setLimit(limit);
		response.setOffset(offset);
		response.setTotal(total);
		response.setTransactions(transactionHistoryVOs);
		
		return response;
	}
}
