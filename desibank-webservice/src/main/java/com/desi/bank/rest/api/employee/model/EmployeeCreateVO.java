package com.desi.bank.rest.api.employee.model;

public class EmployeeCreateVO {
	private String userid;
	private String email;
	private String mobile;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Override
	public String toString() {
		return "EmployeeCreateVO [userid=" + userid + ", email=" + email + ", mobile=" + mobile + "]";
	}

}
