package com.desi.bank.rest.api.employee;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.desi.bank.customer.service.CustomerService;
import com.desi.bank.customer.web.controller.form.CustomerAccountInfoVO;
import com.desi.bank.customer.web.controller.form.CustomerForm;
import com.desi.bank.email.service.EmailVO;
import com.desi.bank.employee.service.EmployeeService;
import com.desi.bank.employee.web.controller.form.ApplicationMessageResponse;
import com.desi.bank.employee.web.controller.form.CustomerAccountRegistrationVO;
import com.desi.bank.employee.web.controller.form.CustomerAccountRequestVO;
import com.desi.bank.rest.api.employee.model.BasicCustomerInfo;
import com.desi.bank.rest.api.employee.model.CreditCardInfo;
import com.desi.bank.rest.api.employee.model.EmployeeCreateVO;
import com.desi.bank.rest.api.response.GenerateCreditCardResponse;

/**
 * 
 * @author nagendra
 *
 */
@Service
@Path("/employee")
public class EmployeeRestApi {

	private static final Log logger = LogFactory.getLog(EmployeeRestApi.class);

	@Autowired
	@Qualifier("EmployeeServiceImpl")
	private EmployeeService employeeService;

	@Autowired
	@Qualifier("CustomerServiceImpl")
	private CustomerService customerService;

	/*@Autowired
	@Qualifier("CustomerEmailService")
	private ICustomerEmailService customerEmailService;*/

	//<contextpath>/v1/employee/validate-employee-info
	@Path("/validate-employee-info") // @PostMapping("/validate-employee-info")
	@POST
	@Produces(MediaType.APPLICATION_JSON) // @ResponseBody
	@Consumes(MediaType.APPLICATION_JSON) // @RequestBody
	public ApplicationMessageResponse validateEmployeeInfo(EmployeeCreateVO employeeCreateVO) {
		String message="";
		message=employeeService.validateEmployeeInfo(employeeCreateVO.getUserid(), employeeCreateVO.getEmail(),employeeCreateVO.getMobile());
		ApplicationMessageResponse applicationMessageResponse=new ApplicationMessageResponse();
		if(message.length()==0) {
			applicationMessageResponse.setStatus("success");	
		}else {
			applicationMessageResponse.setStatus("failed");
		}
		applicationMessageResponse.setMessage(message);
		return applicationMessageResponse;
	}
	
	

	@Path("/create-customer-account")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public ApplicationMessageResponse createCustomerAccount(CustomerAccountRequestVO customerAccoutRequestVO) {
		CustomerAccountInfoVO customerAccountInfoVO = employeeService
				.createCustomerAccount(customerAccoutRequestVO.getUserid());

		CustomerForm customerForm = customerService.getCustomer(customerAccoutRequestVO.getUserid());
		// write code to send email
		CustomerAccountRegistrationVO customerAccount = new CustomerAccountRegistrationVO();
		// Prepare input data
		customerAccount.setAccount(customerAccountInfoVO.getAccountNumber());
		customerAccount.setAppRef("TempA3030393");
		customerAccount.setAvgbalance("1000");
		
		customerAccount.setBankEmail("admin@desibank.com");
		customerAccount.setBranch(customerAccountInfoVO.getBranch());
		customerAccount.setCurrency(customerAccountInfoVO.getCurrency());
		customerAccount.setImage("http://localhost:8080/desibank-web/images/thanks.png");
		customerAccount.setTo(customerForm.getEmail());
		customerAccount.setName(customerForm.getName());
		customerAccount.setSubject(
				"Regarding account summary for your new " + customerAccountInfoVO.getAccountType() + " account.");
		customerAccount.setType(customerAccountInfoVO.getAccountType());
		customerAccount.setUserid(customerAccoutRequestVO.getUserid());

		employeeService.sendAccountSummaryEmail(customerAccount);
		ApplicationMessageResponse applicationMessageResponse = new ApplicationMessageResponse();
		applicationMessageResponse.setStatus("success");
		applicationMessageResponse.setMessage("Account is created successfully for user id "
				+ customerAccoutRequestVO.getUserid() + " and generated account number is 08383736736363");
		// applicationMessageResponse.setMessage("Account is created successfully for
		// user id "+customerAccoutRequestVO.getUserid()+" and generated account number
		// is "+customerAccountInfoVO.getAccountNumber());
		return applicationMessageResponse;
	}

	@Path("/creditcard-generation")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GenerateCreditCardResponse generateCreditCard(BasicCustomerInfo customerInfo) {
		String number = employeeService.generateCreditCardNumber();
		String expireDate = employeeService.generateCreditCardExpireDate();
		String frontImage = employeeService.generateFrontCreditCard(customerInfo.getCustomerName(), number, expireDate);

		String cvv = employeeService.generateCCVNumber();
		String backImage = employeeService.generateBackCreditCard(cvv);

		GenerateCreditCardResponse response = new GenerateCreditCardResponse();
		response.setStatus("success");
		response.setFrontImage(frontImage);
		response.setBackImage(backImage);

		return response;
	}

	@Path("/creditcard-email")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ApplicationMessageResponse emailCreditCard(CreditCardInfo info) {

		ApplicationMessageResponse response = new ApplicationMessageResponse();

		try {
			EmailVO mail = new EmailVO();
			// TODO: hard code but we should use image from resources not from URL
			mail.setBaseUrl("http://localhost:444/desibank-web");
			mail.setFrom("desibankproject@gmail.com");
			mail.setName(info.getCustomerName());
			mail.setSubject("You credit card information");
			mail.setTo(info.getEmail());
			//TDD
			//customerEmailService.sendCreditCardEmail(mail, info.getFrontImage(), info.getBackImage());

			response.setStatus("success");
			response.setMessage("Email is sent successfully to " + info.getEmail());
		} catch (Exception ex) {
			StringWriter stringWriter = new StringWriter();
			ex.printStackTrace(new PrintWriter(stringWriter));
			if (logger.isErrorEnabled()) {
				logger.error(stringWriter.toString());
			}

			response.setStatus("fail");
			response.setException(ex.getMessage());
		}

		return response;
	}

}
