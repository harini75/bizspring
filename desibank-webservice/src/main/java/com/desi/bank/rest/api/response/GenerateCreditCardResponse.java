package com.desi.bank.rest.api.response;

public class GenerateCreditCardResponse {
	private String status;
	private String message;
	private String exception;
	private String frontImage;
	private String backImage;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getFrontImage() {
		return frontImage;
	}

	public void setFrontImage(String frontImage) {
		this.frontImage = frontImage;
	}

	public String getBackImage() {
		return backImage;
	}

	public void setBackImage(String backImage) {
		this.backImage = backImage;
	}

	@Override
	public String toString() {
		return "GenerateCreditCardResponse [status=" + status + ", message=" + message + ", exception=" + exception
				+ "]";
	}

}
