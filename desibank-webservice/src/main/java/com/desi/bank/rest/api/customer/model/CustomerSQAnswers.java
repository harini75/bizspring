package com.desi.bank.rest.api.customer.model;

import java.util.Map;

public class CustomerSQAnswers {
	private String userId;
	Map<Integer, String> answers;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Map<Integer, String> getAnswers() {
		return answers;
	}
	public void setAnswers(Map<Integer, String> answers) {
		this.answers = answers;
	}
	
	@Override
	public String toString() {
		return "CustomerSQAnswers [userId=" + userId + ", answers=" + answers + "]";
	}
}
