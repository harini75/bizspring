package com.desi.bank.rest.api.customer.model;

public class CustomerUnlockOTP {
	private String userId;
	private int otp;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getOtp() {
		return otp;
	}

	public void setOtp(int otp) {
		this.otp = otp;
	}

	@Override
	public String toString() {
		return "CustomerUnlockOTP [userId=" + userId + ", otp=" + otp + "]";
	}

}
