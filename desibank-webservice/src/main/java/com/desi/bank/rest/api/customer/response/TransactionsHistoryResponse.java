package com.desi.bank.rest.api.customer.response;

import java.util.List;

import com.desi.bank.customer.service.model.TransactionHistoryVO;

public class TransactionsHistoryResponse {

	private int limit;
	private int offset;
	private Long total;
	private List<TransactionHistoryVO> transactions;

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public List<TransactionHistoryVO> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionHistoryVO> transactions) {
		this.transactions = transactions;
	}

}
