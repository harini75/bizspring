package com.desi.bank.rest.api.admin;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.desi.bank.admin.service.AdminService;
import com.desi.bank.customer.service.CustomerService;
import com.desi.bank.employee.web.controller.form.ApplicationMessageResponse;

/**
 * 
 * @author Nagendra
 * @Since 19-Sept-2018
 *
 */
@Component
@Path("/admin")
public class BankAdminRestApi {
	

	@Autowired
	@Qualifier("adminServiceImpl")
	public AdminService adminService;
	
	@Autowired
	@Qualifier("CustomerServiceImpl")
	public CustomerService customerService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	/**
	 * 
	 * @param userid
	 * @param status
	 * @return
	 */
	@Path("/lock-unlock")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ApplicationMessageResponse calculateAge(@QueryParam("userid") String userid,@QueryParam("status") String status){
			String dstatus=adminService.updateLockStatus(userid, status);
			ApplicationMessageResponse applicationMessageResponse=new ApplicationMessageResponse();
			applicationMessageResponse.setStatus(dstatus);
			applicationMessageResponse.setMessage(status.equals("yes")?"Employee with id"+userid+" has been locked":"Employee with id"+userid+" has been unlocked");
			return applicationMessageResponse;
	}
	
	
	@Path("/resetpassword")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ApplicationMessageResponse resetPassword(@QueryParam("userid") String userid,@QueryParam("newPassword") String newPassword){
		String msg=customerService.updatePassword(userid, passwordEncoder.encode(newPassword));
		ApplicationMessageResponse applicationMessageResponse=new ApplicationMessageResponse();
		if(msg == "success")
			applicationMessageResponse.setStatus("success");
		else
			applicationMessageResponse.setStatus("failure");
			applicationMessageResponse.setMessage(msg.equals("success")?"Your password has been successfully updated!":"Sorry your password is not updated successfully!");
			return applicationMessageResponse;	
	}
	
}	