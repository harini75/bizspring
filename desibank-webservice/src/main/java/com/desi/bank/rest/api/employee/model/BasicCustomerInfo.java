package com.desi.bank.rest.api.employee.model;

public class BasicCustomerInfo {
	private int csaid;
	private String customerName;
	private String email;

	public int getCsaid() {
		return csaid;
	}

	public void setCsaid(int csaid) {
		this.csaid = csaid;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "RejectSavingRequestForm [csaid=" + csaid + ", customerName=" + customerName + ", email=" + email + "]";
	}

}