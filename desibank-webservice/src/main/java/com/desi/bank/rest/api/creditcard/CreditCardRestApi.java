package com.desi.bank.rest.api.creditcard;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.desi.bank.creditcard.service.CreditCardApplicationService;
import com.desi.bank.creditcard.service.CreditCardService;
import com.desi.bank.creditcard.service.model.CreditCardApplicationVO;

@RestController
@RequestMapping("api/creditcards")
public class CreditCardRestApi {
	@Autowired
	private CreditCardApplicationService creditCardApplicationService;
	
	@RequestMapping(value="applications",produces="application/json")
	public List<CreditCardApplicationVO> findAllCreditCardApplication(){
		return creditCardApplicationService.findAll();
		
	}
	@RequestMapping(value="applications/{appRef}",produces="application/json")
	public CreditCardApplicationVO findCreditCardApplicationByAppRef(@PathVariable("appRef") String appRef) {
		return creditCardApplicationService.findByAppRef(appRef);
	}
	
	
	@RequestMapping(value="/applications/{appRef}/passportImages", produces = MediaType.IMAGE_JPEG_VALUE,method=RequestMethod.GET)
	public byte[] getCreditCardPassportImage(@PathVariable("appRef")String appRef, Model model) {
		System.out.println("appRef is"+appRef);
		return creditCardApplicationService.findPassportImageByAppRef(appRef);
	}
	
	@RequestMapping(value="/applications/{appRef}/approve",produces="text/plain",method=RequestMethod.POST)
	public ResponseEntity<String> approveCreditCardApplication(@PathVariable("appRef")String appRef, Model model) {

		creditCardApplicationService.approveCreditCardApplication(appRef);
		return new ResponseEntity<String>("success", HttpStatus.OK);
		
	}

	
	@RequestMapping(value="/applications/{appRef}/sendRejectionEmail",produces="text/plain",method=RequestMethod.POST)
	public ResponseEntity<String> rejectCreditCardApplication(@PathVariable("appRef")String appRef,@RequestBody Map<String, Object> payload,Model model) {

		String rejectionMessage=(String) payload.get("rejectionMessage");
		creditCardApplicationService.rejectAppliaction(appRef);
		creditCardApplicationService.sendRejectEmail(appRef,rejectionMessage);
		
		return new ResponseEntity<String>("success", HttpStatus.OK);
	}
	
	
	
	
	@RequestMapping(value="/applications/{appRef}/sendApprovalEmail",produces="text/plain",method=RequestMethod.POST)
	public ResponseEntity<String> sendCreditCardApplicationApprovalEmail(@PathVariable("appRef")String appRef, Model model) {
		
		CreditCardApplicationVO application=creditCardApplicationService.findByAppRef(appRef);
		if(application.getCardNumber()<1) {
			return new ResponseEntity<String>("credit card is not created yet", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
		try {
			creditCardApplicationService.sendCreditCardApplicationApprovalEmail(appRef);
			return new ResponseEntity<String>("success", HttpStatus.OK);
		}
		catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("failed", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	
	
	
}
