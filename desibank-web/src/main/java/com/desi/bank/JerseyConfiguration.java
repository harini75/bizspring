package com.desi.bank;


import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.desi.bank.rest.api.admin.BankAdminRestApi;
import com.desi.bank.rest.api.customer.BankCustomerRestApi;
import com.desi.bank.rest.api.employee.EmployeeRestApi;


/**
 * 
 * @author 
 * 
 */
@Configuration
@ApplicationPath("v1")
public class JerseyConfiguration extends ResourceConfig {
	public JerseyConfiguration() {
	}
	@PostConstruct
	public void setUp() {
		register(BankCustomerRestApi.class);
		register(EmployeeRestApi.class);
		register(BankAdminRestApi.class);
	}
}