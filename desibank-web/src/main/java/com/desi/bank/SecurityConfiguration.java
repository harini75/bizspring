package com.desi.bank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.desi.bank.common.service.impl.LoginFailureUserAuthHandler;
import com.desi.bank.common.service.impl.SpringSecurityService;
import com.desi.bank.customer.service.impl.UserSpringSecuirtyAuthProvider;

@Configuration 
@EnableWebSecurity 
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter { 
	
	
   @Autowired	
   private UserSpringSecuirtyAuthProvider userSpringSecurityAuthProvider;
   
	
	@Autowired
	@Qualifier("SpringSecurityServiceImpl")
	private SpringSecurityService springSecurityService;
   
   @Override
   protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userSpringSecurityAuthProvider).passwordEncoder(passwordEncorder());
   }
	// <bean id="bcryptEncoder" class="org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder" />
   
   
	@Bean
	public BCryptPasswordEncoder passwordEncorder() {
		return new BCryptPasswordEncoder();
	}

 
  @Override 
  protected void configure(HttpSecurity http) throws Exception {   
    http.authorizeRequests() 
        .antMatchers("/**").permitAll().antMatchers("/admin/**").hasAuthority("ROLE_ADMIN");; 
   http.csrf().disable();
    //for login
    http.formLogin().failureHandler(customAuthenticationFailureHandler())
    .loginPage("/corp/auth")
    .defaultSuccessUrl("/homePage")
    /*.failureUrl("/corp/auth?error=true")*/
    .and().exceptionHandling()
    .accessDeniedPage("/access-denied")
    .and()
    .logout().logoutUrl("/corp/logout")
    .logoutSuccessUrl("/logout/success")
    .invalidateHttpSession(true) 
    .permitAll()
    .deleteCookies("JSESSIONID");
    /* http.authorizeRequests()*/
    
 /*   .antMatchers("/", "/*.html").permitAll()
    .antMatchers("/user/**").hasRole("USER")
   
    .antMatchers("/admin/login").permitAll()
    .antMatchers("/user/login").permitAll()
    .anyRequest().authenticated()
    .and()
    .csrf().disable();*/
  } 
  
 
  @Bean
  public AuthenticationFailureHandler customAuthenticationFailureHandler() {
      return new LoginFailureUserAuthHandler(springSecurityService);
  }
  
}