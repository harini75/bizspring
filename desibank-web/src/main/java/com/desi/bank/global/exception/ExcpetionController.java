package com.desi.bank.global.exception;

import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
public class ExcpetionController implements ErrorController {

@RequestMapping("/error")
public String handleError(HttpServletResponse response) {
	
	String errorPage="error";

	if(response.getStatus() == HttpStatus.NOT_FOUND.value()) {
		errorPage="error-404";
	}
	else if(response.getStatus() == HttpStatus.FORBIDDEN.value()) {
		errorPage="error-403";
	}
	else if(response.getStatus() == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
		errorPage="error-500";
	}

    
    return errorPage;
}

@Override
public String getErrorPath() {
    return "/error";
}

}