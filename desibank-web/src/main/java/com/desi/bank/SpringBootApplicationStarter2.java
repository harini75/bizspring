package com.desi.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

class Customer{
	String name;
	String email;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}

@SpringBootApplication(scanBasePackages = {"com.desi.bank"})
public class SpringBootApplicationStarter2 {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootApplicationStarter2.class, args);
		
	/*	//java.util.stream.Stream<Integer> s=null;
	//	s.sorted(Comparator.comparing(z->z).reversed());
		List<String> codes = Arrays.asList ("DOC","MPEG", "JPEG");
		codes.forEach (c -> System.out.print(c + ""));
		String fmt = codes.stream().filter (s-> s.contains ("PEG")).reduce((s, t) -> s + t).get();
		System.out.println("\n" + fmt);
		
		IntStream intStream=IntStream.of(12,23,6,6,789);
		List<String> strings=intStream.mapToObj(x->String.valueOf(x)).collect(Collectors.toCollection(ArrayList::new));*/
	}
	
	 //remove _class
    @Bean
    public MongoTemplate mongoTemplate(MongoDbFactory mongoDbFactory,
                                       MongoMappingContext context) {
        MappingMongoConverter converter =
                new MappingMongoConverter(new DefaultDbRefResolver(mongoDbFactory), context);
        converter.setTypeMapper(new DefaultMongoTypeMapper(null));
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory, converter);
        return mongoTemplate;

    }
}
