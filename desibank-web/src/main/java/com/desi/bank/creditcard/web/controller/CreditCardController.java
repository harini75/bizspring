package com.desi.bank.creditcard.web.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import com.desi.bank.constant.DesiBankNavigationConstant;
import com.desi.bank.creditcard.dao.CreditCardApplicationDaoRepository;
import com.desi.bank.creditcard.dao.CreditCardFeatureDaoRepository;
import com.desi.bank.creditcard.dao.CreditCardTypeDaoRepository;
import com.desi.bank.creditcard.dao.entity.CardFeatureEntity;
import com.desi.bank.creditcard.dao.entity.CardTypeEntity;
import com.desi.bank.creditcard.service.CardFeatureService;
import com.desi.bank.creditcard.service.CardTypeService;
import com.desi.bank.creditcard.service.CreditCardApplicationService;
import com.desi.bank.creditcard.service.CreditCardService;
import com.desi.bank.creditcard.service.model.CreditCardApplicationVO;
import com.desi.bank.customer.service.impl.UserSpringSecuirtyAuthProvider;
import com.desi.bank.employee.service.EmployeeService;
import com.desi.bank.rest.api.employee.model.BasicCustomerInfo;
import com.desi.bank.rest.api.response.GenerateCreditCardResponse;

/**
 * 
 * @author nagendra
 *   This class is used to handle all the request coming from 
 *   the customer
 */
@Controller
@RequestMapping("/creditcards")
public class CreditCardController {
	
	@Autowired
	private CreditCardService creditCardService;
	@Autowired
	private CardTypeService cardTypeService;
	@Autowired
	private CardFeatureService cardFeatureService;
	
	@Autowired
	private CreditCardFeatureDaoRepository fdao; 
	
	@Autowired
	private EmployeeService employeeService;
	
	 @Autowired	
	 private UserSpringSecuirtyAuthProvider userSpringSecurityAuthProvider;
	 
	 @Autowired
	 private CreditCardApplicationService creditCardApplicationService;
	
	public CreditCardController(){
		System.out.println(")@)@)CreditCardController@)@");
		System.out.println(")@)@)CreditCardController@)@");
	}
	
	
//	@InitBinder
//	public void initBinder(WebDataBinder binder) {
//		binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
//	}
//	
	

	//generate image of default credit card
	@RequestMapping(value = "load-card-ccid", method = RequestMethod.GET)
	public void findCreditImageByCardTypeId(@RequestParam("ccno") int ccno, HttpServletResponse response) throws IOException {

		 byte  image[]=employeeService.generatedCreditCardByCcid(ccno);
		 response.setContentType("image/png");
		 ServletOutputStream outputStream=response.getOutputStream();
		 if(image==null){
			 image=new byte[]{};
		 }
		 outputStream.write(image);
		 outputStream.flush();
		 outputStream.close();
	}
	
	@RequestMapping(value="/"+DesiBankNavigationConstant.CREDIT_CARD_APPLICATION_PAGE,method=RequestMethod.GET)
	public String showCreditCardApplication(@RequestParam("cardType")String cardType,CreditCardApplicationVO creditCardApplicationVO,Model model) {
		model.addAttribute("cardType",cardType);
		return DesiBankNavigationConstant.CREDITCARDS_BASE+DesiBankNavigationConstant.CREDIT_CARD_APPLICATION_PAGE;
	}
	
	@RequestMapping(value="/"+DesiBankNavigationConstant.CREDIT_CARD_APPLICATION_PAGE,method=RequestMethod.POST)
	public String handleCreditCardApplication(@RequestParam("passportPhoto") MultipartFile passportPhoto,@ModelAttribute CreditCardApplicationVO creditCardApplicationVO,Model model) {
		
		String appRef=creditCardApplicationService.save(creditCardApplicationVO,passportPhoto);
		
		creditCardApplicationService.sendApplicationConfirmationEmail(creditCardApplicationVO,appRef);
		
		model.addAttribute("cardType", creditCardApplicationVO.getCardType());
		model.addAttribute("firstName", creditCardApplicationVO.getFirstName());
		model.addAttribute("lastName", creditCardApplicationVO.getLastName());
		model.addAttribute("appRef",appRef);
		
		return DesiBankNavigationConstant.CREDITCARDS_BASE+DesiBankNavigationConstant.CREDIT_CARD_APPLICATION_SUCCESS;
	}
	
	

//	@RequestMapping(value="/"+DesiBankNavigationConstant.CREDIT_CARD_APPLICATION_DETAIL,method=RequestMethod.GET)
//	public String handleCreditCardApplication(@RequestParam("cardType")String cardType, Model model) {
//		System.out.println("applying for"+cardType);	
//		model.addAttribute("cardType",cardType);
//		return DesiBankNavigationConstant.CREDITCARDS_BASE+DesiBankNavigationConstant.CREDIT_CARD_APPLICATION_DETAIL;
//	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String showAllCreditCards(Model model){
		System.out.println(cardTypeService.findAll());
		model.addAttribute("cardTypeList",cardTypeService.findAll());
		return DesiBankNavigationConstant.CREDITCARDS_BASE+DesiBankNavigationConstant.CREDIT_CARDS_PAGE;
	}
	
	
	@GetMapping("/thymeleaf")
    String thymeleafPage(Model model,@RequestParam String name) {
        model.addAttribute("name", name);
        return "thymeleaf/sample";
    }
}
