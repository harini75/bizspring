package com.desi.bank.employee.web.controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

import com.desi.bank.common.dao.entity.Login;
import com.desi.bank.common.dao.entity.SecurityQuestions;
import com.desi.bank.constant.DesiBankConstant;
import com.desi.bank.constant.DesiBankNavigationConstant;
import com.desi.bank.creditcard.service.CreditCardApplicationService;
import com.desi.bank.customer.service.CustomerService;
import com.desi.bank.customer.web.controller.form.CustomerForm;
import com.desi.bank.customer.web.controller.form.CustomerSavingForm;
import com.desi.bank.customer.web.controller.form.ProfileForm;
import com.desi.bank.data.util.DesiBankUtils;
import com.desi.bank.email.service.EmailMessageSenderService;
import com.desi.bank.email.service.EmailVO;
import com.desi.bank.email.service.ICustomerEmailService;
import com.desi.bank.employee.service.EmployeeService;
import com.desi.bank.employee.web.controller.form.ApplicationMessageResponse;
import com.desi.bank.employee.web.controller.form.EmployeeLoginForm;
import com.desi.bank.employee.web.controller.form.RegistrationLinksForm;
import com.desi.bank.employee.web.controller.form.RejectSavingRequestForm;
import com.desi.bank.util.UniqueLinkGenaratorUtil;
import com.spring.model.UserSessionVO;

/**
 * 
 * @author nagendra
 *
 */
@Controller
@RequestMapping(value="/employee")
public class BankEmployeeController {
	
	/**
	 * Initiate Logger for this class
	 */
	private static final Log logger = LogFactory.getLog(BankEmployeeController.class);
	
	
	@Value("${registration.link.validity.hrs}")
	  private String  linkvalidity;
	
	@Autowired
	@Qualifier ("EmailMessageSenderService")
	private EmailMessageSenderService emailMessageSenderService;
	
	
	@Autowired
	@Qualifier ("EmployeeServiceImpl")
	private EmployeeService employeeService;
	
	@Autowired
	private CreditCardApplicationService creditCardApplicationService;
	
	@Autowired
	@Qualifier("CustomerEmailService")
	private ICustomerEmailService customerEmailService;
	
	@Autowired
	@Qualifier("CustomerServiceImpl")
	public CustomerService customerService;
	

	
	@RequestMapping(value="accounts/login",method=RequestMethod.GET)
	public String loginCreationPage(@RequestParam("token") String token,Model model) {
		
		  Optional<Login> optional=employeeService.validateToken(token);
		  if(optional.isPresent()) {
			  Login login =optional.get();
			  String email=customerService.findEmailByUserid(login.getLoginid());
			  model.addAttribute("loginid",login.getLoginid());
			  model.addAttribute("email",email);
		  }
		  //write logic to validate the token
		  List<SecurityQuestions> securityQuestions1=customerService.securityQns();
		  model.addAttribute("securityQuestions1", securityQuestions1);
		  List<SecurityQuestions> securityQuestions2=customerService.securityQns2();
		  model.addAttribute("securityQuestions2", securityQuestions2);
		return DesiBankNavigationConstant.EMPLOYEE_BASE+DesiBankNavigationConstant.LOGIN_CREATION_PAGE;
	}
	
	@RequestMapping(value="accounts/login",method=RequestMethod.POST)
	public String loginCreatioPost(@ModelAttribute EmployeeLoginForm employeeLoginForm,Model model) {
		if(employeeLoginForm.getCurrentPassword()!=null && employeeLoginForm.getCurrentPassword().equals(employeeLoginForm.getConfirmPassword())) {
			employeeService.createEmployeeLogin(employeeLoginForm);
		}else {
			
		}
		model.addAttribute("errorMessage", "Your login is created sucessfully , thanks!");
		return DesiBankNavigationConstant.ADMIN_BASE+DesiBankNavigationConstant.LOGIN_PAGE;
	}


	//Spring Bean Life cycle 
	//Aware interface
	@PostConstruct
	public void checkValue(){
		System.out.println("linkvalidity = "+linkvalidity);
		System.out.println("linkvalidity = "+linkvalidity);
		System.out.println("linkvalidity = "+linkvalidity);
		System.out.println("linkvalidity = "+linkvalidity);
		
		if(logger.isDebugEnabled()) {
			   logger.debug("linkvalidity = "+linkvalidity);
		}
	}
	
	@RequestMapping(value="/creditCardDashboard",method=RequestMethod.GET)
	public String getCreditCardDashBoard(Model model) {
//		model.addAttribute("appList", creditCardApplicationService.findAll());
		return "employee/creditCardDashboard";
	}
	
	
	@RequestMapping(value="/creditCardApplicationApprovePage/{appRef}",method=RequestMethod.GET)
	public String getCreditCardApprovePage(@PathVariable("appRef")String appRef, Model model) {
		model.addAttribute("appRef",appRef);
		return "employee/creditCardApplicationApprovePage";
	}
	
	@RequestMapping(value="/creditCardApplicationRejectPage/{appRef}",method=RequestMethod.GET)
	public String getCreditCardRejectPage(@PathVariable("appRef")String appRef, Model model) {
		model.addAttribute("appRef",appRef);
		return "employee/creditCardApplicationRejectPage";
	}
	
	
	@RequestMapping(value="temp",method=RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
	@ResponseBody public ProfileForm temp(){
		
		ProfileForm form=new ProfileForm();
		form.setEmail("anegn@gmail.com");
		form.setUserid("opopopopopop");
		return form;
		
	}
	
	@RequestMapping(value="updateProfileImage.htm",method=RequestMethod.POST)
	@ResponseBody public String updateProfileImage(@ModelAttribute ProfileForm profileForm,HttpSession session,Model  model){
		UserSessionVO userSessionVO= (UserSessionVO) session.getAttribute(DesiBankConstant.USER_SESSION_DATA);
		String userid=userSessionVO.getLoginid();
		employeeService.updateProfilePicByUserid(userid,profileForm.getImage());
		return "success";
	}
	
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		// to actually be able to convert Multipart instance to byte[]
		// we have to register a custom editor
		binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
		// now Spring knows how to handle multipart object and convert them
	}
	
	@RequestMapping(value="lockUnlockCustomer",method=RequestMethod.GET)
	@ResponseBody public String lockUnlockCustomer(@RequestParam("lockStatus") String lockStatus,@RequestParam("loginid") String loginid,Model  model){
		String resultStatus=employeeService.lockUnlockCustomer(loginid,lockStatus);
		return resultStatus;
	}
	
	
	@RequestMapping(value="rejectSavingRequest",method=RequestMethod.POST)
	@ResponseBody public String rejectSavingSavingAccountRequest(@ModelAttribute RejectSavingRequestForm rejectSavingRequestForm,HttpServletRequest request,Model  model){
		if(logger.isDebugEnabled()) {
			logger.debug("Coming data from the client "+rejectSavingRequestForm);
			String userAgent=request.getHeader("User-Agent");
			logger.debug("UserAgent =  "+userAgent);
		}
		
		String result=employeeService.rejectSavingAccountRequests(rejectSavingRequestForm);
		ApplicationMessageResponse applicationMessageResponse=new ApplicationMessageResponse();
		applicationMessageResponse.setStatus(DesiBankConstant.SUCCESS);
		 //write the code
		if(logger.isInfoEnabled()) {
			logger.info("Please wait.....email is sending........................................");
		}
		
		try {
			EmailVO mail = new EmailVO();
			mail.setBaseUrl(DesiBankUtils.getServerBaseURL(request));
			mail.setFrom("desibankproject@gmail.com");
			mail.setName(rejectSavingRequestForm.getCustomerName());
			mail.setSubject("Regarding rejection to open a saving account");
			mail.setTo(rejectSavingRequestForm.getEmail());
			//TDD
			//customerEmailService.sendRejectSavingRequestEmail(mail, rejectSavingRequestForm.getReason());
		} catch (Exception ex) {
			StringWriter stringWriter = new StringWriter();
			ex.printStackTrace(new PrintWriter(stringWriter));
			if (logger.isErrorEnabled()) {
				logger.error(stringWriter.toString());
			}
		}
		
		int pendingRequestCount=employeeService.findPendingSavingAccountRequestsCount();
		String response = "{\"result\":\"success\", \"pendingRequestCount\":" + pendingRequestCount + "}";
		return response;
		
	/*	SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("nagen@synergisticit.com");
        message.setTo(rejectSavingRequestForm.getEmail());
        message.setSubject("Regarding rejection of your openining saving account request!");
        message.setText("Reason of rejection :  = "+rejectSavingRequestForm.getReason());
        mailSender.send(message);*/
//		TestVO testVO=new TestVO();
//		testVO.setFrom("desibankproject@gmail.com");
//		testVO.setName("DesiBank");
//		testVO.setSubject("Regarding Message Rejection");
//		testVO.setMessage("Testing testing testing");
//		testVO.setTo(rejectSavingRequestForm.getEmail());
//		emailMessageSenderService.sendSampleData(testVO);
		/*System.out.println("___message has beeb send to the message broker!~~~~~~~~~~~~~~~~~~~~~~");
		EmailVO mail=new EmailVO();
		mail.setBaseUrl(DesiBankUtils.getServerBaseURL(request));
		mail.setFrom("desibankproject@gmail.com");
		mail.setLink("www.gmail.com/registration/link?cid=ertret345ert533634ge64");
		mail.setName(rejectSavingRequestForm.getCustomerName());
		mail.setSubject("Regarding rejection of your openining saving account request!");
		mail.setTo(rejectSavingRequestForm.getEmail());
		if(logger.isDebugEnabled()) {
			   logger.debug("mail = "+mail);
		}
		customerEmailService.sendRegistrationEmail(mail);*/
//		 return "success";
	}
	
	@RequestMapping(value="pendingSavingRequest",method=RequestMethod.GET)
	public String showPendingSavingAccountRequest(Model  model){
		List<CustomerSavingForm> customerSavingFormList=employeeService.findPendingSavingAccountRequests();
		model.addAttribute("customerSavingFormList", customerSavingFormList);
		
		int pendingRequestCount=employeeService.findPendingSavingAccountRequestsCount();
		model.addAttribute("pendingRequestCount", pendingRequestCount);
		
		return DesiBankNavigationConstant.EMPLOYEE_BASE+DesiBankNavigationConstant.PENDING_SAVING_REQUESTS_PAGE;
	}
	
	@RequestMapping(value="pendingSavingAccountRequests",method=RequestMethod.GET)
	public String showPendingSavingAccountRequests(Model  model){
		List<CustomerForm> customerFormList=employeeService.findPendingSavingAccountApprovalRequests();
		model.addAttribute("customerFormList", customerFormList);
		 return DesiBankNavigationConstant.EMPLOYEE_BASE+DesiBankNavigationConstant.PENDING_SAVING_ACCOUNT_REQUESTS_PAGE;
	}
	
	/**
	 *  This is showing page for already approved customer accounts
	 * @param model
	 * @return
	 */
	@RequestMapping(value="approvedCustomersAccount",method=RequestMethod.GET)
	public String approvedCustomersAccount(Model  model){
		List<CustomerForm> customerFormList=employeeService.findSavingApprovedAccount();
		model.addAttribute("customerFormList", customerFormList);
		 return DesiBankNavigationConstant.EMPLOYEE_BASE+DesiBankNavigationConstant.APPROVED_CUSTOMERS_ACCOUNT_PAGE;
	}
	
	@ModelAttribute("filterOptions")
	public List<String> filterOptions(){
		List<String> list=new ArrayList<String>();
		list.add("Select");
		list.add("Today");
		list.add("Yesterday");
		list.add("Last 3 days");
		list.add("Last 5 days");
		list.add("Last Week");
		list.add("All");
		return list;
	}
	@RequestMapping(value="sendMessageToCustomerRequest",method=RequestMethod.POST)
	@ResponseBody public String sendMessageToCustomerRequest(@ModelAttribute EmailVO mail,HttpServletRequest request,Model  model){
			mail.setFrom("desibankproject@gmail.com");
			mail.setSubject("This is message from the Bank");
		    customerEmailService.sendCustomerMessageEmail(mail);
		    String response = "{\"result\":\"success\", \"message\":\"Email message has been sent\"}";
	       	return response;
		
	
	}
	@RequestMapping(value="approveSavingRequest",method=RequestMethod.POST)
		@ResponseBody	public String approveSavingRequest(@ModelAttribute RejectSavingRequestForm approveSavingRequestForm,HttpServletRequest request,Model  model,HttpSession session){
		UserSessionVO userSessionVO= (UserSessionVO) session.getAttribute(DesiBankConstant.USER_SESSION_DATA);
		String userid=userSessionVO.getLoginid();
		System.out.println(approveSavingRequestForm);
		approveSavingRequestForm.setActionBy(userid);
		employeeService.savingApproveAccountRequests(approveSavingRequestForm);
		//We have to generate unique link
		
		//http://localhost/desi-bank
		String baseURL=DesiBankUtils.getServerBaseURL(request)+"/"+DesiBankConstant.REGISTRATION_URL;
		String cuid=UniqueLinkGenaratorUtil.findUniqueCuid();
		String registrationLink=baseURL+cuid;
		RegistrationLinksForm  linksForm=new RegistrationLinksForm();
		linksForm.setComment("Link is generated");
		Timestamp currentTime=new Timestamp(new Date().getTime());
		linksForm.setDoe(currentTime);
		linksForm.setEmail(approveSavingRequestForm.getEmail());
		linksForm.setExphrs(Integer.parseInt(linkvalidity));  //This value should come from properties file................
		Calendar c = Calendar.getInstance();
        c.setTime(currentTime);
        c.add(Calendar.HOUR, Integer.parseInt(linkvalidity));
        Date ExpirationDate = c.getTime();
        Timestamp linkexpdate=new Timestamp(ExpirationDate.getTime());
        linksForm.setLinkexpiredate(linkexpdate);
		linksForm.setLinkurl(cuid);
		if(logger.isDebugEnabled()) {
			   logger.debug("linksForm = "+linksForm);
		}
		employeeService.saveRegistrationLink(linksForm);
		
		try {
					EmailVO mail=new EmailVO();
					mail.setBaseUrl(DesiBankUtils.getServerBaseURL(request));
					mail.setFrom("desibankproject@gmail.com");
					mail.setLink(registrationLink);
					mail.setName(approveSavingRequestForm.getCustomerName());
					mail.setSubject("Regarding Registration link to open a saving account");
					mail.setTo(approveSavingRequestForm.getEmail());
					//TDD
					customerEmailService.sendRegistrationEmail(mail);
		}catch(Exception ex) {
			StringWriter stringWriter=new StringWriter();
			ex.printStackTrace(new PrintWriter(stringWriter));
			if(logger.isErrorEnabled()){
				logger.error(stringWriter.toString());
			}
		}
		ApplicationMessageResponse applicationMessageResponse=new ApplicationMessageResponse();
		applicationMessageResponse.setStatus("success");
		applicationMessageResponse.setMessage("Hey this request has been approved seuccessfully.....................");
		//We can convert 
//		return "success";

		int pendingRequestCount=employeeService.findPendingSavingAccountRequestsCount();
		String response = "{\"result\":\"success\", \"pendingRequestCount\":" + pendingRequestCount + "}";
		return response;
	}

}
