package com.desi.bank.customer.web.controller;
/*package com.desi.bank.customer.web.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.desi.bank.constant.DesiBankNavigationConstant;
import com.desi.bank.customer.report.model.BankStatement;
import com.desi.bank.customer.service.CustomerService;
import com.desi.bank.customer.service.model.TransactionHistoryVO;
import com.desi.bank.customer.web.controller.form.CustomerAccountInfoVO;
import com.desi.bank.customer.web.controller.form.CustomerForm;
import com.spring.model.UserSessionVO;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Controller
public class CustomerReportController {

	*//**
	 * Initiate Logger for this class
	 *//*
	private static final Log logger = LogFactory.getLog(CustomerReportController.class);

	@Autowired
	@Qualifier("CustomerServiceImpl")
	public CustomerService customerService;

	@RequestMapping(value = "xls/customerReports.xls", method = RequestMethod.GET)
	public String showCustomerDetailsReportxls(Model model, HttpServletResponse response) {
		List<CustomerForm> customerForms = customerService.findCustomers();
		JRDataSource jrDataSource = new JRBeanCollectionDataSource(customerForms);
		model.addAttribute("jasperDataSource", jrDataSource);
		return "xlsReport";
	}

	@RequestMapping(value = "pdf/customerReports", method = RequestMethod.GET)
	public String showCustomerDetailsReport(Model model) {
		List<CustomerForm> customerForms = customerService.findCustomers();
		JRDataSource jrDataSource = new JRBeanCollectionDataSource(customerForms);
		model.addAttribute("jasperDataSource", jrDataSource);
		return "pdfReport";
	}

	@RequestMapping(value = "html/customerReports", method = RequestMethod.GET)
	public String showCustomerDetailsReporthtml(Model model) {
		List<CustomerForm> customerForms = customerService.findCustomers();
		JRDataSource jrDataSource = new JRBeanCollectionDataSource(customerForms);
		model.addAttribute("jasperDataSource", jrDataSource);
		return "htmlReport";
	}
	
	@RequestMapping(value = "xls/bank-statement.xls", method = RequestMethod.GET)
	public String generateXlsBankStatementReport(@RequestParam("accountNumber") String accountNumber,
			@RequestParam(name = "from", required = false) String from,
			@RequestParam(name = "to", required = false) String to, Model model, HttpSession session) {

		String userid = ((UserSessionVO)session.getAttribute("userSessionVO")).getLoginid();
		BankStatement bankStatement = generateBankStatementReport(accountNumber, from, to);
		
		if (bankStatement.getAccount().getCustomerId().equals(userid)) {
			List<BankStatement> dataSource = new ArrayList<>();
			dataSource.add(bankStatement);
			JRDataSource jrDataSource = new JRBeanCollectionDataSource(dataSource);
			model.addAttribute("jasperDataSource", jrDataSource);
			
			return "bankStatementXlsReport";
		} else {
			return DesiBankNavigationConstant.CUSTOMER_BASE+DesiBankNavigationConstant.CUSTOMER_HOME_PAGE;
		}
	}

	@RequestMapping(value = "pdf/bank-statement.pdf", method = RequestMethod.GET)
	public String generatePdfBankStatementReport(@RequestParam("accountNumber") String accountNumber,
			@RequestParam(name = "from", required = false) String from,
			@RequestParam(name = "to", required = false) String to, Model model, HttpSession session) {

		String userid = ((UserSessionVO)session.getAttribute("userSessionVO")).getLoginid();
		BankStatement bankStatement = generateBankStatementReport(accountNumber, from, to);
		
		if (bankStatement.getAccount().getCustomerId().equals(userid)) {
			List<BankStatement> dataSource = new ArrayList<>();
			dataSource.add(bankStatement);
			JRDataSource jrDataSource = new JRBeanCollectionDataSource(dataSource);
			model.addAttribute("jasperDataSource", jrDataSource);
			
			return "bankStatementPdfReport";
		} else {
			return DesiBankNavigationConstant.CUSTOMER_BASE+DesiBankNavigationConstant.CUSTOMER_HOME_PAGE;
		}
	}
	
	private BankStatement generateBankStatementReport(String accountNumber, String from, String to) {
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		Date fromDate = null, toDate = null;

		try {
			if (from != null) {
				fromDate = format.parse(from);
			}

			if (to != null) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(format.parse(to));
				calendar.add(Calendar.DATE, 1);

				toDate = calendar.getTime();
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		CustomerAccountInfoVO account = customerService.getAccountByAccountNumber(accountNumber);
		account.setAccountType(account.getAccountType() + " Account");

		// should create new method which does not consider the limit when generating
		// report
		int total = Math
				.toIntExact(customerService.countTraverseTransactionsByAccountNumber(accountNumber, fromDate, toDate));
		List<TransactionHistoryVO> transactions = customerService
				.traverseTransactionsHistoryByAccountNumber(accountNumber, total, 0, fromDate, toDate);

		BankStatement bankStatement = new BankStatement();
		bankStatement.setAccount(account);
		bankStatement.setTransactions(transactions);
		
		return bankStatement;
	}
}
*/