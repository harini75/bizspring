package com.desi.bank.customer.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.desi.bank.common.service.AccountProcessingStepService;
import com.desi.bank.common.service.SalutationService;
import com.desi.bank.customer.service.model.AccountProcessingStepVO;


@Controller
public class DesibankDashboardController {
	
	
	@Autowired
	private AccountProcessingStepService accountProcessingStepService;
	
	@Autowired
	private SalutationService salutationService;

	@GetMapping(value={"/","/home"})
	public String homePage(Model model){
		
		
		//fetch account processing  data
		List<AccountProcessingStepVO> accountProcessingStepVOs=accountProcessingStepService.findAccountProcessingSteps();
		model.addAttribute("accountProcessingStepVOs", accountProcessingStepVOs);
		return "index"; //WEB-INF/jsp
	}
	
	
	@ModelAttribute("salutionsList")
	public List<String> loadSalutations(){
		return salutationService.findSalutation();
	}
}
