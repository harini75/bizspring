

public class CompareValues {
	public static void main(String[] args) {
		 int x=2;
		 int count=x++ * x++ * x++; // C, C++ =8
		 //Java =24
		 System.out.println(count);
		 long y = x * 4 - x++ * x++ ;  //5*4-5
		 System.out.println(y);
		 System.out.println("x = "+x+" , y = "+y);
		 
		 x=10;
		 //int p = x + x + x * x++ * x++ + x +x;
		 int p = x + x + x +x * x++ * x++ + x +x +x+x+x ;
		 System.out.println("p =" +p);
		 
		 int g=2;
		 long temp = g * 4 - g++ * g++ +g +g*4 + g++ +g;  //5*4-5
		 System.out.println(temp);
		           //= 2*4 - 2 * 3 +4 +4*4 + 4 +5 = g=5
		           // = 8-6 +4+16 +4+5
		           // = 2+20+9 =31

	}
}