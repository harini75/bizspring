public class InstanceBlock {
		final String value1 = "1";
		static String value2 = "2";
		String value3 = "3";
		{
		// CODE SNIPPET 1
			 value2 = "e"; 
		}
		static {
			// CODE SNIPPET 2
			System.out.println("Hey this is static block");
		}
		public static void main(String[] args) {
		}
		
	}
