

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

public class Sum extends RecursiveAction { // line n1
	static final int THRESHOLD_SIZE = 3;
	int stIndex, lstIndex;
	int[] data;
	 //keep static
	 static int sum; 
	public Sum(int[] data, int start, int end) {
		this.data = data;
		this.stIndex = start;
		this.lstIndex = end;
	}

	protected void compute() {
		System.out.println(Thread.currentThread());
		if ((lstIndex - stIndex) <= THRESHOLD_SIZE) {
			for (int i = stIndex; i < lstIndex; i++) {
				sum += data[i];
			}
			System.out.println(sum);
		} else {
			//System.out.println("cool!"+sum);
			int middle=stIndex+((lstIndex-stIndex)/2);
			System.out.println("middle = "+middle);
			Sum t1=new Sum(data, middle, lstIndex);
			t1.fork();
			new Sum(data, stIndex,middle).compute();
			t1.join();
		
			//Sum t1=new Sum(data, stIndex + THRESHOLD_SIZE, lstIndex);
			//Sum t2=new Sum(data, stIndex, Math.min(lstIndex, stIndex + THRESHOLD_SIZE));
			/*t1.fork();
			t2.fork();
			t1.join();
			t2.join();*/
			//invokeAll(t1, t2);
		}
	}
	
	public static void main(String[] args) {
		ForkJoinPool fjPool = new ForkJoinPool ( );
		int data [ ] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		fjPool.invoke (new Sum (data, 0, data.length));
		System.out.println("Sum.................");
		//System.out.println(Sum.sum);
	}
}
