var criteria = new TraverseCriteria();

$(document).ready(function() {
	init();
	
	$('.account-transactions__filter-btn').click(function(e) {
		$('.account-transactions__filter-controller').slideToggle();
	});

	$('.back-btn').click(function(e) {
		window.history.back();
	});

	$('.desibak-pagination__controller__prev-btn').click(function(e) {
		let offset = getOffset();
		let limit = getLimit();

		if (offset == 0) {
			alertMsg('Warning!!!', 'Cannot fecth more data');
			return;
		} else if (offset - limit < 0) {
			offset = 0;
		} else {
			offset -= limit;
		}

		getTransactionsHistory(offset, limit, criteria.from, criteria.to);
	});

	$('.desibak-pagination__controller__next-btn').click(function(e) {
		let offset = getOffset();
		let limit = getLimit();
		let total = getTotal();

		if (offset + limit >= total) {
			alertMsg('Warning!!!', 'Cannot fecth more data');
			return;
		} else {
			offset += limit;
		}

		getTransactionsHistory(offset, limit, criteria.from, criteria.to);
	});

	$('#filter-submit-btn').click(function() {
		let offset = getOffset();
		let limit = getLimit();

		getTransactionsHistory(offset, limit, getFromDate(), getToDate());
	});

	$('#pdf-report').click(function() {		
		var url = '/desibank-web/pdf/bank-statement.pdf?accountNumber=' + getAccountNumber();
		
		if (criteria.from != null && criteria.to != null) {
			url += '&from=' + criteria.from;
			url += '&to=' + criteria.to;
		}
		
		window.open(url, '_blank');
	});
	
	$('#xls-report').click(function() {		
		var url = '/desibank-web/xls/bank-statement.xls?accountNumber=' + getAccountNumber();
		
		if (criteria.from != null && criteria.to != null) {
			url += '&from=' + criteria.from;
			url += '&to=' + criteria.to;
		}
		
		window.open(url, '_blank');
	});
});

function getTransactionsHistory(offset, limit, from, to) {
	params = {
		accountNumber : getAccountNumber(),
		limit : limit,
		offset : offset
	}

	if (from != null && to != null) {
		params['from'] = from;
		params['to'] = to;
	} else if (from != null || to != null) {
		alertMsg('Warning', 'Both From(Date) and To(Date) must have values.');
	}

	$.ajax({
		type : 'GET',
		url : '/desibank-web/v1/customer/transations-history',
		data : params,
		dataType : 'json',
		success : function(data) {
			generateTransactionsHistoryTable(data.transactions);

			criteria.offset = data.offset;
			criteria.limit = data.limit;
			criteria.total = data.total;

			criteria.from = params.from;
			criteria.to = params.to;

			updatePaginationLabel(data.offset, data.transactions.length,
					data.total);
		}
	});
}

function init() {
	criteria.limit = getLimit();
	criteria.offset = getOffset();
	criteria.from = getFromDate();
	criteria.to = getToDate();
}

function updatePaginationLabel(offset, size, total) {
	$('#desibak-pagination-start').html(offset + 1);
	$('#desibak-pagination-end').html(offset + size);
	$('#desibak-pagination-total').html(total)
}

function generateTransactionsHistoryTable(transactions) {
	var accountNumber = getAccountNumber();
	var body = $('.account-transactions__table__body');
	body.empty();

	transactions
			.forEach(function(transaction, index) {
				var row = $('<tr>');

				row.append('<td class="account-transactions__table__date">'
						+ toMMDDYYYY(transaction.date) + '</td>');
				row.append('<td>' + transaction.description + '</td>');

				if (transaction.toAccountNumber == accountNumber) {
					row
							.append('<td class="account-transactions__table__amount--credit">+'
									+ transaction.amount + '</td>');
					row
							.append('<td class="account-transactions__table__amount--debit"></td>');
				} else {
					row
							.append('<td class="account-transactions__table__amount--credit"></td>');
					row
							.append('<td class="account-transactions__table__amount--debit">'
									+ transaction.amount + '</td>');
				}

				body.append(row);
			});
}

function toMMDDYYYY(timestamp) {
	var date = new Date(timestamp);

	var m = ('0' + (date.getMonth() + 1)).substr(-2);
	var d = ('0' + date.getDate()).substr(-2);
	var y = date.getFullYear();

	return m + '/' + d + '/' + y;
}

function getAccountNumber() {
	return $('.account-detail__title__account-no').html();
}

function getOffset() {
	let start = $('#desibak-pagination-start').html();
	return parseInt(start) - 1;
}

function getLimit() {
	return parseInt($('#records-per-page').val());
}

function getTotal() {
	return parseInt($('#desibak-pagination-total').html());
}

function getFromDate() {
	let date = $('#filter-from').val();

	if (date == "")
		return null;

	let splitedDate = date.split("-");

	return '${splitedDate[1]}/${splitedDate[2]}/${splitedDate[0]}'';
}

function getToDate() {
	let date = $('#filter-to').val();

	if (date == "")
		return null;

	let splitedDate = date.split("-");

	return '${splitedDate[1]}/${splitedDate[2]}/${splitedDate[0]}';
}

function alertMsg(title, msg) {
	$('.alert-modal .modal-title').html(title);
	$('.alert-modal .modal-body').html(msg);
	$('.alert-modal').modal('show');
}

function TraverseCriteria() {
	this.offset;
	this.limit;
	this.total;
	this.from;
	this.to
}
