<html lang="en">
<!-- Head -->
<head>
<title>Desi Bank Employee Credit Approve Page</title>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords"
	content="Corporate Bank a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- .css files -->
<link href="${pageContext.request.contextPath}/css/bars.css"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	type="text/css" media="all" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/nstyle.css"
	type="text/css" media="all" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font-awesome.css" />
<!-- //.css files -->
<!-- Default-JavaScript-File -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<!-- //Default-JavaScript-File -->
<!-- fonts -->
<link
	href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
	rel="stylesheet">
<link
	href="//fonts.googleapis.com/css?family=Ropa+Sans:400,400i&amp;subset=latin-ext"
	rel="stylesheet">
<!-- //fonts -->
<!-- scrolling script -->
<script type="text/javascript">



	$( document ).ready(function() {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 1000);
		});
	
		$("#tableTitle").append("Credit Card Application Approval Page for <span>${appRef}</span>");
		fetchApplication();

		$("#saveApplicationButton").click(function(){
			$(this).attr("disabled",true);
			$(".myLoader").show();
			$.post("${pageContext.request.contextPath}/api/creditcards/applications/${appRef}/approve",function(data,status){
				$("#saveApplicationButton").attr("disabled", false);
				$(".myLoader").fadeOut();
				if(data==="success"){
					alert("credit card application saved sucessfully.");
				}else{
					alert("credit card application failed to save.");
				}
			}).fail(function(response){
				$(".myLoader").fadeOut();
				$("#saveApplicationButton").attr("disabled", false);
				alert("saving failed");
			}).always(function(){
				fetchApplication();
			});
		});
		
		
		$("#sendEmailButton").click(function(){
			$("#sendEmailButton").attr("disabled",true);
			$(".myLoader").show();
			$.post("${pageContext.request.contextPath}/api/creditcards/applications/${appRef}/sendApprovalEmail",function(data,status){
				$("#sendEmailButton").attr("disabled", false);
				$(".myLoader").fadeOut();
				if(data==="success"){
					alert("credit card application approval email has been sent.");
				}else{
					alert("credit card application approval email failed to send.");
				}
			}).fail(function(response){
				$(".myLoader").fadeOut();
				$("#sendEmailButton").attr("disabled", false);
				alert("sending email failed :"+response.responseText);
			}).always(function(){
				fetchApplication();
			});
		});
		
		
	});
	
	
	function fetchApplication(){
 		$(".myLoader").fadeIn();
		$("#applicationTable").hide();
		$.getJSON("${pageContext.request.contextPath}/api/creditcards/applications/${appRef}",function(application){
			
			
			for (let p in application){
				//clear old record when refetching
				$("#"+p).next().remove();
				//add new records
				$("#"+p).after("<td>"+application[p]+"</td>");
				
			}
			$("#passportPhoto").next().remove();
			$("#passportPhoto").after("<td><img width='300px' height='200px' src='${pageContext.request.contextPath}/api/creditcards/applications/"+application.appRef+"/passportImages'></img></td>");
			$("#applicationTable").show();
			$(".myLoader").fadeOut();
	});
	}
	
</script>
<style>
.zoom {
	padding: 4px;
	transition: transform .2s; /* Animation */
	height: 80px;
	margin: 0 auto;
}

.zoom:hover {
	transform: scale(2.0);
	/* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}

.myLoader {
  border: 16px solid #f3f3f3; /* Light grey */
  position:relative;
  margin-top:10%;
  position: absolute;
  left: 0;	
  right: 0;
  margin-left: auto;
  margin-right: auto;
  border-top: 16px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}


</style>
<!-- //scrolling script -->
</head>
<!-- //Head -->
<!-- Body -->
<body>

	<div class="top-main">
		<div class="number">
			<h3>
				<i class="fa fa-phone" aria-hidden="true"></i> +91 080 987 6541
			</h3>
			<div class="clearfix"></div>
		</div>
		<div class="social-icons">
			<ul class="top-icons">
				<li><a href="#"><i class="fa fa-facebook"
						aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-google-plus"
						aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-pinterest-p"
						aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-linkedin"
						aria-hidden="true"></i></a></li>
			</ul>
			<div class="form-top">
				<form action="#" method="post" class="navbar-form navbar-left">
					<div class="form-group">
						<input type="search" class="form-control" placeholder="Search">
					</div>
					<button type="submit" class="btn btn-default">
						<i class="fa fa-search" aria-hidden="true"></i>
					</button>
					<!-- <button type="submit" class="btn btn-default">Submit</button> -->
				</form>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<!-- Top-Bar -->

	<jsp:include page="employee-menu.jsp" />

	<!-- body -->
	<div class="container">
	
	<div class="myLoader"></div>
		<table id="applicationTable" class="table table-striped table-bordered table-condensed table-hover" style="width: 800px">
		<caption id="tableTitle"></caption>
		<tr><td id="appRef">Application Reference #</td></tr>
		<tr><td id="applyDate">Apply Date</td></tr>
		<tr><td id="applicationStatus">Application Status</td></tr>
		<tr><td id="firstName">First Name</td></tr>
		<tr><td id="lastName">Last Name</td></tr>
		<tr><td id=cardType>Card Type</td></tr>
		<tr><td id="dob">Date of Birth</td></tr>
		<tr><td id="citizenship">Citizenship</td></tr>
		<tr><td id="phoneNumber">Phone Number</td></tr>
		<tr><td id="email">Email</td></tr>
		<tr><td id="ssn">Social Security Number</td></tr>
		<tr><td id="zip">ZIP number</td></tr>
		<tr><td id="passportPhoto">Passport Photo</td></tr>
		<tr><td id="address">Address</td></tr>
		<tr><td id="monthLived">Month Lived</td></tr>
		<tr><td id="housingStatus">Housing Status</td></tr>
		<tr><td id="monthlyPayment">Monthly Payment</td></tr>
		<tr><td id="employmentStatus">Employment Status</td></tr>
		<tr><td id="annualIncome">Annual Income</td></tr>
		<tr><td id="cardNumber">Credit Card Number If Have Approved</td></tr>
		</table>
	<div class="row">
	<div class="col-md-6 bm-auto"><button  id="saveApplicationButton"  class="btn btn-primary">Save</button></div><div  class="col-md-6"><button id="sendEmailButton" class="btn btn-success">Send Email</button></div>
	</div>
	</div>


	<!-- modal -->
	<div class="modal about-modal fade" id="myModal" tabindex="-1"
		role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span class="span1" aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">
						Corporate<span> Bank</span>
					</h4>
				</div>
				<div class="modal-body">
					<div class="agileits-w3layouts-info">
						<img src="images/business.jpg" alt="" />
						<p>Duis venenatis, turpis eu bibendum porttitor, sapien quam
							ultricies tellus, ac rhoncus risus odio eget nunc. Pellentesque
							ac fermentum diam. Integer eu facilisis nunc, a iaculis felis.
							Pellentesque pellentesque tempor enim, in dapibus turpis
							porttitor quis. Suspendisse ultrices hendrerit massa. Nam id
							metus id tellus ultrices ullamcorper. Cras tempor massa luctus,
							varius lacus sit amet, blandit lorem. Duis auctor in tortor sed
							tristique. Proin sed finibus sem.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //modal -->


	<!-- footer -->
	<jsp:include page="../common/footer.jsp" />
	<!-- //footer -->

	<!-- copyright -->
	<jsp:include page="../common/copyright.jsp" />
	<!-- //copyright -->

	<script src="js/jarallax.js"></script>
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed : 0.5,
			imgWidth : 1366,
			imgHeight : 768
		})
	</script>

	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/move-top.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/easing.js"></script>

	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		function openPopup() {
			$("#editProfileModel").modal("show");
		}
		$(document)
				.ready(
						function() {

							$
									.getJSON(
											"http://localhost:444/desibank-web/employee/temp",
											function(data) {
												console.log(data);

											});
							$('#UpdateProfile')
									.click(
											function() {
												var urlAddress = "${pageContext.request.contextPath}/employee/updateProfileImage";
												var formData = new FormData(
														document
																.getElementById('editProfileForm'));
												$
														.ajax({
															url : urlAddress,
															type : "POST",
															dataType : 'text',
															data : formData,
															processData : false,
															contentType : false,
															success : function(
																	result) {
																console
																		.log(result);
																$(
																		'#editProfileModel')
																		.modal(
																				'hide');
																//update profile image on GUI
																$(
																		'#loggedProfileImage')
																		.attr(
																				'src',
																				$(
																						'#loggedProfileImage')
																						.attr(
																								'src')
																						+ '&p='
																						+ Math
																								.random());
																//$("#loggedProfileImage").attr("src","${pageContext.request.contextPath}/findImageByUserid.htm?userid=${sessionScope.userSessionVO.loginid}");
															}
														});
											});
							/*
							var defaults = {
							containerID: 'toTop', // fading element id
							containerHoverID: 'toTopHover', // fading element hover id
							scrollSpeed: 1200,
							easingType: 'linear' 
							};
							 */

							$().UItoTop({
								easingType : 'easeOutQuart'
							});

						});
	</script>
	<!-- //here ends scrolling icon -->
	<script src="js/bars.js"></script>
	<script type="text/javascript">
		function InvalidMsg(textbox) {
			if (textbox.length == 0) {
				textbox.setCustomValidity('please enter your userid.');
			} else {
				textbox.setCustomValidity('');
			}
			return true;
		}
	</script>

	<!-- Modal -->
	<div class="modal fade" id="editProfileModel" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div>
					<div class="container">
						<div>
							<div class="col-md-5">
								<form id="editProfileForm" enctype="multipart/form-data"
									method="post">

									<img class="zoom"
										src="${pageContext.request.contextPath}/findImageByUserid?userid=${sessionScope.userSessionVO.loginid}"
										style="float: right; height: 100px; margin-left: 20px;"
										class="img-circle" />
									<h5 class="modal-title"
										style="display: inline; font-size: 16px;">Edit Profile
										Pic</h5>
									<table>

										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>Email</td>

											<td><input type="text"
												value="${sessionScope.userSessionVO.email}"
												readonly="readonly" name="email" class="form-control"
												style="background-color: #ffdecb; margin-left: 20px; width: 350px; color: black;"></td>
										</tr>


										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>Userid</td>
											<td><input type="text"
												value="${sessionScope.userSessionVO.loginid}"
												readonly="readonly" name="userid" class="form-control"
												style="background-color: #ffdecb; margin-left: 20px; width: 350px; color: black;"></td>
										</tr>


										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>Photo</td>
											<td><input type="file" name="image" class="form-control"
												style="background-color: #d9edf7; margin-left: 20px; width: 350px; color: black;"></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp; <input type="button" id="UpdateProfile"
												style="color: #FFFFFF; background: #ffb900; border: 2px solid #ffb900; text-transform: uppercase; padding: .2em 1em; font-size: 1.3em; font-family: 'Ropa Sans', sans-serif;"
												value="Update Profile"></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
									</table>
								</form>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
</body>
<!-- //Body -->
</html>
<!-- //html -->
