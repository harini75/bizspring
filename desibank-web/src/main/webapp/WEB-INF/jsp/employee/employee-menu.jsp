	<div class="top-bar">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="${pageContext.request.contextPath}/customerHome">home</a></li>
							<li><a href="#services">Customer Accounts</a></li>
							<li><a href="${pageContext.request.contextPath}/customer/transferMoney">Saving Accounts</a></li>
							<li><a href="${pageContext.request.contextPath}/customer/showCustomerAccountsSummary">Account Summary</a></li>
							<li><a href="${pageContext.request.contextPath}/customer/addPayee">Lock/Unlock</a></li>
							<li><a href="#blog" class="scroll">Transactions</a></li>
							<li><a href="${pageContext.request.contextPath}/employee/creditCardDashboard">Credit Card</a></li>
<%-- spring mvc logout		<li><a href="${pageContext.request.contextPath}/auth/logout">Logout</a></li> --%>
							<!-- 			spring boot logout				 -->
							<li><a href="${pageContext.request.contextPath}/corp/logout">Logout</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>