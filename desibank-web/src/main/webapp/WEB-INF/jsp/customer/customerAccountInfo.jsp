<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
			p{
				width: 70%;
				font-family: 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans';font-size: 16px;
				font-style: normal;
				font-variant: normal;
				font-weight: 500;
				line-height: 26.4px;
			}
	</style>
	
	
	<title>Customer Accounts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Corporate Bank a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- .css files -->
	<link href="${pageContext.request.contextPath}/css/bars.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/nstyle.css" type="text/css" media="all" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css" />
<!-- //.css files -->
<!-- Default-JavaScript-File -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<!-- //Default-JavaScript-File -->
<!-- fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Ropa+Sans:400,400i&amp;subset=latin-ext" rel="stylesheet">
<!-- //fonts -->
<!-- scrolling script -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- //scrolling script -->
	
	
</head>
  
<body>
	 		<jsp:include page="top-bar.jsp"/>
		<!-- Top-Bar -->
	
<jsp:include page="customer-menu.jsp"/>
<!--team-->
<jsp:include page="dashboard-options.jsp"/>
<!--//team-->

<div class="container">

   	 <table border="1" style="width: 70%;font-family: 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans';font-size: 14px;font-style: normal;	font-variant: normal;font-weight: 500;line-height: 24.4px;">
		<tr style="background-color: #1e2a7b;color:white;">
	
					<td>&nbsp;A/C Number</td>
					<td>&nbsp;A/C Type</td>
					<td>&nbsp;Avg Balance</td>
					<td>&nbsp;Branch</td>
				<td>&nbsp;Currency</td>
			<td>Date Opened</td>
		</tr>
		<c:forEach items="${customerAccountList}" var="account">
	
			<tr style="background-color: #b6fffe;color:black;">
		
					<td>&nbsp;${account.accountNumber}</td>
					<td>&nbsp;${account.accountType}</td>
					<td>&nbsp;${account.avBalance}</td>
					<td>&nbsp;${account.branch}</td>
				<td>&nbsp;${account.currency}</td>
				<td>&nbsp;${account.statusAsOf}</td>
				
				
		</tr>
		</c:forEach>
	
	
	
</table>
</div>
<div style="position: fixed;bottom: 0px; width: 100%">
   	 <jsp:include  page="../common/copyright.jsp"/>
   	 
   	 </div>
   	 
   	 
</body>
</html>