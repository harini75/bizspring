<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Desi Bank</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords"
	content="Corporate Bank a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- .css files -->
<link href="css/bars.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" media="all" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/nstyle.css" type="text/css" media="all" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css" />
<!-- //.css files -->
<!-- Default-JavaScript-File -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<!-- //Default-JavaScript-File -->
<!-- fonts -->
<link
	href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
	rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Ropa+Sans:400,400i&amp;subset=latin-ext" rel="stylesheet">

<link rel="stylesheet" href="${pageContext.request.contextPath}/css/views/bank-account.css" />
</head>
<body>

	<jsp:include page="top-bar.jsp" />
	<jsp:include page="customer-menu.jsp" />

	<section class="blog" id="blog">
		<div class="container account-container">
			<div class="blog-heading">
				<h3 class="section-header back-btn" style="text-align: left;">
					<i class="fa fa-chevron-left" aria-hidden="true"></i> Account Summary
				</h3>
			</div>

			<div class="card account-detail">
				<div class="card-body">
					<div class="account-detail__title clear-both">
						<i class="fa fa-id-card account-detail__title__icon pull-left"></i>
						<div class="pull-left">
							<div class="account-detail__title__account-type">${fn:toUpperCase(account.accountType)}&nbsp;ACCOUNT</div>
							<div class="account-detail__title__account-no">${account.accountNumber}</div>
						</div>
						<div class="pull-right">
							<div class="account-detail__title__av-balance-val">$ ${account.avBalance}</div>
							<div class="account-detail__title__av-balance-lbl">Available balance</div>
						</div>
					</div>
					<div class="account-detail__table">
						<h5>
							<b>Activity Summary</b>
						</h5>

						<table class="table">
							<tr>
								<td>Current posted balance</td>
								<td class="account-detail__table__tav-balance">$ ${account.tavBalance}</td>
							</tr>
							<tr>
								<td>Pending withdrawals/debits</td>
								<td class="account-detail__table__tav-balance">$ 0.00</td>
							</tr>
							<tr>
								<td>Pending deposits/credits</td>
								<td class="account-detail__table__tav-balance">$ 0.00</td>
							</tr>
							<tr>
								<td><b>Available balance</b></td>
								<td class="account-detail__table__av-balance"><b>$ ${account.avBalance}</b></td>
							</tr>
						</table>
					</div>
				</div>
			</div>

			<div class="account-transactions">
				<div class="clear-both">
					<h5 class="pull-left">
						<b>Transactions</b>
					</h5>

					<div class="btn-group account-transactions__report-btn pull-right">
						<button class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-print" aria-hidden="true"></i>
							<span class="caret"></span>
						</button>
						
						<ul class="dropdown-menu account-transactions__report-list" aria-labelledby="dLabel">
   							<li id="pdf-report">
   								<i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF
   							</li>
   							<li id="xls-report">
   								<i class="fa fa-file-excel-o" aria-hidden="true"></i> XLS
   							</li>
  						</ul>
					</div>

					<button class="btn account-transactions__filter-btn pull-right">
						<i class="fa fa-filter" aria-hidden="true"></i>
					</button>
				</div>

				<div class="account-transactions__filter-controller clear-both">
					<div class="container-fluid">
						<div class="row">
							<div class="col col-sm-4">
								<div>
									<label for="filter-from">From (Date)</label> <input id="filter-from" type="date"
										class="default-bootstrap-form-control account-transactions__filter-controller__date-from" />
								</div>
							</div>
							<div class="col col-sm-4">
								<div>
									<label for="filter-to">To (Date)</label> <input id="filter-to" type="date"
										class="default-bootstrap-form-control account-transactions__filter-controller__date-to" />
								</div>
							</div>
							<div class="col col-sm-3 col-sm-offset-1">
								<div class="account-transactions__filter-controller__records-per-page">
									<label for="records-per-page">Transactions per page: </label><select id="records-per-page"
										class="default-bootstrap-form-control form-control-sm">
										<option value="10">10</option>
										<option value="15">15</option>
										<option value="20">20</option>
										<option value="25">25</option>
										<option value="30">30</option>
									</select>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col col-sm-12 align-right">
								<button id="filter-submit-btn" class="btn desibank-primary-btn account-transactions__filter-controller__submit-btn">Filter</button>
							</div>
						</div>
					</div>

				</div>

				<div class="wrapper">
					<table class="table account-transactions__table">
						<thead class="thead-dark">
							<tr>
								<th scope="col">Date</th>
								<th scope="col">Description</th>
								<th scope="col">Deposits/Credits</th>
								<th scope="col">Withdrawals/Debits</th>
							</tr>
						</thead>
						<tbody class="account-transactions__table__body">
							<c:forEach items="${transactions}" var="transaction" varStatus="status">
								<tr>
									<td class="account-transactions__table__date"><fmt:formatDate pattern="MM/dd/yyyy" value="${transaction.date}" /></td>
									<td>${transaction.description}</td>
									<c:choose>
										<c:when test="${transaction.toAccountNumber == account.accountNumber}">
											<td class="account-transactions__table__amount--credit">+${transaction.amount}</td>
											<td class="account-transactions__table__amount--debit"></td>
										</c:when>
										<c:otherwise>
											<td class="account-transactions__table__amount--credit"></td>
											<td class="account-transactions__table__amount--debit">${transaction.amount}</td>
										</c:otherwise>
									</c:choose>
								</tr>
							</c:forEach>
						</tbody>
					</table>

					<div class="container-fluid">
						<div class="desibak-pagination">
							<div class="row">
								<div class="col col-sm-6">
									<div class="desibak-pagination__label">
										<span id="desibak-pagination-start">1</span><span> - </span><span id="desibak-pagination-end">${fn:length(transactions)}</span><span>
											of </span><span id="desibak-pagination-total">${total}</span>
									</div>
								</div>
								<div class="col col-sm-6">
									<div class="desibak-pagination__controller">
										<div class="btn-group btn-group-sm">
											<button type="button" class="btn btn-default desibak-pagination__controller__prev-btn">
												<i class="fa fa-chevron-left"></i>
											</button>
											<button type="button" class="btn btn-default desibak-pagination__controller__next-btn">
												<i class="fa fa-chevron-right"></i>
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>
	</section>

	<jsp:include page="../common/footer.jsp" />
	<jsp:include page="../common/copyright.jsp" />

	<div class="modal fade alert-modal" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="${pageContext.request.contextPath}/js/views/bank-account.js"></script>
</body>
</html>