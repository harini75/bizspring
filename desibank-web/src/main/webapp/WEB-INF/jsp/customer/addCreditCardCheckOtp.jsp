<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<head>
<title>Desi Bank Customer Check Credit Card One Time Password</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Corporate Bank a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!-- .css files -->
	<link href="${pageContext.request.contextPath}/css/bars.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/nstyle.css" type="text/css" media="all" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css" />
<!-- //.css files -->
<!-- Default-JavaScript-File -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<!-- //Default-JavaScript-File -->
<!-- fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Ropa+Sans:400,400i&amp;subset=latin-ext" rel="stylesheet">
<!-- //fonts -->
<!-- scrolling script -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});

		
	});
</script>
<!-- //scrolling script -->
</head>
<!-- //Head -->
<!-- Body -->
<body>
		<jsp:include page="top-bar.jsp"/>
		<!-- Top-Bar -->
	
<jsp:include page="customer-menu.jsp"/>
<!--team-->
<%-- <jsp:include page="dashboard-options.jsp"/> --%>
<!--//team-->

<br/>
<div class="collapse navbar-collapse" id="myNavbar">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="${pageContext.request.contextPath}/customer/addCreditCard">Add CreditCards</a></li>
							<li><a href="${pageContext.request.contextPath}/customer/showCustomerAccounts">My CreditCards</a></li>
							<li><a href="${pageContext.request.contextPath}/customer/transferMoney">Pay Bill</a></li>
						</ul>
					</div>
					Please enter the one time code sent to the associated email.
					<div id="message" style="color: red">${message} </div>
				<form:form id="addCreditCardForm" modelAttribute="addCreditCardForm" method="post" action="${pageContext.request.contextPath}/customer/addCreditCard/validateOneTimePassword">
					  <table>
                <tr>
                    <td><label for="cardNumber">Credit Card Number:</label></td>
                    <td><form:input  id="cardNumber" name="cardNumber" path="cardNumber" readonly="true"/></td>
                </tr>
                <tr>
                    <td><label for="expDateMonth">Expired Date:</label></td>
                    <td><form:input  path="expDateMonth" id="expDateMonth" name="expDateMonth" placeholder="MM" readonly="true"/>&nbsp;/&nbsp;</td>
                   <td><form:input  path="expDateYear" type="text" name="expDateYear" placeholder="YY" readonly="true"/></td>
                </tr>
                <tr>
                    <td><label for="ccv">
                      CCV: </label></td>
                    <td><form:input path="ccv"  id="ccv" name="ccv" readonly="true"/></td>
                </tr>
                
                  <tr id="oneTimeCodeRow"><td> <label for="otp">One Time Code: </label></td>
						<td><form:input path="otp" id="otp" type="text" /></td>
					</tr>
            
               <tr>
						<td><input type="submit" id="addCardButton" type="button" value="Add Credit Card"/></td>
					</tr>
            </table>
				</form:form>


	<!-- copyright -->
<jsp:include page="../common/copyright.jsp"/>
<!-- //copyright -->

	

</body>
<!-- //Body -->
</html>
<!-- //html -->