<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<head>
<title>Desi Bank Customer Credit Card Dashboard</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="Corporate Bank a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!-- .css files -->
	<link href="${pageContext.request.contextPath}/css/bars.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/nstyle.css" type="text/css" media="all" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css" />
<!-- //.css files -->
<!-- Default-JavaScript-File -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<!-- //Default-JavaScript-File -->
<!-- fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Ropa+Sans:400,400i&amp;subset=latin-ext" rel="stylesheet">
<!-- //fonts -->
<!-- scrolling script -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- //scrolling script -->
</head>
<!-- //Head -->
<!-- Body -->
<body>
		<jsp:include page="top-bar.jsp"/>
		<!-- Top-Bar -->
	
<jsp:include page="customer-menu.jsp"/>
<!--team-->
<%-- <jsp:include page="dashboard-options.jsp"/> --%>
<!--//team-->

<br/>
<div class="collapse navbar-collapse" id="myNavbar">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="${pageContext.request.contextPath}/customer/addCreditCard">Add CreditCards</a></li>
							<li><a href="${pageContext.request.contextPath}/customer/showCreditCards">My CreditCards</a></li>
							<li><a href="${pageContext.request.contextPath}/customer/transferMoney">Pay Bill</a></li>
						</ul>
					</div>
				<div class="container">
		<table  border="1" style="width: 70%;font-family: 'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans';font-size: 14px;font-style: normal;	font-variant: normal;font-weight: 500;line-height: 24.4px;">
		<tr style="background-color: #1e2a7b;color:white;">
		
					<td>&nbsp;Card Number</td>
					<td>&nbsp;APR</td>
					<td>&nbsp;Cash Back</td>
					<td>&nbsp;CCV</td>
				<td>&nbsp;Credit Score</td>
				<td>&nbsp;Network</td>
				<td>&nbsp;Current Balance</td>
				<td>&nbsp;Statement Balance</td>
				<td>&nbsp;Statement Due Date</td>
				<td>&nbsp;Monthly Minimum Payment</td>
		</tr>
		<c:forEach items="${creditCardList}" var="creditCardEntity">
			<tr style="background-color: #b6fffe;color:black;">
			
					<td>&nbsp;${creditCardEntity.cardNumber}</td>
					<td>&nbsp;${creditCardEntity.apr}</td>
					<td>&nbsp;${creditCardEntity.cashBack}</td>
					<td>&nbsp;${creditCardEntity.ccv}</td>
				<td>&nbsp;${creditCardEntity.creditScore}</td>
				<td>&nbsp;${creditCardEntity.network}</td>
				<td>&nbsp;${creditCardEntity.currentBalance}</td>
				<td>&nbsp;${creditCardEntity.statementBalance}</td>
				<td>&nbsp;${creditCardEntity.statementDueDate}</td>
				<td>&nbsp;${creditCardEntity.monthlyMinPayment}</td>
				
		</tr>
	</c:forEach>
		
<!-- 		<tr> -->
<!-- 			<td colspan="7">__________________</td> -->
<!-- 		</tr> -->
</table>			
			</div>		
				
				
					


<div style="position: absolute;bottom: 0;width: 100%">
<!-- copyright -->
<jsp:include page="../common/copyright.jsp"/>
<!-- //copyright -->
</div>
	
  
</body>
<!-- //Body -->
</html>
<!-- //html -->