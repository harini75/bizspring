<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<head>
<title>Desi Bank</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords"
	content="Corporate Bank a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- .css files -->
<link href="css/bars.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" media="all" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/nstyle.css" type="text/css" media="all" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css" />
<!-- //.css files -->
<!-- Default-JavaScript-File -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<!-- //Default-JavaScript-File -->
<!-- fonts -->
<link
	href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
	rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Ropa+Sans:400,400i&amp;subset=latin-ext" rel="stylesheet">

<!-- //fonts -->
<!-- scrolling script -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 1000);
		});
	});
</script>
<!-- //scrolling script -->

<style type="text/css">
.account.clearfix {
	overflow: auto;
}

.account.clearfix::after {
	content: "";
	clear: both;
	display: table;
}

.account:hover {
	cursor: pointer;
	background-color: #f0f0f0;
}

.account span {
	color: #515151;
}

.account {
	border-left: 7px solid #ffb900;
}

.account__acc-info {
	display: inline-block;
	float: left;
	margin-left: 2em;
}

.account__acc-info__acc-type {
	color: #ffb900;
	font-size: 2em;
}

.account__acc-info__acc-no {
	color: #8a8a8a;
	margin-top: 5px;
}

.account__acc-balance {
	float: right;
	display: inline-block;
	font-size: 25px;
	margin-top: 7px;
}

.account__acc-balance i {
	color: #8a8a8a;
	margin-left: 15px;
}

.account__acc-ic {
	color: #8a8a8a;
	font-size: 2.5em;
	display: inline-block;
	float: left;
}

.account__acc-ic i {
	vertical-align: middle;
}

.account__acc-balance__lbl {
	font-size: .5em;
	color: #b4b4b4;
}

.account__acc-balance__val {
	font-size: 1.1em;
}

.credit, .debit {
	width: 160px;
	text-align: right;
	padding-right: 25px !important;
}

.credit {
	color: #0b7d0b;
}

.debit {
	color: #9e0e0e;
}

.mini-statement-date {
	width: 160px;
}

.mini-statement-tbl {
	text-align: left;
}

.mini-statement-tbl th:first-child {
	text-align: left;
}

.mini-statement-tbl th {
	text-align: center;
}

.mini-statement-tbl th, .mini-statement-tbl td {
	padding-top: 10px !important;
	padding-bottom: 10px !important;
}

.mini-statement-tbl tr:nth-child(even) {
	background-color: #f8f8f8;
}

.desibank-btn {
	color: #FFFFFF;
	background: #ffb900;
	border: 2px solid #ffb900;
	text-transform: uppercase;
	padding: .2em 1em;
	font-size: 20px;
	font-family: 'Ropa Sans', sans-serif;
}
</style>

</head>
<!-- //Head -->
<!-- Body -->
<body>
	<jsp:include page="top-bar.jsp" />
	<!-- Top-Bar -->

	<jsp:include page="customer-menu.jsp" />

	<section class="blog" id="blog">
		<div class="container">
			<div class="blog-heading">
				<h3 style="text-align: left;">Accounts Summary</h3>
			</div>
			<div class="list-group" style="margin-top: 2em;">
				<c:forEach items="${customerAccountList}" var="account" varStatus="status">
					<div class="list-group-item list-group-item-action flex-column align-items-start account clearfix" data-account-no="${account.accountNumber}">
						<div class="account__acc-ic">
							<i class="fa fa-id-card"></i>
						</div>
						<div class="account__acc-info">
							<div style="vertical-align: middle;">
								<div class="account__acc-info__acc-type">${fn:toUpperCase(account.accountType)}&nbsp;ACCOUNT</div>
								<div class="account__acc-info__acc-no">${account.accountNumber}</div>
							</div>
						</div>
						<div class="account__acc-balance">
							<div style="display: inline-block; vertical-align: middle;">
								<div class="account__acc-balance__val">$ ${account.avBalance}</div>
								<div class="account__acc-balance__lbl">Available balance</div>
							</div>
							<i class="fa fa-chevron-circle-right"></i>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</section>

	<!-- our blog -->
	<jsp:include page="../common/bank-news.jsp" />
	<!-- //our blog -->

	<!-- modal -->
	<div class="modal about-modal fade" id="myModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span class="span1" aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">
						Corporate<span> Bank</span>
					</h4>
				</div>
				<div class="modal-body">
					<div class="agileits-w3layouts-info">
						<img src="${pageContext.request.contextPath}/images/business.jpg" alt="" />
						<p>Duis venenatis, turpis eu bibendum porttitor, sapien quam ultricies tellus, ac rhoncus risus odio eget nunc. Pellentesque ac fermentum
							diam. Integer eu facilisis nunc, a iaculis felis. Pellentesque pellentesque tempor enim, in dapibus turpis porttitor quis. Suspendisse ultrices
							hendrerit massa. Nam id metus id tellus ultrices ullamcorper. Cras tempor massa luctus, varius lacus sit amet, blandit lorem. Duis auctor in
							tortor sed tristique. Proin sed finibus sem.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //modal -->


	<!-- footer -->
	<jsp:include page="../common/footer.jsp" />
	<!-- //footer -->

	<!-- copyright -->
	<jsp:include page="../common/copyright.jsp" />
	<!-- //copyright -->

	<script src="js/jarallax.js"></script>
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed : 0.5,
			imgWidth : 1366,
			imgHeight : 768
		})
	</script>

	<script type="text/javascript" src="${pageContext.request.contextPath}/js/move-top.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/easing.js"></script>

	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			 */

			$().UItoTop({
				easingType : 'easeOutQuart'
			});
			 
			 
			$(document).on('click', '.account', function (e) {
				var account = $(this);
				var detailIcon = account.find('.account__acc-balance>i');
				
				if (account.hasClass('opened')) {	
					var miniStatementTable = account.next();
					
					miniStatementTable.slideToggle("slow", function() {
						miniStatementTable.remove();
						account.toggleClass('opened');
						detailIcon.toggleClass('fa-chevron-circle-right');
						detailIcon.toggleClass('fa-chevron-circle-down');
					});
				} else {
					generateMiniStatementTable(account.data('account-no')).then(function (miniStatementTable) {
						account.after(miniStatementTable);
						miniStatementTable.slideToggle("slow", function() {
							account.toggleClass('opened');
							detailIcon.toggleClass('fa-chevron-circle-right');
							detailIcon.toggleClass('fa-chevron-circle-down');
						});     
					}).catch(function (error) {
						console.log(error);
					});
				}
			});
			
			$(document).on('click', '.more-detail-btn', function (e) {
				window.location.href = '/desibank-web/customer/account?accountNumber=' + $(this).data('account-no');
			});
		});
	</script>
	<!-- //here ends scrolling icon -->
	<script src="js/bars.js"></script>
	<script type="text/javascript">
		function generateMiniStatementTable(accountNo) {
			return new Promise(function (resolve, reject) {
				$.ajax({
					type:'GET',
					url: '/desibank-web/v1/customer/mini-statement',
					data: {
						accountNumber: accountNo
					},
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(data){
						
						var wrapper = $('<div>');
						wrapper.css({'display': 'none', 'padding': '1em 2em', 'text-align': 'center'});
						
						var tbl = $('<table>');
						tbl.addClass('table mini-statement-tbl');
						
						var header = $('<thead>');
						var headerRow = $('<tr>');
						headerRow.append('<th scope="col">Date</th>');
						headerRow.append('<th scope="col">Description</th>');
						headerRow.append('<th scope="col">Deposits/Credits</th>');
						headerRow.append('<th scope="col">Withdrawals/Debits</th>');
						header.append(headerRow);
						tbl.append(header);
						
						var body = $('<tbody>');
						
						data.forEach(function (transaction, index) {
							var row = $('<tr>');
							
							row.append('<td class="mini-statement-date">' + toMMDDYYYY(transaction.date) + '</td>');
							row.append('<td>' + transaction.description + '</td>');
							
							if (transaction.toAccountNumber == accountNo) {
								row.append('<td class="credit">+' + transaction.amount + '</td>');
								row.append('<td class="debit"></td>');								
							} else {
								row.append('<td class="credit"></td>');
								row.append('<td class="debit">' + transaction.amount + '</td>');
							}
							
							body.append(row);
						});
						
						tbl.append(body);
						wrapper.append(tbl);
						
						var moreBtn = $('<button>');
						moreBtn.prop('type', 'button');
						moreBtn.addClass('btn desibank-btn more-detail-btn');
						moreBtn.text('More Detail');
						moreBtn.data('account-no', accountNo);
						
						wrapper.append(moreBtn);
						
						resolve(wrapper);
					}
				});
			});
		}
		
		function toMMDDYYYY(timestamp) {
			var date = new Date(timestamp);
			
			var m = ('0' + (date.getMonth() + 1)).substr(-2);
			var d = ('0' + date.getDate()).substr(-2);
			var y = date.getFullYear();
			
			return m + '/' + d + '/' + y;
		}
	
		function InvalidMsg(textbox) {
			if (textbox.length == 0) {
				textbox.setCustomValidity('please enter your userid.');
			} else {
				textbox.setCustomValidity('');
			}
			return true;
		}
	</script>
</body>
<!-- //Body -->
</html>
<!-- //html -->