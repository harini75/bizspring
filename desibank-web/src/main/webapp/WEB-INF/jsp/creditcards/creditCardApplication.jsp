<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Credit Card Application</title>

<!-- .css files -->
<link href="${pageContext.request.contextPath}/css/bars.css"
	rel="stylesheet" type="text/css" />

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/nstyle.css"
	type="text/css" media="all" />

	<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.min.css"
	type="text/css" media="all" />

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font-awesome.css" />
	


<!-- //.css files -->
<!-- Default-JavaScript-File -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<!-- //Default-JavaScript-File -->
<!-- fonts -->
<link
	href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
	rel="stylesheet">
<link
	href="//fonts.googleapis.com/css?family=Ropa+Sans:400,400i&amp;subset=latin-ext"
	rel="stylesheet">
<!-- //fonts -->

<style>

.helloBox {
	padding: 30px 20px;
	text-align: center;
	color: #0950a0;
	background-color: #d3ecf7;
	font-size: 30px;
}
.beforeYouBeginContent{
background-color: #bfbfbf;}
.beforeYouBeginContent .checklist {
	width: 346px;
	padding: 10px 0;
	margin-right: auto;
	margin-left:30px;
	margin-top: -5px;
	margin-bottom: 0px;
	color: #0950a0!important;

}


/* Add a black background color to the top navigation */
.topnav {
 margin-top:10px; 
  background-color: #bfbfbf;
  overflow: hidden;

}

/* Style the links inside the navigation bar */
.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 12px 10px;
  text-decoration: none;
  font-size: 13px;
}

/* Change the color of links on hover */
/* .topnav a:hover { */
/*   background-color: #bfbfbf; */
/*   color: black; */
/* } */

/* Add an active class to highlight the current page */
.active {
  background-color: #0d0169;
  color: #0d0169;
}

/* Hide the link that should open and close the topnav on small screens */
.topnav .icon {
  display: none;
}


#topHeader {
    display: inline-block;
}

.appForm{
	background-color: #f8f8f8;
	margin-top: 0;
}

.col-md-4{
margin-bottom: 10px;
}
.col-md-8{
margin-bottom: 10px;
}

.col-md-6{
margin-bottom: 10px;
}


</style>


<script>

	$(()=>{
		$("#applicationForm").submit(function(event){
			if(hasEmptyField()==true){
				event.preventDefault();
				window.scrollTo(0, 0);	
			}
			
		    if(!document.getElementById('agreementCheckBox').checked) {
                $('#errorMessage').append("<li>Must agree our terms and condition before the application.</li>");
                event.preventDefault();
                window.scrollTo(0, 0);	
		    }
			
		});
		$("#applicationForm").find("input").keydown(function(){
			$('#errorMessage').html("");
		})
		
	});

	function hasEmptyField(){
		let hasError=false;
		$("#applicationForm").find("input").each(function(){
			let inputValue=$(this).val();
			if(inputValue==null||inputValue==""){
				let field=$(this).attr("name");
				$("#errorMessage").append("<li>"+field +" cannot be empty."+"</li>");
				hasError=true;
			}
			
		});
		return hasError;
	}

</script>




</head>
<body>
	<div class="top-main">
		<div class="number">
			<h3 style="color: white; font-weight: bold;">
				<i class="fa fa-phone" aria-hidden="true"></i>Customer Care : +91
				080 987 6541
			</h3>
			<div class="clearfix"></div>
		</div>
		<div class="social-icons">
			<ul class="top-icons">
				<li><a href="#"><i class="fa fa-facebook"
						aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-google-plus"
						aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-pinterest-p"
						aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-linkedin"
						aria-hidden="true"></i></a></li>
			</ul>
			<div class="form-top">
				<form action="#" method="post" class="navbar-form navbar-left">
					<div class="form-group">
						<input type="search" class="form-control" placeholder="Search">
					</div>
					<button type="submit" class="btn btn-default">
						<i class="fa fa-search" aria-hidden="true"></i>
					</button>
					<!-- <button type="submit" class="btn btn-default">Submit</button> -->
				</form>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<!-- Top-Bar -->
	<div class="top-bar">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#myNavbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse home" id="myNavbar" style="margin: auto;position: relative;left: 50%">
						<ul class="navbar navbar-nav navbar-left">
							<li><a href="${pageContext.request.contextPath}">home</a></li>
						</ul>
					</div>
			</div>
		</nav>
	</div>
	
	
	<div class="container" style="width: 800px">
		<div class="helloBox">

			Hello! <br /> Welcome to your <span>${cardType} </span> application. <br />
			It's fast, simple and secure. <br />



		</div>

		<div class="beforeYouBeginContent">
		
			<div class="checklist">Applicants need
					to:</div>
			<ul class="checklist">
				<li>Be at least 18 years old</li>
				<li>
					Be legal residents of the United
						States</li>
				<li>
					Have a Social Security number or
						Individual Taxpayer Identification Number</li>
			</ul>
		</div>
		
<!-- <div class="topnav" id="cardMenu"> -->
<!--   <a href="#cardMenu" class="active">Personal information</a> -->
<!--   <a href="#cardMenu">Housing Information</a> -->
<!--   <a href="#cardMenu">Income and Employment Information</a> -->
<!-- <a href="#cardMenu">Term & Conditions</a> -->

<!-- </div> -->

<div class="appForm">
<div style="color: red" id="errorMessage"></div>
<spring:form id="applicationForm" action="${pageContext.request.contextPath}/creditcards/creditCardApplication" enctype="multipart/form-data" method="post" modelAttribute="creditCardApplicationVO">
  <div class="form-group">
<div class="col-md-12 mb-5">
  <h3>Personal Information (* = required)</h3>
  </div>
</div>
  
  
  
  <div class="form-row">
   <div class="col-md-4 mb-5">
     
  <spring:hidden path="cardType" value="${cardType}" /> 
<%--   <spring:hidden path="appRef" value=""/>  --%>
    <label for="firstName">First Name:</label>
    <spring:input path="firstName" type="text" id="firstName" class="form-control" name="firstName" placeholder="First Name*" />
  	</div>
 
 
  <div class="col-md-4 mb-5">
    <label for="lastName">Last Name:</label>
    <spring:input path="lastName" type="text" id="lastName" class="form-control" name="lastName" placeholder="Last Name*" />
  </div>
  <div class="col-md-4 mb-5 col-md-offset-8"></div>
  </div>
  
   <div class="form-row">
      <div class="col-md-4 mb-5">
    <label for="dob">Date of Birth:</label>
    <spring:input path="dob" type="date" id="dob" class="form-control" name="dob" />
  	</div>
  	  <div class="col-md-4 mb-5 col-md-offset-8"></div>
  </div>
  

  <div class="form-row">
    <div class="col-md-4 mb-5">
    <label for="citizenship">Citizenship:</label>
    <spring:input path="citizenship" type="text" id="citizenship" class="form-control" name="citizenship" placeholder="Citizenship*" />
  </div>
  <div class="col-md-4 mb-5 col-md-offset-8"></div>
    </div>

    <div class="form-row">
    <div class="col-md-4 mb-5">
    <label for="phoneNumber">Phone Number:</label>
    <spring:input path="phoneNumber" type="text" id="phoneNumber" class="form-control" name="phoneNumber" placeholder="Phone Number*" />
    </div>
<div class="col-md-4 mb-5 col-md-offset-8"></div>
    <div class="col-md-4 mb-5">
    <label for="email">Email:</label>
    <spring:input path="email" type="email" id="email" class="form-control" name="email" placeholder="Email*" />
  </div>
  <div class="col-md-8 mb-5 col-md-offset-4"></div>
    </div>
    
        <div class="form-row">
    <div class="col-md-4 mb-5">
    <label for="ssn">SSN:</label>
    <spring:input path="ssn" type="text" id="ssn" class="form-control" name="ssn" placeholder="SSN*" />
  </div>
       <div class="col-md-4 mb-5 col-md-offset-8"></div>
  </div>
    
    <div class="col-md-4 mb-5">
    <label for="passportPhoto">Passport Photo:</label>
    <input id="passportPhoto" type="file" class="form-control"  name="passportPhoto" placeholder="passportPhoto*" />
  </div>



  <hr class="col-md-12" style="width: 96%"/>
    <div class="form-group">
  
<div class="col-md-12 mb-5">
  <h3>Housing Information</h3>
  </div>
</div>
    <div class="form-row">
    <div class="col-md-8 mb-5">
    <label for="address">Address:</label>
    <spring:input path="address" type="text" id="address" class="form-control" name="address" placeholder="Address*" />
    </div>
    <div class="col-md-4 mb-5  col-md-offset-8 "></div>
    <div class="col-md-4 mb-5">
    <label for="address">Zip Code:</label>
    <spring:input path="zip" type="text" id="zip" class="form-control" name="zip" placeholder="Zip*" />
    </div>
    <div class="col-md-8 mb-5  col-md-offset-4 "></div>
    
  </div>
    <div class="form-row ">
    <div class="col-md-4 mb-5">
    <label for="monthLived">Number of Months You have Lived on this Address:</label>
    <spring:input path="monthLived" type="number" id="monthLived" class="form-control" name="monthLived" placeholder="Months Lived*" />
  </div>
  <div class="col-md-6 mb-5  col-md-offset-6 "></div>
      <div class="col-md-6 mb-5">
    <label for="housingStatus">Housing Status:</label>
    <br/>
    <spring:select id="housingStatus" class="custom-select mr-sm-2"  name="housingStatus" path="housingStatus" placeholder="Housing Status*">
        <spring:option value="">Housing Status*</spring:option>
        <spring:option value="own">OWN</spring:option>
        <spring:option value="other">OTHER</spring:option>
  </spring:select>
   
  </div>
   <div class="col-md-6 mb-5 col-md-offset-6"></div>
  <div class="col-md-4 mb-5">
  
    <label for="monthlyPayment">Monthly Payment:</label>
    <spring:input path="monthlyPayment" type="text" id="monthlyPayment" class="form-control" name="monthlyPayment" placeholder="Monthly Payment*" />
  	</div>
  	<div class="col-md-8 mb-5 col-md-offset-4"></div>
  </div>
  
    <hr class="col-md-12" style="width: 96%"/>
  
<div class="form-group">
<div class="col-md-12 mb-5">
  <h3>Income and Employment Information</h3>
  </div>
</div>
				<div class="form-row">

					<div class="col-md-6 mb-5">
						<label for="employmentStatus">Employment Status:</label> <br />
						<spring:select id="employmentStatus" class="select"
							name="employmentStatus" path="employmentStatus"
							placeholder="Employement Status*">
							<spring:option value="" >Employment Status*</spring:option>
							<spring:option  value="FULL-TIME EMPLOYMENT">FULL-TIME EMPLOYMENT</spring:option>
							<spring:option value="PART-TIME EMPLOYMENT">PART-TIME EMPLOYMENT</spring:option>
							<spring:option value="UNEMPLOYED">UNEMPLOYED</spring:option>
							<spring:option value="SELF-EMPLOYED">SELF-EMPLOYED</spring:option>
							<spring:option value="HOMEMAKER">HOMEMAKER</spring:option>
							<spring:option value="RETIRED">RETIRED</spring:option>
							<spring:option value="STUDENT">STUDENT</spring:option>
							<spring:option value="MILITARY">MILITARY</spring:option>

						</spring:select>

					</div>

					<div class="col-md-4 col-md-offset-6"></div>
				</div>



				<div class="form-row mb-5">
				<div class="col-md-4 mb-5">
					<label for="firstName">Annual Income:</label> <spring:input type="text" path="annualIncome"
						id="annualIncome" class="form-control" name="annualIncome"
						placeholder="Annual Income*" />

				</div>
			</div>
			
			<div class="form-group">
<div class="col-md-12 mb-5">
  <h3>Terms and Conditions</h3>
  
  </div>
<div class="form-group">
 <textarea class="form-control" disabled="disabled" rows="10" cols="5">
IMPORTANT INFORMATION ABOUT PROCEDURES FOR OPENING A NEW ACCOUNT:
To help the government fight the funding of terrorism and money laundering activities, Federal law requires all financial institutions to obtain, verify and record information that identifies each person who opens an account. What this means for you: When you open an account, we will ask for your name, address, date of birth and other information that will allow us to identify you. We may also ask to see your driver's license or other identifying documents.
We may change APRs, fees, and other Account terms in the future based on your experience with Desi Bank National Association and its affiliates as provided under the Cardmember Agreement and applicable law.

We apply your minimum payment to balances with lower APRs first, including promotional APRs. Amounts paid over the minimum payment will be applied in the order of highest to lowest APR balances.
Disclosure of Credit Card Terms
Interest Rates and Interest Charges	Desi Bank ${cardType}
Annual Percentage Rate (APR) for Purchases	Desi Bank ${cardType}: 21.24%
This APR will vary with the market based on the Prime Rate.
APR for Balance Transfers	Desi Bank ${cardType}: 21.24%
This APR will vary with the market based on the Prime Rate.
APR for Cash Advances	Desi Bank S${cardType}: 26.24%
This APR will vary with the market based on the Prime Rate.
Penalty APR and When it Applies	Not applicable
How to Avoid Paying Interest on Purchases	Your due date is 24 - 30 days after the close of each billing cycle. We will not charge you interest on purchases if you pay your entire balance by the due date each month.
Minimum Interest Charge	If you are charged interest, the charge will be no less than $2.
For Credit Card Tips from the Consumer Financial Protection Bureau	To learn more about factors to consider when applying for or using a credit card, visit the website of the Consumer Financial Protection Bureau at http://www.consumerfinance.gov/learnmore
Fees
Annual Fee	
Annual Membership Fee	$29
Transaction Fees	
Balance Transfer	Either 3% of the amount of each transfer or $5 minimum, whichever is greater.
Convenience Check Cash Advance1	Either 3% of the amount of each advance or $5 minimum, whichever is greater.
Cash Advance ATM	Either 4% of the amount of each advance or $10 minimum, whichever is greater.
Cash Advance	Either 4% of the amount of each advance or $10 minimum, whichever is greater.
Cash Equivalent Advance	Either 4% of the amount of each advance or $20 minimum, whichever is greater.
Overdraft Protection	None
Foreign Transaction	2% of each foreign purchase transaction or foreign ATM advance transaction in U.S. Dollars
3% of each foreign purchase transaction or foreign ATM advance transaction in a Foreign Currency
Penalty Fees	
Late Payment	Up to $39
Returned Payment	Up to $35
Overlimit	None
How We Will Calculate Your Balance: We use a method called 'average daily balance (including new purchases)'. See your Cardmember Agreement for more details.
Billing Rights: Information on your rights to dispute transactions and how to exercise those rights is provided in your Cardmember Agreement.
The information about the costs of the card described in this application is accurate as of 04/2019. This information may have changed after this date. To find out what may have changed, call us at 1-800-285-8585 (we accept relay calls) or write to us at P.O. Box 6352, Fargo, ND 58125-6352.
1This product does not receive convenience checks.
How Variable Interest Rates Are Determined: After the introductory period, your interest rate is a variable rate and is determined by a combination of the Prime Rate (which may vary) added to a margin (which does not change). Because the Prime Rate may vary, your variable interest rate will go up or down if the Prime Rate changes. If you are granted an Account, the following rates on the Account are variable: Non-Introductory Purchase Rate; Non-Introductory Balance Transfer Rate; Cash Advance Rate. More information is available in the Cardmember Agreement.
Notice to New York Residents: You may contact the New York state department of financial services by telephone at (800) 342-3736 or visit its website at www.dfs.ny.gov for free information on comparative credit card rates, fees and interest-free periods.
Notice to California Residents: An applicant, if married, may apply for a separate account.
Notice to Married Wisconsin Residents: No provision of any marital property agreement, unilateral statement under section 766.59 of the Wisconsin statutes or court decree under section 766.70, adversely affects our interest unless we, prior to the time the credit is granted or an open-end credit plan is entered into, are furnished a copy of the agreement, decree or court order, or have actual knowledge of the adverse provision. IF YOU ARE A MARRIED WISCONSIN RESIDENT, CREDIT EXTENDED UNDER THIS ACCOUNT WILL BE INCURRED IN THE INTEREST OF YOUR MARRIAGE OR FAMILY.
Notice to Ohio Residents: The Ohio laws against discrimination require that all creditors make credit equally available to all creditworthy customers, and that credit reporting agencies maintain separate credit histories on each individual upon request. The Ohio Civil Rights Commission administers compliance with this law.
By submitting this application, you understand and agree that Desi Bank National Association ("we", "us" or "our"), as the creditor and issuer of your Account, will rely on the information provided here in making this credit decision, and you certify that such information is accurate and complete to the best of your knowledge. If we open an Account based on this application, you will be individually liable (or, for joint accounts, individually and jointly liable) for all authorized charges and for all fees referred to in the most recent Cardmember Agreement, which may be amended from time to time. We may request consumer credit reports about you for evaluating this application and in the future for reviewing Account credit limits, for Account renewal, for servicing and collection purposes, and for other legitimate purposes associated with your Account. Upon your request, we will inform you if a consumer report was requested and, if it was, provide you with the name and address of the consumer reporting agency that furnished the report. By providing us with a telephone number for a cellular phone or other wireless device, including a number that you later convert to a cellular number, you are expressly consenting to receiving communications including but not limited to prerecorded or artificial voice message calls, text messages, and calls made by an automatic telephone dialing system from us and our affiliates and agents at that number. This express consent applies to each such telephone number that you provide to us now or in the future and permits such calls for non-marketing purposes. Calls and messages may incur access fees from your cellular provider. By submitting this application, you also agree that we may verify your employment, income, address and all other information provided with other creditors, credit reporting agencies, employers, third parties, and through records maintained by federal and state agencies (including any state motor vehicle department) and waive any rights of confidentiality you may have in that information under applicable law. By submitting this application you certify that you read and understood the disclosures here and you agree to the terms of this application.
EXPANDED ACCOUNT ACCESS: Any Card or PIN issued to or selected by you under this Agreement can be used to access multiple checking, savings, line of credit and credit card account(s) held in your name with us or our bank affiliates; and any account you open with us and our affiliates may be accessed by the Card or PIN issued under this Agreement now or in the future. "Expanded Account Access" means use of a card or account number and PIN to conduct a transaction or obtain information at ATMs, over the telephone, through personal computer banking, or via any other available method. If the Card or PIN is for a joint account, the Card or PIN can be used to access all the accounts linked to the Card or PIN Access, whether joint or individual. There are no additional fees or charges for Expanded Account Access, but fees applicable to each applicable account will continue to apply in accordance with the terms of the applicable account agreements. Call Cardmember Service at 800-285-8585 to cancel Expanded Account Access, allowing a reasonable time for cancellation to become effective. If you cancel Expanded Account Access for any account, this Account will not be accessible by any card or PIN other than the Card or PIN issued under this Agreement.
Security Agreement
As a condition to obtaining my Desi Bank Secured Credit Card (the "Credit Card Account"), I hereby grant to Desi Bank National Association ("you") a security interest in, and assign and transfer to you all rights, title, and interest in my SSA held at Desi Bank National Association and all interest, additions, and proceeds therein to secure the payment and performance of my obligations to you associated with my Credit Card Account and all obligations to you under my Credit Card Account Cardmember Agreement.  I hereby instruct Desi Bank National Association to open the designated SSA in the name of "Desi Bank (Primary Applicant's name) Secured Credit Card Collateral Account" using the Primary Applicant's name and Social Security Number (SSN) or Taxpayer Identification Number (TIN) provided on this Application.  I agree that while this security interest is in effect, you will have the exclusive dominion and control and you will have the sole right and power to redeem, collect, and withdraw any part or the full amount of the SSA.  If I am in default under the terms of my Credit Card Account Cardmember Agreement, I agree that you will have all rights the law allows, including the right to take funds from the SSA and apply them to my Credit Card Account balance without notice to, or further consent from, me.

  
  </textarea>
  </div>
</div>
<div class="col-md-12 mb-5">
By checking the box below and clicking submit, you acknowledge and agree to the Terms and Conditions.	
  </div>
  <div class="form-group">

<div class="custom-control custom-checkbox">
  <input type="checkbox" class="custom-control-input" name="Agreement Checkbox" id="agreementCheckBox"/>
  <label class="custom-control-label" for="customCheck1"> Agree to terms and conditions</label>
</div>
  
</div>
		


			<div class="form-row" >
<div class="col-md-6 mb-5 col-md-offset-5" style="margin-bottom: 100px">
  <button type="submit" id="submitButton" class="btn btn-primary" style="margin-top: 30px">Submit Application</button>
  </div>
  </div>

  
</spring:form>
	
	</div>
</div>

	<section class="copyright"
		style="bottom: 0; width: 100%; /* Height of the footer */ background: #6cf; position: fixed;">
		<div class="agileits_copyright text-center">
			<p>
				2019 Desi Bank. All rights reserved | Design by <a
					href="//desibank.com/" class="w3_agile">Desibank</a>
			</p>
		</div>
	</section>

	<span id="ctx" style="display:none;"><%=request.getContextPath()%></span>
</body>
</html>