<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<head>
<title>Desi Bank</title>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    

<meta charset="utf-8">
<meta name="keywords" content="Corporate Bank a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- .css files -->
	<link href="css/bars.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/nstyle.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/font-awesome.css" />
<!-- //.css files -->
<!-- Default-JavaScript-File -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- //Default-JavaScript-File -->
<!-- fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Ropa+Sans:400,400i&amp;subset=latin-ext" rel="stylesheet">
<!-- //fonts -->
<!-- scrolling script -->
<script type="text/javascript">
	/* jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	}); */
</script>
<!-- //scrolling script -->

<script type="text/javascript">
$(function() {
    $("#cardMenu").children().click(function(){
    	let category=$(this).text().trim();
    	
    	$("#cardMenu").children().removeClass();
    	$(this).addClass("active");
    	
    	if(category.includes("All")){
    		$("#cardTable").find("tr").show();
    	}else{
    		$("#cardTable").find("tr").hide();
			$("#cardTable").find("tr").filter(".card_"+category.split(" ",1)).show();
    	}   	
    });
    
    $(".applyButton").click(function(){
    	let ctx=$("#ctx").text();
    	const params=jQuery.param({
    	    cardType: $(this).val(),
    	});
    	const url=ctx+"/creditcards/creditCardApplication?"+params;
    	console.log(url);
    	window.location.href = url;
    });
    
    
});


</script>



<style>
/* Add a black background color to the top navigation */
.topnav {
  background-color: #0d0169;
  overflow: hidden;
}

/* Style the links inside the navigation bar */
.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

/* Change the color of links on hover */
.topnav a:hover {
  background-color: #ddd;
  color: black;
}

/* Add an active class to highlight the current page */
.active {
  background-color: #4CAF50;
  color: white;
}

/* Hide the link that should open and close the topnav on small screens */
.topnav .icon {
  display: none;
}

</style>


</head>
<!-- //Head -->
<!-- Body -->
<body>



	<div class="top-main">
		<div class="number">
			<h3><i class="fa fa-phone" aria-hidden="true"></i> +91 080 987 6541</h3>
			<div class="clearfix"></div>
		</div>

		<div class="social-icons">
		<ul class="top-icons">
			<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
		</ul>
		<div class="form-top">
		  <form action="#" method="post" class="navbar-form navbar-left">
			<div class="form-group">
				<input type="search" class="form-control" placeholder="Search">
			</div>
				<button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
				<!-- <button type="submit" class="btn btn-default">Submit</button> -->
			</form>
		</div>
			<div class="clearfix"></div>
		</div>
			<div class="clearfix"></div>
	</div>
		<!-- Top-Bar -->
		<div class="top-bar">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="index.jsp">home</a></li>
							<li><a href="#services" class="scroll">services</a></li>
							<li><a href="${pageContext.request.contextPath}/customer-app-status.jsp">Status</a></li>
							<li><a href="#team" class="scroll">team</a></li>
							<li><a href="#payment" class="scroll">payment</a></li>
							<li><a href="#blog" class="scroll">blog</a></li>
							<li><a href="#contact" class="scroll">contact</a></li>
							<li>
								<a href="javascript:openLoginPage();" class="scroll" data-toggle="modal">login</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
		<div class="logo" style="text-align: left;margin-top: -50px;z-index:100;position: relative;width: 200px;">
			
			<img alt="" src="images/logo.jpg" style="height: 80px;"><a href="index.html"><!--<i class="fa fa-inr" aria-hidden="true"></i>--> <span style="color: black;font-size: 24px;" >DesiBank</span>
			</a>
		</div>
		<!-- //Top-Bar -->
		<div class="cbanner-main jarallax">
			<div class="container">
				<div class="banner-inner">
					<div class="col-md-5 cbanner-left">
					</div>
					<div class="col-md-7 banner-right">
						<h1>Account Processing Steps !</h1>
						<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec aliquet venenatis vehicula. Etiam malesuada arcu metus, sollicitudin ullamcorper leo lobortis ut.</h4>
							<div class="banner-right-text">
								<div class="main-icon">
									<i class="fa fa-share" aria-hidden="true"></i>
								</div>
								<p>Etiam felis tellus, interdum in fringilla ac, imperdiet sed mi.</p>
							<div class="clearfix"></div>
							</div>
							<div class="banner-right-text">
								<div class="main-icon">
									<i class="fa fa-share" aria-hidden="true"></i>
								</div>
								<p>Donec aliquet venenatis vehicula. Etiam malesuada arcu.</p>
							<div class="clearfix"></div>
							</div>
							<div class="banner-right-text">
								<div class="main-icon">
									<i class="fa fa-share" aria-hidden="true"></i>
								</div>
								<p>Etiam felis tellus, interdum in fringilla ac, imperdiet sed mi</p>
							<div class="clearfix"></div>
							</div>
							<div class="banner-right-text">
								<div class="main-icon">
									<i class="fa fa-share" aria-hidden="true"></i>
								</div>
								<p>Donec aliquet venenatis vehicula. Etiam malesuada arcu sed mi.</p>
							<div class="clearfix"></div>
							</div>
							<div class="banner-right-text">
								<div class="main-icon">
									<i class="fa fa-share" aria-hidden="true"></i>
								</div>
								<p>Etiam felis tellus, interdum in fringilla ac, imperdiet sed mi.</p>	
							<div class="clearfix"></div>
							</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>

<!-- services -->
<section class="services" id="services">
	<div class="container" style="font-size: 15px;">
	 <p>
	 <img alt="" src="${pageContext.request.contextPath}/images/app-status.png" style="height: 120px;"/>All Available Credit Cards</p>
	 
  <p>The .table-bordered class adds borders on all sides of the table and the cells:</p>    
  
<div class="topnav" id="cardMenu">
  <a href="#cardMenu" class="active">&nbsp;&nbsp;&nbsp;All&nbsp;&nbsp;&nbsp;</a>
  <a href="#cardMenu">Life Style</a>
  <a href="#cardMenu">Reward Cards</a>
  <a href="#cardMenu">Shopping Cards</a>
  <a href="#cardMenu">Travel & Fuel Cards</a>

</div>
  
       
  <table id="cardTable" class="table">
    <tbody>
    
    
    
    
    <c:forEach items="${cardTypeList}" var="cardType">
      <tr class="card_${cardType.category}">
<%--         <td><img src="${pageContext.request.contextPath}/creditcards/load-card-ccid?ccno=${cardType.cardTypeId}<%= java.lang.Math.round(java.lang.Math.random() * 899999999999999.0+10000000000000000.0) %>&name=Robert%20King&expdate=<%= java.lang.Math.round(java.lang.Math.random() *11 +1) %>/<%= java.lang.Math.round(java.lang.Math.random() *10 +10) %>&type=${cardType.cardTypeName}" style="height: 140px;"> --%>
         <td><img src="${pageContext.request.contextPath}/creditcards/load-card-ccid?ccno=${cardType.cardTypeId}" style="height: 140px;">
            
         <br/>
           <br/>
           <p> ${cardType.cardTypeName}</p>
         <br/>
      
          
         <a>Learn more</a><span class="glyphicon glyphicon-play"></span>
         
   <button id='applyCreditCardButton" t'pe="submit" value="${cardType.cardTypeName}" class="btn btn-primary applyButton" >Apply Now</button></td> 
         <td>${cardType.category}</td>
		<td>
        	<ul>
        	
        		<c:forEach items="${cardType.features}" var="feature"> 
	        		<li>${feature.feature}</li>	        	
        		</c:forEach>
        	</ul>
        </td>
         </tr>
   </c:forEach>
   
  
       	  <hr/>
	
	
	      
    </tbody>
  </table>
  <br/> <br/> 
  
  <h2><center>Have you applied for DESI bank Card already?</center></h2>
  <br/> <br/> 
  
  <table class="table">
    <tbody>
      <tr>
      	<td> 
 			<p><h3>Track Application</h3></p>
			<br/><br/>
  			<form action="/action_page.php">
    			<td  class="form-group">
      			<label for="track">Check the status</label>
      			<input type="track" class="form-control" id="track" placeholder="Application/Reference Number" name="track">
     	    	<button type="submit" class="btn btn-primary">Track</button>
     			</td> 
  			</form>
  		</td>
  		
  		<td>  
 			<h3>Retrieve Application</h3>
 		</td>
		<br/><br/>
		<td>   
  		<form action="/action_page.php">
    		<td class="form-group">
      		
      		<input type="PAN" class="form-control" id="pan" placeholder="PAN" name="Retrieve">
     	    <button type="submit" class="btn btn-primary">Retrieve</button>
     	    </td>
  		</form>
  		</td>
      </tr>
    </tbody>
  </table>
</section>
<!-- //services -->


<!-- modal -->
	<div class="modal about-modal fade" id="myModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header"> 
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="span1"aria-hidden="true">&times;</span></button>						
						<h4 class="modal-title"> Corporate<span> Bank</span></h4>
					</div> 
					<div class="modal-body">
					<div class="agileits-w3layouts-info">
						<img src="images/business.jpg" alt="" />
						<p>Duis venenatis, turpis eu bibendum porttitor, sapien quam ultricies tellus, ac rhoncus risus odio eget nunc. Pellentesque ac fermentum diam. Integer eu facilisis nunc, a iaculis felis. Pellentesque pellentesque tempor enim, in dapibus turpis porttitor quis. Suspendisse ultrices hendrerit massa. Nam id metus id tellus ultrices ullamcorper.  Cras tempor massa luctus, varius lacus sit amet, blandit lorem. Duis auctor in tortor sed tristique. Proin sed finibus sem.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- //modal -->
	
<!-- //contact -->

<!-- footer -->
<section class="footer">
	<div class="container">
		<div class="footer-grids">
			<div class="col-md-4 footer-grid1">
				<div class="logo1">
					<a href="index.html">Corporate <span>Bank</span></a>
				</div>
				<p> Donec in neque quis orci consequat lobortis. Sed non vestibulum mauris. Donec in neque quis orci</p>
				<p> Donec in neque quis orci consequat lobortis. Sed non vestibulum mauris. Donec in neque quis orci</p>
			</div>
			<div class="col-md-3 footer-grid2">
				<h4>Locations</h4>
				<p class="p1">Stoke Newington,London,</p>
				<p>Smith street,8814DM</p>
				<p class="p1">Paris,arrondissement</p>
				<p>on the Right Bank,2216TF</p>
				<p class="p1">Los Vegas,Nevada,</p>
				<p>Eiffel Tower road,2243FR</p>
			</div>
			<div class="col-md-2 footer-grid3">
				<h4>menu</h4>
					<p><a href="#index.html" class="scroll">home</a></p>
					<p><a href="#about" class="scroll">about</a></p>
					<p><a href="#services" class="scroll">services</a></p>
					<p><a href="#skills" class="scroll">skills</a></p>
					<p><a href="#team" class="scroll">team</a></p>
					<p><a href="#payment" class="scroll">payment</a></p>
					<p><a href="#blog" class="scroll">blog</a></p>
					<p><a href="#contact" class="scroll">contact</a></p>
			</div>
			<div class="col-md-3 footer-grid4">
				<h4>our links</h4>
				<p><a href="#">Funds transfer</a></p>
				<p><a href="#">Mobile banking</a></p>
				<p><a href="#">Deposits</a></p>
				<p><a href="#">New joint accounts</a></p>
				<p><a href="#">Internet online banking</a></p>
				<p><a href="#">Balance enquiry</a></p>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</section>
<!-- //footer -->

<!-- copyright -->
<section class="copyright">
	<div class="agileits_copyright text-center">
			<p>� 2017 Corporate Bank. All rights reserved | Design by <a href="//w3layouts.com/" class="w3_agile">W3layouts</a></p>
	</div>
</section>
<!-- //copyright -->

	<script src="js/jarallax.js"></script>
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript">
		/* init Jarallax */
		$('.jarallax').jarallax({
			speed: 0.5,
			imgWidth: 1366,
			imgHeight: 768
		})
	</script>

	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {

			$("#apprefno").keyup(function(){
				 $("#emessage").html("");
			});
			$("#appstatus").click(function(){
				  	var contextPath="${pageContext.request.contextPath}";
				  	var apprefno=$("#apprefno").val();
				  	if(apprefno.length==0){
				        $("#emessage").html("please enter your app ref no.");
				        $("#apprefno").focus();
				        return;
				    }  
				  //contentpath/v1/customer/app/status?appRefNo=AS0292022
					$.ajax({url:contextPath+"/v1/customer/app/status?appRefNo="+apprefno, type: 'GET',success:function(data) {  //data= this.responseText
						console.log(data);
						if(!data.name){
						     $("#emessage").html("Hey this app ref "+apprefno+" is not valid!!!!!");
						}
						$("#sname").html(data.name);
						$("#semail").html(data.email);
						$("#sstatus").html(data.status);
						$("#sdoa").html(data.doa);
						$("#smobile").html(data.mobile);
						$("#slocation").html(data.location);
					} //end of function
				});	 //end of the ajax			
			});
								
	});
	</script>
	<!-- //here ends scrolling icon -->
	<script src="js/bars.js"></script>
	<script type="text/javascript">
	function InvalidMsg(textbox) {
	     if(textbox.length==0){
	        textbox.setCustomValidity('please enter your userid.');
	    }    
	    else {
	        textbox.setCustomValidity('');
	    }
	    return true;
	}
	
	</script>
	
	  <!-- Modal -->
  <div class="modal fade" id="loginModel" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
        </div>
     	<div>
			<div class="container">
				<div>
					<div class="col-md-5">
						<form id="loginForm" action="${pageContext.request.contextPath}/j_spring_security_check" method="post">
							 <img src="images/login.png" style="display: inline;"/><h5 class="modal-title" style="display: inline;font-size: 16px;">Login</h5>
							 <br/>
							 <br/>
							<span id="errorMessage" style="color: red;font-size: 15px;"></span> 
							<table>
							
									<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>Username</td>
									<td><input type="text"  id="userid" name="j_username" class="form-control"  style="background-color: #d9edf7;margin-left: 20px;width: 350px;color:black;"></td>
								</tr>
								
									<tr>
									<td colspan="2" id="userError">&nbsp;</td>
								</tr>
								<tr>
									<td>Password</td>
									<td><input type="password"  id="password"   name="j_password" class="form-control" style="background-color: #d9edf7;margin-left: 20px;width: 350px;color:black;" ></td>
								</tr>
									<tr>
									<td colspan="2" id="passwordError">&nbsp;</td>
								</tr>
									<tr>
									<td>&nbsp;</td>
									<td>&nbsp;
									<input type="button" name="login" style=" color: #FFFFFF;
    background: #ffb900;
    border: 2px solid #ffb900;
    text-transform: uppercase;
    padding: .2em 1em;
    font-size: 1.3em;
    font-family: 'Ropa Sans', sans-serif;" value="Login"  id="loginButton"></td>
								</tr>
									<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
									<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
							</table>							
						</form>
					</div>
					<div class="clearfix"></div>
				</div>
				<span id="ctx" style="display:none;"><%=request.getContextPath()%></span>
  
</body>
<!-- //Body -->
</html>
<!-- //html -->