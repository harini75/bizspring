<%@ taglib prefix="spring"
uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<head>
<title>Desi Bank</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords"
content="Corporate Bank a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<!-- .css files -->
<link href="css/bars.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
href="${pageContext.request.contextPath}/css/bootstrap.min.css"
type="text/css" media="all" />
<link rel="stylesheet"
href="${pageContext.request.contextPath}/css/nstyle.css"
type="text/css" media="all" />
<link rel="stylesheet"
href="${pageContext.request.contextPath}/css/font-awesome.css" />
<!-- //.css files -->
<!-- Default-JavaScript-File -->

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript"
src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript"
src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<!-- //Default-JavaScript-File -->
<!-- fonts -->
<link
href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
rel="stylesheet">
<link
href="//fonts.googleapis.com/css?family=Ropa+Sans:400,400i&amp;subset=latin-ext"
rel="stylesheet">
<!-- //fonts -->
<!-- scrolling script -->
<script type="text/javascript">
jQuery(document).ready(function($) {

$("#datepicker").change(function(){
console.log($(this).val());
var sdate=$(this).val();
if(sdate.length<10){
return;
}
var context="${pageContext.request.contextPath}";
//methd =GET , Accept = "application/json"
$.getJSON(context+"/v1/customer/find/age?pdate="+sdate,function(data){
//{"age" : 10}
console.log(data);
$("#age").val(data.message);	
});
});

$("#datepicker").datepicker({
 dateFormat: "yy-mm-dd"
});

$(".scroll").click(function(event) {
event.preventDefault();
$('html,body').animate({
scrollTop : $(this.hash).offset().top
}, 1000);
});
});
</script>
<!-- //scrolling script -->
</head>
<!-- //Head -->
<!-- Body -->
<body>
<jsp:include page="top-bar.jsp" />
<!-- Top-Bar -->
<jsp:include page="admin-menu.jsp"/>

<section class="blog" id="blog" style="margin-bottom: 20px;">
<div class="container">
<div class="blog-heading">
<h3 style="text-align: left; font-size: 20px; display: inline;">Employee
Creation Page</h3>
&nbsp;&nbsp;&nbsp;&nbsp; <img alt=""
src="${pageContext.request.contextPath}/images/add-payee.png"
style="height: 80px;" /> <br /> <br />

<span style="font-size: 16px;color:red;" id="errorMessage">${msg}</span>
<%-- <h4 style="text-align: left; font-size: 20px; display: inline;">
&nbsp;&nbsp;Name :&nbsp;&nbsp;&nbsp;&nbsp;<b>${customerSavingForm.name}</b>
</h4>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<h4 style="text-align: left; font-size: 20px; display: inline;">&nbsp;&nbsp;Email
:&nbsp;&nbsp;&nbsp;&nbsp;${customerSavingForm.email}</h4>
<h4 style="text-align: left; font-size: 20px; display: inline;">&nbsp;&nbsp;Mobile
:&nbsp;&nbsp;&nbsp;&nbsp; ${customerSavingForm.mobile}</h4>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<h4 style="text-align: left; font-size: 20px; display: inline;">&nbsp;&nbsp;Address
:&nbsp;&nbsp;&nbsp;&nbsp;${customerSavingForm.location}</h4>
<hr /> --%>
<%-- <spring:form method="POST"
action="${pageContext.request.contextPath}/customer/registration.htm"> --%>
<spring:form method="POST"
action="${pageContext.request.contextPath}/employee/accounts/creation"
enctype="multipart/form-data" id="employeeAccountForm">
<table class="table" style="width: 80%;">

<tr>
<td>UserID</td>
<td><input type="text" id="userid" name="userid"
class="form-control"
style="background-color: #d9edf7; margin-left: 20px; width: 250px; color: black;" onkeyup="clearMessage();"></td>

<td>Email</td>
<td><input type="email" value="${customerSavingForm.email}"
id="email" name="email" class="form-control"
style="background-color: #d9edf7; margin-left: 20px; width: 250px; color: black;" onkeyup="clearMessage();"></td>
</tr>

<tr>
<td>Name</td>
<td><input type="text" value="${customerSavingForm.name}"
id="name" name="name" class="form-control"
style="background-color: #d9edf7; margin-left: 20px; width: 250px; color: black;"></td>
<td>Phone</td>
<td><input type="tel" value="${customerSavingForm.mobile}"
id="mobile" name="mobile" class="form-control"
style="background-color: #d9edf7; margin-left: 20px; width: 250px; color: black;" onkeyup="clearMessage();"></td>
</tr>

<tr>
<td>GENDER</td>
<td><select id="gender" name="gender" class="form-control"
style="background-color: #d9edf7; margin-left: 20px; width: 250px; color: black;">
<option>Male</option>
<option>Female</option>
</select></td>

<td>Job title</td>
<td><input type="text" id="jobTitle" name="jobTitle"
class="form-control"
style="background-color: #d9edf7; margin-left: 20px; width: 250px; color: black;"></td>
</tr>

<tr>
<td colspan="4" style="text-align: right;">
<input type="button" name="createEmployee"
style="color: #FFFFFF; background: #ffb900; border: 2px solid #ffb900; text-transform: uppercase; padding: .2em 1em; font-size: 1.3em; font-family: 'Ropa Sans', sans-serif;"
value="Create" id="createEmployee" onclick="validateForm();"></td>
</tr>
</tbody>
</table>
</spring:form>
</div>
</div>
</section>

<br/><br/><br/>

<!-- modal -->
<div class="modal about-modal fade" id="myModal" tabindex="-1"
role="dialog">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal"
aria-label="Close">
<span class="span1" aria-hidden="true">&times;</span>
</button>
<h4 class="modal-title">
Corporate<span> Bank</span>
</h4>
</div>
<div class="modal-body">
<div class="agileits-w3layouts-info">
<img src="${pageContext.request.contextPath}/images/business.jpg"
alt="" />
<p>Duis venenatis, turpis eu bibendum porttitor, sapien quam
ultricies tellus, ac rhoncus risus odio eget nunc. Pellentesque
ac fermentum diam. Integer eu facilisis nunc, a iaculis felis.
Pellentesque pellentesque tempor enim, in dapibus turpis
porttitor quis. Suspendisse ultrices hendrerit massa. Nam id
metus id tellus ultrices ullamcorper. Cras tempor massa luctus,
varius lacus sit amet, blandit lorem. Duis auctor in tortor sed
tristique. Proin sed finibus sem.</p>
</div>
</div>
</div>
</div>
</div>
<!-- //modal -->


<!-- copyright -->
<jsp:include page="../common/copyright.jsp" />
<!-- //copyright -->

<script src="js/jarallax.js"></script>
<script src="js/SmoothScroll.min.js"></script>
<script type="text/javascript">
/* init Jarallax */
$('.jarallax').jarallax({
speed : 0.5,
imgWidth : 1366,
imgHeight : 768
})
</script>

<script type="text/javascript"
src="${pageContext.request.contextPath}/js/move-top.js"></script>
<script type="text/javascript"
src="${pageContext.request.contextPath}/js/easing.js"></script>

<!-- here stars scrolling icon -->
<script type="text/javascript">
	function clearMessage(){
		$("#errorMessage").html("");
	}
    function validateForm(){
    	var userid=$("#userid").val();
    	var email=$("#email").val();
    	var mobile=$("#mobile").val();
    	if(userid.trim().length==0){
    		$("#errorMessage").html("user id cannot be blank!");
    		$("#userid").focus();
    		return;
    	}
    	
    	if(email.trim().length==0){
    		$("#errorMessage").html("email cannot be blank!");
    		$("#email").focus();
    		return;
    	}
    	
    	if(mobile.trim().length==0){
    		$("#errorMessage").html("mobile cannot be blank!");
    		$("#mobile").focus();
    		return;
    	}
    	var contexpath="${pageContext.request.contextPath}";
    	var obj={userid:userid,email:email,mobile:mobile};
    	fetch(contexpath+'/v1/employee/validate-employee-info', {
			method: 'POST',
			headers: {
	            'Content-Type': 'application/json',
	            'Accept': 'application/json',
	            // 'Content-Type': 'application/x-www-form-urlencoded',
	        },
			body: JSON.stringify(obj)
		}).then(function(response){
				return response.json();
			}).then(function(data){
					console.log(data);
					if(data.status=="success"){
						//submit form using jQuery
						//employeeAccountForm - > this is the id of form which you want to submit
						$("#employeeAccountForm").submit();
					}else{
						$("#errorMessage").html(data.message);
					}
					//if data is okay submit the form
					
			});
    	///
    	//now let;s make ajax call and ensure userid ,email and mobile do not exist into the database!
    	//fetch().then().then(data);
    	
    }

</script>
<!-- //here ends scrolling icon -->
<script src="js/bars.js"></script>
<script type="text/javascript">
function InvalidMsg(textbox) {
if (textbox.length == 0) {
textbox.setCustomValidity('please enter your userid.');
} else {
textbox.setCustomValidity('');
}
return true;
}
</script>


</body>
<!-- //Body -->
</html>
<!-- //html -->