<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- .css files -->
	<link href="${pageContext.request.contextPath}/css/bars.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/nstyle.css" type="text/css" media="all" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css" />
<!-- //.css files -->
<!-- Default-JavaScript-File -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<!-- //Default-JavaScript-File -->
<!-- fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Ropa+Sans:400,400i&amp;subset=latin-ext" rel="stylesheet">
<!-- //fonts -->

<title>Login Page</title>
 <script type="text/javascript">
 		$(function(){

 			$("input[type='text'][name='username']").keyup(function(){
 				$("#errorMessage").html("");
 		     });
 			$("input[type='password'][name='password']").keyup(function(){
 				$("#errorMessage").html("");
 		     });
 	 		
			$("#loginButton").click(function(){
				console.log("Hey!");
				let username=$("input[type='text'][name='username']").val();
				console.log("username = "+username);
				let password=$("input[type='password'][name='password']").val();
				console.log("password = "+password);
				if(username.length==0){
					$("#errorMessage").html("Username cannot be empty.");
					$("input[type='text'][name='username']").focus();
					return;
				}

				if(password.length==0){
					$("#errorMessage").html("Password cannot be empty.");
					$("input[type='password'][name='password']").focus();
					return;
				}

				//submit form using jQuery
				$("#loginform").submit();

			});	
 	 	});
 </script>
</head>
<body>
	<div class="top-main">
		<div class="number">
			<h3  style="color:white;font-weight: bold;"><i class="fa fa-phone" aria-hidden="true"></i>Customer Care  :  +91 080 987 6541</h3>
			<div class="clearfix"></div>
		</div>
		<div class="social-icons">
		<ul class="top-icons">
			<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
		</ul>
		<div class="form-top">
		  <form action="#" method="post" class="navbar-form navbar-left">
			<div class="form-group">
				<input type="search" class="form-control" placeholder="Search">
			</div>
				<button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
				<!-- <button type="submit" class="btn btn-default">Submit</button> -->
			</form>
		</div>
			<div class="clearfix"></div>
		</div>
			<div class="clearfix"></div>
	</div>
		<!-- Top-Bar -->
	<div class="top-bar">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
						<ul class="nav navbar-nav navbar-right">
							<li><a href="${pageContext.request.contextPath}/">home</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>

<div class="container">
	<br/><br/><br/>
    	  <!-- Modal -->
      <!-- Modal content-->
							 <img src="${pageContext.request.contextPath}/images/login.png" style="display: inline;"/>
							 
							 <h5 class="modal-title" style="display: inline;font-size: 16px;">You have successfully logged out from the application.</h5>
							 <br/>
							 <br/>
							<span id="errorMessage" style="color: red;font-size: 15px;">${errorMessage}</span> 
							<a href="${pageContext.request.contextPath}/corp/auth">
							
							<input type="button" name="login" style=" color: #FFFFFF;
    background: #ffb900;
    border: 2px solid #ffb900;
    text-transform: uppercase;
    padding: .2em 1em;
    font-size: 1.3em;
    font-family: 'Ropa Sans', sans-serif;margin-top: 200px;" value="Click here to login"></a>
					<div class="clearfix"></div>
				<div class="clearfix"></div>
				 <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
  
</div>

<!-- copyright -->
<section class="copyright" style="bottom:0; width:100%; /* Height of the footer */  background:#6cf;position: fixed;">
	<div class="agileits_copyright text-center">
			<p>2019 Desi Bank. All rights reserved | Design by <a href="//desibank.com/" class="w3_agile">Desibank</a></p>
	</div>
</section>
</body>
</html>


