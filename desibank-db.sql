-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: desi_bank_db
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_number_generator_tbl`
--

DROP TABLE IF EXISTS `account_number_generator_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `account_number_generator_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountNumber` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_number_generator_tbl`
--

LOCK TABLES `account_number_generator_tbl` WRITE;
/*!40000 ALTER TABLE `account_number_generator_tbl` DISABLE KEYS */;
INSERT INTO `account_number_generator_tbl` VALUES (1,12233450);
/*!40000 ALTER TABLE `account_number_generator_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_account_information_tbl`
--

DROP TABLE IF EXISTS `customer_account_information_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `customer_account_information_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `accountNumber` varchar(255) DEFAULT NULL,
  `accountType` varchar(20) DEFAULT NULL,
  `avBalance` float NOT NULL,
  `branch` varchar(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `customerId` varchar(255) DEFAULT NULL,
  `payeeName` varchar(255) DEFAULT NULL,
  `statusAsOf` datetime DEFAULT NULL,
  `tavBalance` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1007 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_account_information_tbl`
--

LOCK TABLES `customer_account_information_tbl` WRITE;
/*!40000 ALTER TABLE `customer_account_information_tbl` DISABLE KEYS */;
INSERT INTO `customer_account_information_tbl` VALUES (1000,'0031051237467','Saving',1000,'Fremont','$','yadna0192','NAGENDRA','2018-02-22 14:20:43',1000),(1003,'00315112233447','Saving',10000,'Fremont','$','openclose',NULL,'2018-09-24 11:41:51',10000),(1004,'00315112233448','Saving',10000,'Fremont','$','poieioe29292',NULL,'2018-09-24 11:44:54',10000),(1005,'00315112233449','Saving',10033,'Fremont','$','test',NULL,'2019-01-08 16:25:36',10033),(1006,'00315112233450','Saving',9967,'Fremont','$','tiz',NULL,'2019-01-10 16:24:11',9967);
/*!40000 ALTER TABLE `customer_account_information_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_question_answer_tbl`
--

DROP TABLE IF EXISTS `customer_question_answer_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `customer_question_answer_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `userid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_16a9exybxl383l2meaxr8xv1e` (`userid`),
  CONSTRAINT `FK_16a9exybxl383l2meaxr8xv1e` FOREIGN KEY (`userid`) REFERENCES `user_login_tbl` (`loginid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_question_answer_tbl`
--

LOCK TABLES `customer_question_answer_tbl` WRITE;
/*!40000 ALTER TABLE `customer_question_answer_tbl` DISABLE KEYS */;
INSERT INTO `customer_question_answer_tbl` VALUES (1,'goa','What is your birth place?','admin'),(2,'rdj','What is the name of your childhood hero?','admin'),(3,'goa','What is your birth place?','nagen@synergisticit.com'),(4,'rdj','What is the name of your childhood hero?','nagen@synergisticit.com'),(5,'Powiw','What is your birth place?','poieioe29292'),(6,'jack','What is the name of your childhood hero?','poieioe29292'),(7,'wer','What is your birth place?','openclose'),(8,'werer','What is the name of your childhood hero?','openclose'),(9,'111','What is your birth place?','test'),(10,'222','What is the name of your childhood hero?','test'),(11,'123','What is your birth place?','tiz'),(12,'123','What is the name of your childhood hero?','tiz');
/*!40000 ALTER TABLE `customer_question_answer_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_saving_enquiry_tbl`
--

DROP TABLE IF EXISTS `customer_saving_enquiry_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `customer_saving_enquiry_tbl` (
  `csaid` int(11) NOT NULL AUTO_INCREMENT,
  `doa` datetime DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `appref` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`csaid`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_saving_enquiry_tbl`
--

LOCK TABLES `customer_saving_enquiry_tbl` WRITE;
/*!40000 ALTER TABLE `customer_saving_enquiry_tbl` DISABLE KEYS */;
INSERT INTO `customer_saving_enquiry_tbl` VALUES (2,'2018-09-18 11:38:33','synergisticit2020@gmail.com','JAPAN','0192928282','Jhon','APPROVED','AS-1537295913028'),(3,'2018-09-21 15:25:48','nagendra.synergisticit@gmail.com','12321321','123123','King jack','PENDING','AS-1537568748268'),(5,'2018-10-12 10:34:39','nagen@synergisticit.com','Fremont!!!!!','+1-2892-2892822','Rest Template','PENDING','AS-1539365679392'),(15,'2019-01-07 20:57:04','synergisticitsessionusc2@gmail.com','London','7788778878','Kuro','APPROVED','AS-1546923423885'),(16,'2019-01-10 16:20:25','leoniiz.t@gmail.com','London','7788778878','Tiz','APPROVED','AS-1547166024686');
/*!40000 ALTER TABLE `customer_saving_enquiry_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_security_questions_tbl`
--

DROP TABLE IF EXISTS `customer_security_questions_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `customer_security_questions_tbl` (
  `qid` int(5) DEFAULT NULL,
  `questions` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_security_questions_tbl`
--

LOCK TABLES `customer_security_questions_tbl` WRITE;
/*!40000 ALTER TABLE `customer_security_questions_tbl` DISABLE KEYS */;
INSERT INTO `customer_security_questions_tbl` VALUES (1,'What is your birth place?'),(2,'What is your mother\'s maiden name?'),(3,'What is your favourite author\'s name?'),(4,'What is your pet name?'),(5,'What is your favourite soccer team?'),(6,'What is the name of your childhood hero?'),(7,'What is your father\'s middle name?'),(8,'What is the name of your first crush?'),(9,'What was the name of your first school?'),(10,'What is your favourite vacation spot?');
/*!40000 ALTER TABLE `customer_security_questions_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_tbl`
--

DROP TABLE IF EXISTS `customer_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `customer_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountNum` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `age` int(11) NOT NULL,
  `approved` varchar(255) DEFAULT NULL,
  `dob` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `father` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `image` longblob,
  `jobTitle` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `photoName` varchar(255) DEFAULT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  `ssn` varchar(255) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_5np8f9dvwel6067uguhtqmd67` (`userid`),
  CONSTRAINT `FK_5np8f9dvwel6067uguhtqmd67` FOREIGN KEY (`userid`) REFERENCES `user_login_tbl` (`loginid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_tbl`
--

LOCK TABLES `customer_tbl` WRITE;
/*!40000 ALTER TABLE `customer_tbl` DISABLE KEYS */;
INSERT INTO `customer_tbl` VALUES (1,'20202020','USA',11,'yes','19-11-12','amog.niit@gmail.com',NULL,'male',_binary '�\��\�\0JFIF\0\0\0\0\0\0�\�\0�\0	\Z (\"%!1!%)+...383,7(-.+\n\n\n\r,$ $,,,4/,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,��\0\0\�\0\�\0�\�\0\0\0\0\0\0\0\0\0\0\0\0\0�\�\0D\0	\0\0\0\0!1AQa\"q��2BR���\�#br\��3CS�����\�\�$s4T�\�\0\0\0\0\0\0\0\0\0\0\0\0\0�\�\04\0\0\0\0\0\0!1AQ\"2aq����\��3BR�#\��b�\�\0\0\0?\0\�0\0@\0\0\0@\0\0\0@\0\0\0@\0\0\0@\0\0\0@\0\0\0@\0\0\0@\0\0\0@\0\0\0@\0\0\0@���\0\��\��(��ˣWQ\�k�h�T{#\'Z�\�R{k%ƅJ\�I�\�5XJ��\�SEEm�{�_�~���4g�\�m�\Z�S��\�e\�\�G�;[`�\�-=����E��,\\8ܺ\�\�\�+G@�@�\�#7���\Z,E7\�`\�\�X�T9c�F���$A!\0\0\0@\0\0\0@\0\0F�\�l������D��\0\0@\0LL��U�)I-�h�����f����9�y\nHb8:�}劂\�QT\�٬�ʹ���W\�pQ�\�\�X\��\n&qه5uC�z�\�8zq\�J�In\�p�ROi&5I-��os\�H&�F\Z4q\�\�x)iI�&*\�F;�YBOdz�\�\Zw�n��´�\�`�\�-�8In�1b�\0\0zB\�MA ��4�\���\ZJm\��\�8/�\�\�\�K\rN\\=����#\�\r�I��\���q\�\��	�\�3Z�\�\�J\�!\�\�B��a9E\�١�\�I]EK\0\0@\0\0Q\�1$˶V��8�֕\'RVFuj*q�9�#���\�\�\��\�Zta\r�̝Y\�vP�L\�L\��\�uc�G\�t\��H��%�c	m��F�\�P\�R\�\�|,iME\�\�ᶎ	&2��5�\�r>+l\�\�\���l���\"�\�\�L/\�\�>\�߯�5��\�g,MG\�T\�Q��Q\�I1�Ih�[os\�I\�\�)\�)[����\�\\�ŉ\�8\�R�\�\�7r���J�����쌛��s����@.\�3�\�\�,WJ��\�T�F�ݩ|����@M[̢nVJ��x\�KVOqIc�I\�o �\�6��)�4RJ��c*ǍɇHV�\�\�́�NrD\�Z�ĸ4PW��ƺ�\�vG_\�m�K�ɎӫF��\�/�6\�N(\�\�aƕ�: �#q�\�*Fj�3��r.T \0�\0,\�O-�fmT;\�\�ȍ�IӌդZ�Ѽ���L\n��\�N�\�\�9u��\�\�R��\�8�\�\�\0\0 \0�w�X�u�袩�S\��\�\�S\�r�3O\�M\0\0@��eUk�\�i�|C�\�\'fK3E`\�@\�(��\�V\�J\�JT�=m[\nw\�]D��rT`Ģ\�\�jD:\����7��*r�T���5�`�U�\�A\�3(҉��v�\�$_=6�[[�4J\'��=\�2��m��\\���\\Ӟx�k�ѤjH\���gx��\�\�%RN+���2�E\����~􀪄Y\\@�a\�\�fԡI\�*ړKŲ��S\��uVH�\��A�Pq�\�\�w�{�q\�M�GB�ɐ�G\�8r����\�y\�|v:;\Zӳ�\0�\�a�|D:�w�\�#\�6\�ݶ�����ٖ:�x�\�\Z\�4�HU�\�d�r�B�\��e�\��\�ZK��j�ޢ��[Ǳ\�$�m\�����\0Zk|\�O��D\�b��\nu�_\�1eґn\�|\�/���g�#F\�Ֆn]\�bw�\'ƀ�S\�ӟ\�\�\�3+Ӓf�Ju$-��\'r�k���%u�1i\�\�l�{i\�Co�Aϒ���|/\�Oퟹ��x�@\0\�=���\�I>�hG4�+\'h�r�c�q��\0@\02ʔh�I�m-���N\�ʄ�ÉJ\��0J��\�)\�\nB��gba	Ϻ��\�\�\�d��N\�r�\0�>$Dd�>\�\��~E���@��\'@\ZY�\�E��4_��k\�\�E3�b�\ny�\�K?�1+?佉�\Z/m}M�\��h\�*]\�h�\��h\�4\�|Ej(�vv=\�ʘ���LˍI��RU�\�Z�����_IbT)\�ź4\�U�\r\�\0eM\�\�<��gM��\�RT%6\��\\(�Z[AJz�*��!i�T��.�bz��ŭ�m.	@�-S�\'aL\���[N�\�\��{\�S�]\�\�JR�%%�\�\�\�	[*q+	=R3W-7�\�5\��(ԕ֜\�qU!Vy��\�\�8�Q�TF9u����	�Ҝu�ص�;�A�\�Z|�\�2������z+HPWq�`�v��>��ǁ\���f��f�\�L�Բ�\0�޴Nc\�o!\�.2�e��\�\�U\���\�\�f\�n��d|\�\�|#\�Q��/J\�]7\�t\r�\�\�j\r�jٰ\'\�;��\�bp\�K4w��a븼�\�\�G0\�\0\'\�ǲʯ\�Q>&�\�\�Xު0\�;Sg:��\�\0�(�\�(\rJ�~�\���K(\�H���\�\�\�u\�숓\�\�C\�@�eħ:�u�\�PRw[�R���l�\�i�d0Z�	̣��\�<Τ��\ZB1�uJ���m���V��^\�]V嘙�B�AYI\�ݑNʹث�B�<+)(\�\�*O�!�۩DY*S�҈I��\n\��\�ӏ�G\r7��\�\'\'�m�\�˝O\�9V���\�r�])eh�z��V����+`�Hni\�Q	Ȅ\�vPI��N�\�ת\�N7~\"��C�]\�\�ٶ\�%2TЈ^+��{�\�D�U�u1kKR�\�̳~�\�\'�\����r{#XѨ���\�m\��Ԣ2\�\0A#���\�Ӝm3\�z!�`ZY�����~�ɦ���[MH\�?ED�QN\�_̕WZ1o\���\"N\�\�\n��7\r=���Y�D����\�\�{\�^����3rd�OH\�4K\�\�\�46\�U\n���|��zU\�\�\��3i\'2�[K���7²���9󋄜e�q\�%\�\�Ax2�\�2�\�=tu�A��f\�1\�\�\�L;\�r��\�yC\�\�\�s�P�h��\��:�˂\�ζ:�J�\�d6���˅gT�\�\�ʝ\' \'rQZS��\�!*���ya\�Gf0�U�yZ\�\�J1F\�\�eImԶ�͇T[^�b�.�\�\�3��JԂ�Z-2\�ZR���@x�*#֧utqZ��:f\�Ne\�M\�:��[\�H\�b!��GV��A1�`lf6�\�4\�x��\0�a\�\�7\�)�}���d��{D^��\�\�js�`��N:�\�I���Ml#9VI\�vFy��m5D��\�Mn\�\�F��\�W���Tw���\�b8Y=d\�Von�Ӭ�\n\�J~)1gJ�$�K�$�Wڙ鱐%2��K\"�G �Pw\n\�V�\�\�^hGW�sH�l�Vovu}I$\�>&y �Ujn\�Z��v逬�%Ʌ\�I��*{��jc)\�mAa�k\�\��\�&�	�l��\���\�6�Jp�\�\�VUp��\0��_�\�}��B�0\�\��Ne\��\�Q̩���\�t�\�h��\�L2\�!��7��\�}�J[�)ל�\�\�\�&�*&\�2n\�-\�k,\�	\�[�\�h�t��y�\n*�\�F_v`b[h�\�2\�x ug\��\�@�����\�J=\�/O��\�N����\�Q�\�K��%�^p\�m�\'JНkFu*=e�\�c\rV�Y7)��V\�\�\�\��ÙI)\�и�k�Q�G�X+NRw\�e��R���[��\�\�\�`-9s\�\�\�\�\�jr�SlYJ\�\�+.d�����s�\�t0�W��\�\��}�j�x}ϻM�8��\��A.������nXJ�{WJ\��ڣ�C�J-Z6)l\�*\��\�r˱\�j+�C���\�\�jF5U����u��\�8$�\0�!.2�\�U�\r\�k��0�YFVg�t�	\�z1n\�̭+nFN�3��z)\���Nþ�\�$`�\�l�lw\r�S�g����\�Y�.A�l9QPy\�W��R.\n�7\�\0��҅z�u��Oo\�w��$t\�r��8T�\�]IJH�n	�4�\��\�­_�l�\0F�nP�\�s�@\�Ξ����Р!�<\�3�\�\"\�dXͭ`Y\�H�xf��s���X\�u=�w\�\�O\�?(S\Z�I�\�\�e�Q	\��]\���\�\n\�O\�G�H[&��\�N��P�T�[U|cj�j\�\r[�9\�3���9�`�dݦ}�?x\�u@;\�R���=�/�ѥMR^#)\\0��%T�M����3�f�n\�\�-�pA�\�u�\�̝y\\\�b;@��\�I7\�\�\\WP�\�\�*�\�8�li�?ѮW%���=Iliu]$��\�\r�0HJG|�;c\�\�\�NoAZ�\�vi+/��U˜���d\0>�\�Tmݳ�:����ȧ.TSKV3r3\�\�Z@R\�.�Dj\�ՖĞK�ڼbr\�pk�u2�R�\�\���e/���p\rEo���X�|�&�Qݻ��`С�\Z�u\��;�B)�){��\ZX\�<\��\�[�\�z X�s\�ިv��੯S���\Zj����\r�\�\�=Z\�T4�\�lF\�9\�\\Qk�J�\��27\'�\�\�s�0�\�Ԗ\�\��fԞw\�\�G�ε%�H�\��\�Ve\�\'(Q\�45\�Hr��7s\�ֆT�;/����c}�\�u���\�J��s��f�$��\�)���q\�\�l!�\'xX*�m\�E7{�*\�G(\�`6�Rn�$\�]���\�$�S^̼\�8DV�\�79�\��;;�\0G\\�a�(4�\�\�17֯�ר�ʔ?\�\�8�귗W���MS��6��\�QS\���B3;\Z�JJr\�\�[�\�\�2\�/�,|��a�����zz�ַᘏ\�\n\�\�\�\�5�.RrQWe|6L2\�\ZN�HOm�\��\�\��QG\ZR\�\�6�.�\n#�P�;h��\���\�\��x�e��\'%�\�DGG��\�\�\�c;\�\�\��B�.-I�h�֩=\Z;\n�Q�#�Z�:\�\�\�\�\\,-y�Kt=\�U_p��ps\�;>$��t�K�BnN�\ro^�+V�V�e\�1\'�%��B[�M�x\�f\�9r\�63���Y\�ì\�\��F�	�l��R\�Y�Vy���8*��\�\�V\�N��\"i\�)6�\�o�\�^�%:�[p�ح�� �\��\�R�\�_)l�֥	��\�%\"m���Q\��X�2\�g\�%B���O+��,Ib��J \Z�&�\�z\"�M��\�L��u%7+\0�*4>\�Ƙh�TW\�\�\n\nu��k\�mpY7a�\�l�\0w^��\���9f�fuju�r|J��j\r�e3��h�yk�R\�;\0�\�:���\�::9!\��4IG)WH8U<c\�\�U���ǥN�N�&\�\�\��\'(݈֊������@\�	6\���D���PPxG�\�Z�,��lm\Z8�R\��f\�e��wF\�M7��%#�4�\�\n\r\�\'�qm�x\�;3:�ZanP��A\0\�<-Aؑ\�N1�ב\�\�I\�D\�\�:\"F�`\\j��S��\�\�\�u�`\�\�\�B�#l\\\�4�pBO��t���\��9���\0����W�\�L�*RT�\r\�\�B7P�q�(�\�\��dFk�PFeXR�6\�K\�4zZ\�\�\�\�8�I\�\�g$٫�g\"�!�S�G����\0�\�bsT#�Z\�\�j\�e�XKM�\0\\�\��\�Ǟ�ܵ�\�\Z�G7y;���YF\�Ɍ\�3m2iqZ�����\��(\�9r�(\�T񮔋��e���\��\�I\�Pk\��\�$�$�\�I�< \"\�\�	\�4�4���I$�\Zi�I\�J�\"�7�9�-��a\�	tgC+K�5!A�Sʕ�j\�\Z=Z�ޯO#��\�\�\�Rݦ�<N���\�&Y\"�MAI\�*\�/R�95\�J��\�(�X�:�ڵ�c9�FS�\�9+R9e\��\�\��\0��\��d>1\�q�;x���\�~F\�I�\�*\n\�2��PG6Ri�c�c�\�4Kͤ�EM��\\\�sQwX�\"\r���a�,\�BT�A\'�CM��\�ocE��,\�\��R\�\�)\�D$��y҂�TZ�%B���%\�\�\�\�\0��\�\�(\�rĲ\���(b�6\�\��Q��g\�\�\�O�K\�\�/�p!#��\�u:�.�\�花N\�P9<�jM.\�H�\�\�Ay/�\�\�w�\�\�xt��\r�RF�\Z�,oI��r�t̴�\�ii-���S�	�-[PZ}�\n��-���EK\�f�*��.9\�\�#n\��\�<QO~�\��>\�^\'?�I��pP\�\�L⦟,��i��U\"�.$1\�\�\�#��\�b4a\�\�\�{,\Ze������Mt\"\��&<�J�N\�\�U�)\�\�\\J��5U8[H\�\�rv�.PS}bR\�\"��%J�FƚvŶж\��csr>qb\�D�}R��v@\��3\�He�:\�P�I�\\�\\��hE\�Yc�\�q\\U\���Ȗ\�[\Z���O��uER\�nzlGƚ\�-\�X;)M\0HB�FRi��Q��β�%\�$�9�	4k:i\�7[5\"\����\�\�)�x���h�)L�N\\y\�\�\�K�\�e��ɰ\��<\"d��ºFx�sT\�\�I�+���)f�g��<��\�!T�o\�e8R�۠t\�N\���ٮ�\�h\�\�\�ѭu�F\�K&\�8�H5\�\0�\�tr\�Qkǐɋ\��HW�a\�;��\�qU\�\�~���o�WD�}\�\�V��:���N�\�y�.\�����\�;\�O\ns�X�^Zh�\�\�\�2�M\�#\ZQR�����L��\�6m�\�Q�Gz���8�5A\���\�KK&e\�Wf�]Q4�	%>$�4-\�u��uq\�: \�\�\�O�\"�}j׬~�\�z*6��>�X��(\�г>�\�\�\�p&�kMt�R�G0�\��\�q���D\�/��4%�y�j���)����y��\�n<\�\�p���xX됸ɐ\�\��\��1��\������3�\�6�\Z�V\n�wVr�5���r\��o�k\�\�\�~\"�i疻6g\nL��+�\�v\�\�\�*��C_��ıx��Zl�%B�n���B�nǩ`s!孢\��\�U	A\�)\�Swn��і[�F�,H-5����A�\0>�\0�h9���\�g�(��t�+�(��2��\0\0<4*.L���=79n/��s��V\�U45qC\�#�/�a��\��G�\�\�V�O\�>\�Yl5m)9R���\�@�\�\r}\�\�2C�{1\r�rc�*\�QӨO`�\0=Ѭh\��~�Y\�>縯~q�R\�\���)I�;\�)&4�\�˼�\�t|*�\�Q��ܾ\��\0\�`)>\�\'\�	#\�#\'B\�s�[�\Z\�q<ܮ(\��t�0\�Zj��0��u\�\�FS��Bq�S	+8\��~Eo\�\�[蟙ba�\�\"RT��B�y�i�\�b\�\�8�J�\�1�aHk�\0&i]В�7\n�W�c�N_㎬\���\�\�)\�\�\�i��l�H\�\�Ѡ��\�\�ظ�w��\�ijB}#�mu)\��\�ҧ%\�wgci�\rI�RV5.6�j\�\�\� �\��S\�;V#<\�\��6\�I2�\�i\��,p����\�u��ȫ\�f.�:AnPe\r���E���<㓃üE^\�ۿ\�L.�\�RV*V3*�ԟ��\\=.��\\w\�ͦ�`�\�qd�ݩ&\�S��pe����^�\�WN\�Q\�y}�.���U�.MB�^�(\�\�M�\Z��ǩ\�aQ�-��\�4~�#�/�\��\�q����Y(7nU�\�\�\�h~%\'�aN��m�\�V���;|e�\�̩$\�X�g}2\�.�\�!\�&Ŭ�<h!5)@I�\�\�B\�\�\�PPD��$)`\���\0}@�\Z\�5>�5�����D��bS\�B3)iJ*�\�\no<\�u���\\��s<o^ ���Ȕ\�\�ܚ�M\��%w\���\�\�=�\�\\LԴ�*V�\�\�Ct;�]»\�d\�Χ+q�םHSZ�\�[��t�\�KiM\n�<z\�z��Z7�\Zt��:�S��l6bE��%)\�\�8Z��wՎ\��//�KT\0\�5F\��\�N\�\�p�غRIjU]�\�\�\�	��C�8\Z}$\�;�8+wˏ-#V��j�k\�aV6��\0\�#2�!�\�L5\�L۳\'�ws�\�\�ɪzS��\�.jH\�b[їj�|�EI\��\�\�)*�K��\�E�\�\�=�Z�{y�NM)@O�((�j�\�\�J���\�\Z��E�\Z��՞\�ϔ}�N�Kc<\�\�V[��r\�t͸ӧ���t�!�6/c��\nzmc�t��(�\Z��y\\�Ğ\�FujE.\�Ȅ`q\�`⳽(��c�\�M�h�&ǐ	c\�tGG�j\��)9g1JK:�5\0 �\�6\�.{�����\r�\�*�\�M�\�a	��@s��u\�Q�+^\�Ƃ\�ǌ\'J\�\�)Z}d�ا�m����9y`\�~�\�=\��!|T����6�u�\\dE�l\�\'\�RO�~p\�\rڥ��b�\�#f]%\���+x�~�\Zx���\�[\�\�t��a\�\�QZu��Y\�\�a��v\nKH�%-Ft���`�}���\'Ji\���v�\�\�.\�v�^V\�y(P\�eH��!Nr\�jT*\�\�\�Nm\�\�J��jK��Gm�\�r;#eA.��:Tz&ry��\0<̴\�\�L�f�\\\�,��x[�T\�\�e]�ot\�\�\�\�\��\0=\�\r\�\����i:S��|�Ԍjp�\0\��\n\�:b\�\Z�}�\�|7f]\nu)Q)\nR���\�T\�wt-:��\�q*\�+W�V�\�fp&\�\�\\r\�N,�U����;Vɨ�G�\�\�P�e�cM�M��\�`r�X\�	~��\"\�\�\'5v+���u9\�ŐMB3���vA*Ԡ\�\�~���r\�\�&%h�\�^`�R(�V��\rߪ\�\�0\�W��\�5�\�H\�y^�|\�}\"�`�ؤ\��#);\'s�.�#\�N6�ҡ̪�\0RT\r�\��0\�:\�\�Kf���#V��k|�\���\\U����;�]#��\�Lr��z\�\�-����b���\�O%f\�\\\��Re̾\��P����������A�\�\Z�\��k|6\Zn7�ex�؛c�M��3\�\�<\�\�R748p�\0i\�\�q=g�hwx�d<�\�\Z\�\��\0Mad٬\�Ǭ�\��\'��#�Ѹ*��sw�s�\"Nȧ�\��HKh\�iǗi�W\�QB��[�p��\�ؗ#쥀y\�5�S\�H\�R�B嚢���\�\�r\�:8�Ⲡ�+�\0�q�=��S\�U�$L�\�\�6��?����w(�9s�i6w!�#�\�KqƳK�>⏀�\�5�\�R/\�β�\Z�?7\�:\�D\��ef\�h#���MN\'I���4�wYg*EE\�M!1��әA \�|Y[�ek\�]$e\0Ń�O\�\�aui@\�By�\�FN\�\ZB�\�.ʹ���\ne�[\�x�\�o\�H\�a�\�\n=Rz\�O\�a.!?<�&a�.\����G\�\ZES]\�vuh�e\ZZ\�\��H0�i+.]W��{os\�\�NV�I[���\�\�_�A\�6t��m\�\r���\��-,U���\�\�\�\�:^�M!��\�aĶ��\� \0/D��ZS��\�\�2UeS�\�3��+�\�P\�s�U+��Q\�E%2����o\�D��1�W���k5h��͘y-��\�\�7�*\�&{j}ĉ1L���)��#Z�)��1o�U)4s�ZRT�55~U)A]!�\��\�Q\�<\�i�{:RX��հ\�\�i\'unc4#,�q\�g�:P�F	9J�(\�Bhu< �5\�I|�mO��vʯ&NK\�y\�UO`TO}�+Fjꭢ�fX���� \�t4\�p\0p�\nwwb\�ch6A�ի<ۂ[@\�I:_2�sw�\�w��\�����\r6Wd��i��uQ\�(�j7\��Z��˶\�\��,}�\�鲿fG�;T2��;\�N	�\�)�pXIN�\�\�\�\�\r��gp�\�@7u\�U�I\'q<��\�0\�U([��\�Ś\�7i.u�\n�d\�Azr�k\'ev%S4�mnI\�L��\�:�.����@�\0qP\�̦\�y>$b���\��\�S\'\�\�S,\�:G\������[�\Z\�yi\�n\�\r|\�e�\�6NEI�\�~�h\�9Л��{\�\Z�ұТܠ�\�f�|@}���_�yy��u��\�L��\�\�+;d\�ܧ\�O\�an�\�5,\�\��r1�eR�j%�\�\�\�H\�!D&�\�Ɖʎ\��kK�й�\�v�Ip\�Ʉ�\�\��[<\�}Ü3N��i\�t�}\Z\�ZS\�K�\�)}Ź6�(��Kt\�Z\�\�\�j��h/Dz:Xzt\�k�I��9TU)\'\�\�@\�\�@\�i\�7�.\"����E[�\�CgM�S\�\��SN	\�\��&OH\�=�\�*\�]���\�ied�P�\0\"ƛ�F\n-�Y�\�%0��+�\��z1)H�ѩP���tY�HE\ri�8	��m\Z\�#0�_\�!_(ڃ�D3�v�3�6V\�J@̤�R\�ik�\��\�M\�\Z�1�)��P�\���J�D��\�BO/��\�V\�b��F�r�\���.O&\�\�j���^)\�F����\�jt=E\'�bGژ\�V�\�,&�\�sf+#L��ou�\�Ei�\�\�\�\�5Q\�MݚL:X��T(z��kw\�²��\�^�γ2[;7\�\�u3r\��\'\n�=\�D�\�\'��\�\'�\���<ҹإ��H)�kXh7\�8�(ZCi$);�h�KtIW\�dS憎\�Z�%#y�ɴ5��\�;E\�]�{�3�*E�orS�\�]\�\�JG�\�\�\n�\���cˁ���ZS�P8i�\����b�\��\�{\�eL\�*JUMȭ\�\�c\�	\�i�n��E~�l��Ը��Cm��hJp�O}�\�\�B�\�ٔD�N$�\�%�v:�\Z�\�9�ؚx��BS�w�6\�jYy��նh\�U�\��0�#�Yѡ�hg�V��8������*U�z\��W�q�\�\�F�l\�p\�kNG��Ƌ�S\��l~�w+\�{���X�;�3��\��r\��\�o1\�2�\�&�;�j�oW\"t�Z\�\�Kd_�\�v�E]�\�[�aO����c\�4���\")^r�����(Ӊ\�rt��	\�ب)h\n�.rߗ�G(\�\�\�~|��=�\�FۧK\�h��%)lZ��\r\0![��8-�;�\�2b�k�,�\n%�����=ZvE�dzEjj\0\�x�I2)z��H;�<�\0��)ZT�\\(�r\"�\���\�M�tr�&\\�yr�E+B�l\�\r\�׼���i%4{uV\nƭ\�)Z\�W�\"\�S�a}ǋ�\�\�w/W�/@t�m �\�Y\0)\nI\�*�ZBҔ�j�>�)\�\\aj\�$\�\�TG�\�Ԟ�Q��Y.\���L]xуl�\�CN�\�A.%�w�*=\�\�-Q\�\"|���\'()>&\�ohe�n� \�YKT\�r\�|#�F�I\��jeq�+,�E(T\�Ε\nJ:S1�v\�S\�5g.֞VCv2S-=:\�~y]\"�F�����\�X��.��B:�\�)L\�Ka\nP���zGO0���\�b\�Je�[Aq\�%	\�a^CRxԓ�n�ج��б�\�dA�y]4\�\�RǢ�\�����FM�-ɫW?f:$3wZ�RiC\�\��i;�Y�BN���V��\�\�U�Z�����@)\�9T%\�s�D�DWp&��2��9R\�\Z�1\�`��iR&�UE�\�<G����e�ª����FJiӨ�\�!\�̭n�qiIB�l(	\�~�)SYm�:X82�G8�̸Z�Ĳ�\Z8\��\�\r\�a7%\�o/��\0b=-�\��8\�\�L@��\�C\�=˥�����i�wE�\�M���r�zC\��{s$l��5�I6=�c\�Gp�����\�iH.\��JA\�\r�\�q:�\�\�L\"\��<h�[�\��w�U\�\�t\�0��Ҟ��_c\�&仉n}��\�\�\�hB�<�\�On\�)kzj\�T[�x����d\�}���X�@\�2�\'�C}d\�5\�Y�OB�㪭4���\�\�\�S�՛��\\�\�s��\�\�`X�+q��16\�\�]���S˷.�pԎum�\�\��6\"��\��v\\��\�\�t\�3\ni+ue\�6H\nUo�ҽ�C�z65��\�k�\r\�h%�\�vq�z�\�\Z����0��\��`\�\��i\�J[Oe+\�#d��x�� Z%k����\r\�ʥU>\�,	��:R�^�\�c>�\�\�\�r \���	\0h4\",�\� �\rLW�\"@\�\�ye\�q\'\�W\�Z\��%\�u\�Ay\�#@�.\�02��\nV ��K\�H\�\�%��^G7�u\�\�O�\"eRi.E��\0̛z\�Q\�zT֕V\�\�g�\��1&?���J�*���7\����N��)W\�S<\�7+\�a�QRr:�S��H\�b:\"5\�&�\n5\�\�拟보\�e\Z$z�t�\0�a7\�\�\��B\�h�g���IB�K�\�O\�\�\�\�g\�\�h�4W6Ff1#s6�\�6�ll�>2\�C��\�\���4y�?\0�$|b�\�~V�d<\�ޥ�]�q�<\�]I�\�u��?8B�Fԇ\��/S��ܗ��\�6�Vd�MO�z��V�ՄgFq\�\�\�U��F�\�\�R�-Q\�#\'2�]�۱]	@V�}b�\�x��&��e�t \�Mb󍞆� �\�\�\�\��Mto�\�\�\�\�I\�\�:v�F�\�\�V\�\�oF�I\�\�\�\�\�S�w�\Z�*ܪ=�TTG\�þ;8|\rg\�~�\0�:ԣ(w�\�jGe�E\�W;�<8恵���\0\0\0�\nCi%�*Q\�	5�-�\"��g]o\n\�%�;���*F2�62\�\r����^\�\�7m�ʬߑe\�HRFRk[�\�\�E4\�\�]\�(UI\�k��0�I\�\�D�T���ݩưQRZnL�\�BJ�����M\0��(�n\���$ڒ\��TM`�;�\0�6!?|��Y\�o��i\�AY\��b\�9@Jh\�6<Og�iO{�̐�*!#R@�ѳvԲW\�\�-#(\0h\0�n\�\�i++�	\0C�R]#��g7v�\�\�\ra\'�v\�-��h_���e��Pˡd�U*IʦչHV\�v�,ͳ�J.\�\�\�M$ѩ\�\r\�2��\�PS_\�=U\�\�\� ���\"V\�b?��S\�ӟ��\�j�\�R\��\�l\�$R�\'E4\�|A�|J�UpF��;Y6�oHN\"��\�G�J��o���˪���Щ8�!\���\�w>���)�i\\�G\�myd�\��Q��c2� \�\�w\�\�\�\Z\��\��\�4;�+��q	�\�[\"�\�M$�(4?*�\�t)OtiDy�#6ř�u h�`;���jt\\%���	S�S��=��J��@z6�\0\��\0�\�\��)�?�\��čbss/tBm	R\�\�\��4\�hΖ\n��e���\�Q��T;�\�\Zd\�\n�\�\Z��\�\�,=:{\"\�r\ZV��i��\�i/�*�S�X�ayb,�E��X\�\�\0����U�k�ޥE\�,�(Q&\�$�\�nQ�!|2�$N&W��\�_m�+\�6��}!��3�g%e�\�a✴IUM-��F)\�\� ��\�\Z�lFbM\�Rո$|\�:�U5viJ2�\�\�[$\�Ғ�$:��(=�.�J\� �\�Q\�~\�άa٧\��5��h97R�/�\�~�F�j\��b\'^\'�RR\r��R[�\Zl��I0�tGX�i\��F�\��t\�C4�����\0@\�$A�60l6\��.\���.�\��UH߉ɭK���\�mŐ������\�qZ\�{�\�JJɥF\�~�FH�{�a�Iܥ]BJ\�\���WN7�D�\�\�RJz\�\�p튺:\�FS\�}���8\�RiR��*\�N��x��\"�\0\�2J9\�\�\���å#�T;�uy^�F\�=�g\�6{Jg\r@\�yo\�@.{!�U{3,\�be�	�\rGl0]I���:�@H��w�	7�3�A�-\�	$4\�؊\n$\n\�~#nQ�e)���u�\�]\�\�\r��vuM��\�,b�\�Fxt�\��{��\��5��\0\0��hVR�Б*B� ��\"�|��e#)��Ė�r\�Q~Є��VR��ZR\��\�CMŤkC3\�=�c)m�Н��ƀo��r��\�\�M�G�M\�SR\n�6�[\�2F\�P\r\�F�D՝��\�2�(���ʹ��G\�q�J�\Zn��7-v\\\�[%�*C�w:C�G\�H�Y=��*ٷ�[�<��Y[$6����R��Z\�\�]\�`�\�J�U	\�\n\�n��D�mvZ��\�iT�S\�n6W���=�\�D$8\0@\0�l53\r�+�\'�\�\ZR�\�\�\�Υ58ٜ\�~Ul���Pב\�Dv!8\�fG*ppvel����T�\�	4\�f\�ܩ$�\�+$\�\"�$\�y�<�U���\�G�*��v�\0\�\�$�M\�J�J�QB\�V���W\�AVe\�X���+zPq�$^)q$���%^�A��T�ؐ�\���q[\���>4��\�.\�~,\�\�^H\�H`����BE*kZRrܒ ��hPm\�%6�+��I�S\\�\ZXV�\�H3p,\�\�m��;8\�)\�\�҄�Q����\�\�X\�\�\��bno��vO\�\�I\n#�m����w-#��~\�;ھK�g\�\�\�\�J\�\�B\�\�\�wi몇 \�7n�\�\�tZ�\�3����,#�z�bO�T\�0���8�j�b��E�NS}��p��׫��\�x(\Z�\0@MR\�p�:��DdD[ CC��]lI�\0�$\08�h�\�W`��GP\�\�\�-!�d_�\��\�Ry\�\�v)\�$R-\��\0\0@\01\�3(����G�mF���kRU��q�e���S\�P\�\�GUJc�̝7i\�\�\�N\�<�Aʴ��\�\�j9F��(�\�\�$���cV�\�7-��.�y�	\�w�֒\�\\\�d�y�׏��fe��\�s\�D_\�!\�\�зR\�\�?RA�L\�V�ƴC.�_\�u�^��{��<2��\�\�$T��^aik��TC\�I�c\�)ǽ/b\�)>\��7�]��\0�w�Fnu\�\�D:��v7�/3�,~�駫\���<�+���\�U\�%�%\�\\��p��-4=cU+ĒcH\�kT���%�\�0a�e)�q\�^\�)\�);�,�D�\�fd�@�\0,I:���\���1Y&ք2\�#/\'0��ZI\'-:�\�*�A\0�\�\�E\�ٓN;2�2R�_,!��gU��\�,��裥\��,\�R}��\nU�N\�d�Dt\�*m`�F��&��\�Ld\�H{�up���|�\�#�\�RR\�J\�U\�⮵z�[��\�5�@\�u%Q\�h:��\0hB\nF�\�pŜ6Ԧ\�\�\Z�=)\�@�\r�+\�\r^\�`�����?\��qu�\0d}Gp\�{5�\�\0 \0�\0\0 �T8��HP\�G\�h\�\�\�2����3�[0�\�\\���#��!�RV�\�\'W	�D��u�Q\�\�)ḈiBW��7�\�c	Q*�@ݿ�*\�4�S),\� \ZP�\�R+9-J\�Y�hD\n�5\�\�4y��UsYI��\"ʒ�9QU׽�Թ\'(�D�HL��\'5G/�D\�\��\�ʪ7��|�\0=�\�R�F�!�+�Jis-�I2O,\�SE��\r\�\�T�{p�.��\�ԫ\\Eӕ�.���ÉHʒTb�\'ݏ�l��zW�JlJ*\�^3*M҃\�e��~�#�n�ԇ^\�SV��\'q �\�\0p�@7f���\�^&\�TT��*F���U���^,I<��\�VV\�Ty|\�謧�ɖ�����6Y-\�n\�kܟT}Ls�bܴ��~�GYj\�$&4\0@\0\0\0@\0\0̥b�HP\�@1*M;�\ZOF&�\�Iu�!H?t��}\�\�qu��KM\���݌$�ڽ���\�\�cW�1x>L�݋p��=���Ycc\��r\�\�\�df+\�v\�?H�\�S�)�<mlR�g@\�I?#7�\\���,�\�ŷ\�8�\�>��\�K�,�q\�ɓ�\�{N��\0\�+�9\"\�	\�{-,=B{T����]^e�\Z�\"d\�\��\�\�*?8�\�\�\�[\�\��<��r\��\���Uq!\�\�g0�ٻQ�\�\�\�)\Z,J}\�\�a,���1�M\�\�h\'\�#H֥\�\�?2��~miʢ�V�U�\�4��)\�\\���7��\�q�1����ˬ��\\gb\�\�O\�H\Z\�9�E\�\rqc	}��N�*�J?A\�U�p\�\��%�HH\�\0��\�f\�)h�\"	\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0��\�','SE','2313242424','amog','$2a$10$ZE61f33ck3XiUvvb5nwm0ufS/mZWskTCocEw2pfRuUbe/K.EiTAtu',NULL,NULL,NULL,'admin'),(2,'21233445329','USA',11,'yes','19-11-12','nagen@synergisticit.com',NULL,'male',_binary '�\��\�\0JFIF\0\0\0\0\0\0�\�\0�\0	\Z( \Z%!1!%)+...383,7(-.,\n\n\n\r,$ $,,,,,,,,,,,0,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,��\0\0\�\0\�\0�\�\0\0\0\0\0\0\0\0\0\0\0\0\0\0�\�\0N\0	\0\0\0\0!1AQaq���\"2�#3Br�\�CRSTb���\�\��$DUs���\�c�4��\��\�\0\Z\0\0\0\0\0\0\0\0\0\0\0\0�\�\07\0\0\0\0\0\01!AQ2Ra��Bq�\��\"#��\�3Sb��\�\0\0\0?\0\�(\0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� <\�\0� \0� \rCj�H��q��g�L#}\��vvi\�dH=H\�G\�SP��i�~\'��rI�\�}ꗈ�k$S,U1\�H��2Դ\�\\-��K%\�>��S\�H\�qT�$��e�T���Cy)\�&\�d�7x�k�\"�F\��W&��駽\�\�Ѐ \0� \0� ��GOA	��@\�\�ѫ�\��cu\'�6@r�ghq�I�\��m.�\Z]j���\�\�_�\�\�:�vc��ɿBJ\r�\r=<���U���,��g�\�\�t��:�1\rGn/4�f6\�\�ޚ\�\�L�1g\�B\�1\�񟊪\��:F\��\�ͳf�R��p��D\0NƁ#/�ƌ�<�ݢ\�]�����\�\�\�\�s\�v�>�9\�l���F�v��\��炴\�I@Dҵ�.q\rkA.q6\0rI:\0Gm:I��Χ\�\�\�i�\���Ēsqkz�\�\�\�e�3.#\nVZ�F��\�L�z�\�:�\�\�\�W���\��\�xwb\'k�O\��\�\�wg5������#\��ѣ��t_`WSS�[(��]�\�DjN��+#�Uc\�7����]�:_N\�y�\�6�^\�K>l�J0��9#9�\�Ev�\�T��\�\�\r/\"�\�}\r\�r:� \�\�\�\�n�h\�%�\�EVɣl��>7�9��4���%\�\0@\0@f���F�$pk\�=\�:�]\��\0P+\nŪ��T\�\�\�\�D}�1�\'[����<���ĸ�\�~�b\�G��\�\�K�\�\��Z\����:\��l��*[�ޖ\�ͤ�+�I#��\�Tۤ\�C\\\�Aߍ\��\\r$�ǞKf	8\�Q>>̧Z�\r>\'�üƽ�y�hp\�p�TJ./&|��\�\�|�\�\�\�B�v80�O4{�{R\��\�IsA;\����[㫟09�j�T\�\�`�=lrz�s\�f�\�\�0U\�\�\�\0@qn���\�3�́\�D\�\�U�|\�sd@�О�?޻g��جB��\�j\�p�f�@\��\�\�\'�\��ғ�͙zX7�΃\�b\"d\0]��<K_\�\�\r.�{	\��~ǁ\�T�iݬ�G�ѕ%>ѸP`\�\�Uk[�\�L\��k�\��\�0H\�\�f���->���˸#%D\���d\�2/{���y6\�*-\�?�jy�oA5�c+(\�E,\�\�߃$.�\�aw\��\�n\n\\\�ZɝQL\�@\0@\0@h9W����M���>\�;yý�#���v������NVs[p{I���y\�no�/\�Ή0*o��K��T>w�5\�{\�\�\�4\Z[��_D�Ƞ\���\�b�6\'\�\�\��\r�\�]\�`\� \�ia\�a\�\�)Bk�\'(���QE}F�{�\�o+,������tr�FJ��;�՘\�`�z1#]�����Z�\�d\�*�\�5%�\�\�oKB����\�,�p�\�F\��\�\�\��26\�	�A^\�\�\�3\��\�\�\�qM_H��\�\�\�OJߢ^��\� ��.��2O�j���Q�\���#P?\�\"1�\0\�eK\�G��]&�D�\�\�xb5\��\0�\'ڣ\�\�\"�vrF4\��Ls\�\�\�\�\�;\���\�K�$��]R\�&FX���\0�?T\�.5��ʆ]\���\�\�j\�\�VA\�<\�O	�ک\�/su�p,i��\�O&b$[�\�\rs\0�Č\��\Z�tsl\� \�v���u\��l��^\���SIFi#�N\�y\�wg\�/�\�z\�L\�@/O�K��~F��;ES�����\�G�\�wp\�\�uu^�2I�t>��FK�֘\�,�J\�\��w��\�V�4\\kQz�2\�\��\0@\0@\��z�\�\�$w\�\��/��#\�Lg\�RGI�,�7��#\�\�\0#��\�\�_U\'�o/�cy�p\�V��=��2S��\�b/͠�\'�X�\�4��q!7\���9�./q%\�q̝\�Ǌ�\�-�6M��VR��\�?^\�\�\��?\����ٜ\�2\��\\�3�\��69\����ߒ�+2E�V욊1�3�\�K�%n\�h\�g\�\�/\0�\�ܞP��\�ъ�9*��}ٳ2��\\\�CtB\�� *\0-�@F�\�b���1�i\��[B�\�8��\�r�\�/&G��qM�\�=\�3�c�!��N�v\�\�\�\��e�^\�\�\�,��2eTPs���\"�H\�C&�\��,/\�Z\�:��\nKU�>��\�J\\�\�\�n\�5\�T+7\�=���\�\�:�j;_�0l��z\�O\�\n�G�F>�$\����S*g\�\n@ \0� \0���W\�O\�ᑰ9\�@�\�͸�\�D�\�~D놷t&\0\�\�\�t\\f�ꨲ\��0bqѩ\�\�{\�h{7���\�a�a\�-\�Y�z&�=\n䤳FzX\Z\�BƤ\�a�C��:\�\�N��\� �#c\�\�3$@\�H/?�n<��\�Oj\�\��\�6\�m|�兵\��uv�\�R\�\�5�\�\0\�\�~cCn�\�&�$�օ\�\� �\"@x�H\0Z\�E\��sDu,\�~�iDF\�2HM\��=�\�B�,5����3J�\���?�FN�d��Aд\�vuD�\��\�\�(J/),�~\�\����|yM�>m>\�\��{�R\�\�j�\�z�oњ�1ƶ홮�A�KO��\��\�z>�6FK4\�5[@\"�\�u\�U8a^y͉Y\�\�\�\�v8c$�\�G\�L��pw�\��\0F\�\�$\�W�D\�(\0� \0� \��>j\�\�����\�>R.\���;\�\�V\�f&L]�T7j�00A~�?���e�?3\�\�)�\�V\�i\Z\��G7;�!\�F�T��[\�#^:wj��nBH9��\�eoY�|\Z=��yoL�A�\�\�l�RzR3F_?ޣ,RKf���5�%)n�\�\�\�3\��]`H\�\n[����\�8\�4_��\�#�Y�\0\�[T\�\�{O�\��4U\�{O�\�\�P\�<J�U\�\r�y�\Zy�i\�Or��/-�,��1o-�%\�d\�no\�e1 =k�n2(	�tn7^ƽ�Qϴ���D\�\�0���oMN\��`\�2x\�uYY;\�8\�\��[f\"S�̷��-\�uR0�Ƈ�\�7B\���\�ב\�\�B-\�-\�epQo)n�,��E)\�1\�\'\�l-\�x�\�5)V\�\�})U(,ԗі\�\r�@�\0y\�\�j[ʛoSc\��\�}}-�-��FFM\�\�&��{��\���y�\�>GbS.\0� \0��_�\�K�{Z\0\'2.m\�qRP��D%8Ǽϙvy\�\�N�\�4�H�\�\���\�\�]�,�zFmۗ$l԰\�ۀ^[y�ـɠ0x��sϢ�F\�Zތ8�/�\'�^\n\�\�Z_�9�K\�I~�\��c\r�׉$�I8�\����]�\�K$�^Gl\�9-����\�\�*L\�\0@�gbI�\�\0@2\����\�A��6\�>i�!\�\�ۂ�PH\�s����JwC\�\�\�W\�_G��S�\�=�=��r;�-S$ѹ����\��F�\�2Yŗ�I\0@qB8d�\�\Z��)F.O$FsQY��\�]$:R[L\�~2�_���A\�[\�\�E\�{�\0��y\�\�$�-\�\�\�sh����WO�y�7$�a�\�!h�(\�ב��\�bϋ5}��r�>do~�${\�\�\�\�k�)\�Kj\�5vm���,\�bB\� \0� \0� \0��tA�w�5O�:\�L~N_���K&�⾇��	4zxw\�\�\�ⷣ�A��\�(�\�\�o\0:��G��~q�_�B�\�$�F�;T\'\"97wϲ\��.\�<��\�l��\�zX|V\�ٞ�ҲB\0�\�m4\�X�\�m\�\�z�\'�qV\�S��q)�\�Z�\�q:�FlBOOP\�c�\�\�\�\�sx��2\��\�e�\�\��g���I짿���\��#\r%�vC�\�\�\�ʡ\�ֶ\�\���9\�\�\�\�決_��\�^OH߱����\���j*a�@�Z\0\�\Z�\r\��g�\�o6eXː =#L\�� <k��@x� \0� \0� \n䔺\�7��\�@P��W\�\�3@`1|5��;#��#B��\�T�E\�\\Ꚓ!l�%�zZ�zVe\�f$h\�]M�F|\�\�\�Q�	?���%�\�h��\�\���ߋ\�S�\"�3\Z��akzn��.\�\�\�u\�qy��\�S/�������tz\r\�2.���nW��\��\\sG��ī�d\��f5\��\�I6\0O 5D�\�q��l\��ES�\�_}Ǉ4L7\0v\�̯f��a�\�x�Z\�=�C\Z\��\�өh\�\�ۢbv�h\�q��\�5q�9�]\�F�\��,��\�,��LF\�a�\�t�g4�?���vX/�\�b\�\��%��/*\�\�sn�=\�e�\�V� \0@^e+΍>\�z\�Gre�a�:\�=I�ݖ_f\�Ǹ.\�\�+uL\�\�\�I\��Ts:���W\�,63\�\�\��	V*f��-TM\�\�\�\��SM�\��Wz�</\�\�g�\��\n�\�\�~UO��>\�\�l�A\�\��@v���_�c�S��\��Qg��01�\'h�?\�E��u3�C�E�\�\�͖�\�>\���EE\��uIj�P��\�Gs���G`�\�m:8�͓�%�a\'��x)�͒Ęs\�\0{ڹ�\�f6��\��-=c%̈�\�\�\�^�����2\�f#��h\�b%L��XlK�\��\�\�\�)�:@c�ZX򑧏\r\���\0\��L.!_2\�\�U�(\�\�\�{�\0\�C����ԫů\���\��\�����\�0mY\�\�A�y\r\�\�\�B\�\�NVg\�Ɍ�\�ys9l�D\�2�\��<IO��cE\'q�\�1\�1�\��q\Zvj�ߋ�K\��R�p���=\�\�D\�0w�O�T�\�e�#<<�W\�cq�M\'��;~&*=U]\�|ͺ�\r\�\�\�K\�\n�\�\�`	\�@M�q\�\�tx��wd��������;�F��U�*\�\�\�-|0\�KH�ͻc\�m%MI�\�~�#�+�6\�~I\Z\�\�\�=^^\��\�\��tp\�7�\��_��xXk&�F�t\\~&\�\�V1/\�\�0\�~G�n�\��\�3��o\��\�\���\�}�\�/�\�\�\�w\�\�P}#�v��>�\�\�ִKГO\�\�3\�&<K\�׷q�E��\�L�йWd\�\�\�h��i�O��\�T�؇�T\"^a��\�\��\0\�Q\��\�ݕ\���u�~G��\0�w�\�\�g6I\�\���?֓���\�<Ca�\�\�\n:ҴvI(�=s�޾?\�\�\�\�X=aG�9\�o\�S]%�\�\�\�\"\�� \�̔vJ\�K�N�k\�\�V���;�|\�T0�l���J\�\�[x\���;ԧ����S�f����V}M���\��\�A\�\�ش���\�Ο�\�\�\��\0�o\�S]![\�V���X*��W����|t�э}�\�\�f�5\�\�ۄ�vn?=�tdwq�j)\'q�@\�I�E�6�����b\��\�j;ב\�݀�\Zo�\�`���\�\�PT7 �ߞ�\��k\���z{��/�=Ia�\�S��ގ��RY�r\�ۅ}1~\�9�����s\�9\�L.�\�D)\�dn�O\�hs{\�\�\�6Ҍ�wq{.Q��\�rCiá�WmC�#\�>\�r\'?\0y�\�\�lUM�\��2W�q�9�V����!�!�\�:�u�\�:�D�{�2\�12��pF\���\�b�l.6\�ũ\�,&~�ֆ?ZBN�\�ޯ�9\�\�\�\�N\�t[���Տ\�\�\�\�\�t|\�\�\��\�\�\��)O\rV�\�~_�\�\�\�ё]\��\�J�\�6u��\�\�R>\��\0�}#��\0\\�7ׅ�\Z#cº?é\��K�\�zA\�\r��e\�ܳO|���n.PH\�\�E����$����M\�S\�ڡ\�ėW#\�P:\�:\�\�\�I�\�\\\�#�Q\�\�z�:\�#�W�nj\�\�w�sq\0x�VI\�Huk���۪F\�ٽ3�݁��ߗłyW�1U��\�\'?G\�\�˶ӻ\�p���GE���N��ߺ+\���D�v��\���Qׄ�\0\��\0\�\�~s/�\�LH��u1\�},r4�]� �[�g�4\�.�{_F��q\�z��ꤐ@z� <@���e���\�\';\'7\�ɑo���\��_�\��\�FQ���\�V	F\�J�U���0q|� \�Xmbu-\�_W\�\�\�l����<��\�3MqNQk<�:/G�M�:H\�wy\�m\�\�\�\�	}��/��R�rrY61��Qy�:��Od\��A�H���\�\��wY\�c�\�\�\�\�CS\�8H4��<[L�����-\�\�]�\��am}/\�D8�VP;>w�^.#��\��\�u\�E�~�\�_�ڪg�׼�\�e�[\\�=��\"\��ydS,�E��\'\�1\�\�wC\���-uK\�\�Im}�\�I7N���\�\�zxn�K|��\0\�\�vWa\�(=h\�\�&>\�\�z\�u�>\�w}\��V]��ܖ��(\�l���\�L�\��R\�\\o�e\�\�V\�&��B��@��Ë\�w��#BCM�/��;���\�T\��@\�R�Ğ�sE\�\�\�\�@]\�\�̨\�\����uc2���I�`]U�̚r\�Z�z-���\\��u�%\\މ�ю�\�j8���v�FV_�����ZA�vEq0�}&a�6�\�8\�u�\�\�h�Eb��\��h�������ɕ.Q\'#-�o�~�\�S�;|�\�\�}�3\�Tx�\'űz�d����m\�x\�%\�\��\�\��7T{��o푊Ε𐝲��\�UOQTu�\��~���eױOGQWuF1�\�\��M\r;mΦ�	\�?j\�\n\�S���̛���v\'\ZݒH4f��|�4���hт�rS|��gc^I\�gHL\�� ����\�\�y\��\�3O[\�8 #Wa�L-,lx�\�\r��BP��D\�9G�\�CvM��\�\�QL\�J\�;�\�\�e�M��\��4\�lu\�N���\�\�YP\��g�y�z\�kη���w\���P\�/&\r�\�[a6\�4�����5\�\�y��\0�\��d��\�\�_H\�]gI\�+C[y��\�\�ן?��BѯFj�:�\�\�b\�O99�\�y>\'\�����\�Tx\'����Eo���o0狊��\�%�Ng�G\�W�\���\�Jn\�П픿\�F=\�C�\�<�gz\��E_Ҫ\�i?׋��;&#�\0�\�#\��\0Jh�,��\0^/\�N\��^�u�\�rmu\0\�\�SwM�u`�\���d9�WH�lz\�4�\�\�6��lz7/�\�$E\�\�\�Oҥޣ��\�Dx����\�\�\\�\�\\�UǉN�\��l�\�\�W����m��n\�ޗ�\�\�.��q��&\�����\���\�\�\�z5�Ի\��o\�f�J�90Z�s��y\�\�z6��%\�W\�\�h|+\�}\'cжv6,�O\'=\�\\o\�e�8:��C\�\��\�E\�\�|�S��-\\���y�\"d[3H\�)\�\�ߵu%Ek����L\��J\�{c~�@�+R\��7�.�)�@\�J%�m\"�d闽X�̭ϑ�\�q�\�C\Z��&F\�\�I\��\�*��5\�\��-�+7�˙\�:.\�G\�=\�:�Q\�\�H\�4����`\�s\�\�\�NR՞�N.j0\�~ftŘ\�k QI%80��\Z\�Zl\r\�r��ӆ�a���(�����.%���\��Y\�<C���\�\�8X�=��o~��3#P:\��*N-SL��H \0������gSȃS�SnOk�(�Q|.�Q��``�g\���T^\Z\�$�6x�O�\�\�#\�i\�XJ\�\�2\�G\�\n\�3�\�j\�e��v�����膔�~�>�\�\�\n#ڧ\�d�b��\�loc\Z=�MT��w7�d�Է��vY�\\l�\�B\�L�h�\0@jJ��ޥ\�\�9$F���2���s\�bq^s�@-\\Df�eЯ�\�WE�wQc����b0@u�\\�9��{�\�)\�=\�Y.f\�\�k�}�7\�����`\����ZGg!���\�ڡ]M\�\�-�\�wFݱ�:WK+��h\�5\'�T1iE(��\n\�\�FԱ�b�H\�\�/�\�F`��(Ke\�B\�m\�\�\�K�����ˡ�g�\�#!�p?�\�]G-V\��z�b��qT\�F�e�H=�\rc�{���\���dq\�\\�M\�tc䭫�\�h*����hV,o�/�!ؗ\�5��ѷ\���=�\nKW\�\�v+�4��q\�}1���\�\�SX�_b	z\�\�K�h!w�P\��\�T\�\�=\Z+tܵ�20W\�B\��=\�O$�+ڒԝ�\�x(��4\�.��)r|��\�5\�\�\"\�C����USs]\�qQ}\�Y�\�j��\��y\��\�m\�1����\�j��K���k\�\�c\�\�\������\�4$y�|��_��\�G\�\�\�^�\�< y���\0_t;,|~̼͟\�O\�)\���\'�cQ\�s���\0������\0e\��W:�&��\�����|�\�*\����c�(�\�J|�\�\�i�\�\���\�j\��WؕG��\�#�#x����K\��\�\�\�Lxn�\��\"Me#J\�A�]�R��\�\ZH��\"�+\r�<q�x_y�\0\�~�r|�\�3ĵ�(�E�\��\r\�j��\0\�\�\�x�\�\�\"��]f�\�Н�c�\�\�a{=�Q�c�\�\�>�/�n8�\���5vng\'���Z�jGd,�կ�Ъ�3��J��\��>�\�\�6��\�=�y\0��T��~G���\�K\�y�Y\�!�\�\\\0T7y��dx8~	��\�E�[\�\�e\�a�Ś\�Ѥ��\"X\�=�j\�(\�K4yOj/&T\�ba�\�\�؝Ty\�e\�\�q\�wh�]1;�����\0+M���\�_��A\�\�5kDy0�-�\�B;!kaV�\�H�b%͑�\�K\��\�\�\�\�+O�r�Q�}�Ose/\�xi�h\�&��n>@�u��q\�e��4�m\�\�Qk��$�������{=�\��\�E\�=N�\�:rA�\�E�ʷLI)��\�\�1�\��\�y�\�]�<��w�\'Q\�:\�<�\�@\�\�z��\'6\�a���\�\�a\�\nJ��9�\�\�L\�{Nq\�$����3-��\0� *#+�v�dt�t\�@��34��|\�SFn>U\�=��U\r���]�\0\02\0\0P\�x�\�{�e����\�0FT�?U\�\�x\�ua]Mҭ\�Т\�#j߯3�b�T�.\�\�?R�\�6,\�\�\�L�yH��*@\0@�u�\���oZ�\�#��~޿\�cm\Z\���\�h�ბM�sm�\�M]\�a\�\�z�\�`\�Xxq\05\0w�\�\�wm�#M�03ڒ1\���ܠ��,Q�ZE��ꄃxn;˅\�ڣ��d�\0�Y\�ܯrrg�\�ܻ�3�Y\�{^sq�\�[KC�7��lrLF��}\��\�\�cg�~��������zA\0@�\�k\�Z��4\��]M��9(�,��\�\�\r\����9w;\��[k\�p���\�z\�����\�u<��+���0\�nߜ\�;\�\�j\�)y�/����\�)y��2\��1J=G��@\�\�\�9�c:\�\�eS���\�d`\�T�^\�*m\��M�wm����+�,m#߄���6Yݤz\�\�x�\�H��.7��|\���\�GEsl\����\�@㭤\��\0FF���\�4��\�.R�Y4��$�5�\�\�\�>`ζ\�\�\�٩�>\��\�\�T�\�e�Y?c�V�������8\�?\�TK��\�տR[yh��\�<k�\�*?�g>~�\�Z��e\�\r��c�{�\"uM�~㯗5\�\�\�-7���\0�\�y\�ua\�\�s�K�.���\�:h�y������;�ճ\�Wl�5����G`\Z+��\�{eR�f\"�\�7\��B\�^J\�\�e��<\�\�&�F:�gw4z��\�sj\\Cb<^�*�dy���^\�w��X.(弓�{�\\3|�\�3\�x4q%F\�#�%]r�[(\��ua���F��x�\�J�\'79m3ۮ\nQD�a\0@\0@G����Щ\�n%VԬ[\�?\�Jj�������Ѻ�\0\�m��ڄ�2�\�M\r^~�\�\�\�+!�\�-ǃ�>jŴ���)v\'ފ�#��O��O�Wv��3�U�\��5w��O�~\�ڳ\�\�\�W\�E�\�Q�#?{�JJs�2-\�\�FB�d\�;	}O�#0^ֶ\�@�]]\�\�\�\�g�\�dXZ\n�\0��à���\0� ̕Li\r/hs��ۍ\�Mn��(�Y%	=�a\',�t�\�Dz\��~o�\�f.1\�\�5U���ۍ\�ã����É\�O2x�>vJo9�uƵ�Ij�\0@\0@K��\�O#�\'�mԬ:��Kn\\\�:��\"��G�#\�w��2=E|��#\�o�\�ܹ�UApBJ(\�\�F\�\�֟�\�g]q|\�-9�\�_�߱K��\�\���W�A�i�\�c�]\�\��3���\n)��S~%�j\�h��\�\�W�G��\�?�;E�\�f�\�x훥?yoqp�\��\rW�����?0�\�;\�*Ko2/W/sX�\�\�Ęk\��k�\�\�o�\�\�a\�\�Q]�\�G�YJ�\0�\��/+��\�#���2\�\�<T�\0i��9~���\�!ث�.Gэk�W\�\�EN\��\�\��(<M��5��p/G\�\�;�\�*���{\�<�[���l�U�ww\��\�l���\�X�����\�\�\�8�#6�L \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� \0� ?�\�','SE','9872626255','amog','$2a$10$e2YCHDfKNn8/WVXDPEzjK.g3GJcl.yko2f4KC0ULEIeyUd3CYppH2',NULL,NULL,NULL,'nagen@synergisticit.com'),(3,'NA','12321321',32,'yes','2018-09-11','nagendra.synergisticit@gmail.com','NA','Male','','Architecture','9128282827','King jack','$2a$10$XxI6ivUh/.ts3gG0/kFIeuHIzY7ED3vMh1TAKAJ6r5YiTzgqyRhFi',NULL,'BTECT','23424','poieioe29292'),(4,'NA','JAPAN',3,'yes','2018-09-04','synergisticit2020@gmail.com','NA','Male',_binary '�\��\�\0JFIF\0\0\0\0\0\0�\�\0�\0	\Z (\"%!1!%)+...383,7(-.+\n\n\n\r,$ $,,,4/,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,��\0\0\�\0\�\0�\�\0\0\0\0\0\0\0\0\0\0\0\0\0�\�\0D\0	\0\0\0\0!1AQa\"q��2BR���\�#br\��3CS�����\�\�$s4T�\�\0\0\0\0\0\0\0\0\0\0\0\0\0�\�\04\0\0\0\0\0\0!1AQ\"2aq����\��3BR�#\��b�\�\0\0\0?\0\�0\0@\0\0\0@\0\0\0@\0\0\0@\0\0\0@\0\0\0@\0\0\0@\0\0\0@\0\0\0@\0\0\0@\0\0\0@���\0\��\��(��ˣWQ\�k�h�T{#\'Z�\�R{k%ƅJ\�I�\�5XJ��\�SEEm�{�_�~���4g�\�m�\Z�S��\�e\�\�G�;[`�\�-=����E��,\\8ܺ\�\�\�+G@�@�\�#7���\Z,E7\�`\�\�X�T9c�F���$A!\0\0\0@\0\0\0@\0\0F�\�l������D��\0\0@\0LL��U�)I-�h�����f����9�y\nHb8:�}劂\�QT\�٬�ʹ���W\�pQ�\�\�X\��\n&qه5uC�z�\�8zq\�J�In\�p�ROi&5I-��os\�H&�F\Z4q\�\�x)iI�&*\�F;�YBOdz�\�\Zw�n��´�\�`�\�-�8In�1b�\0\0zB\�MA ��4�\���\ZJm\��\�8/�\�\�\�K\rN\\=����#\�\r�I��\���q\�\��	�\�3Z�\�\�J\�!\�\�B��a9E\�١�\�I]EK\0\0@\0\0Q\�1$˶V��8�֕\'RVFuj*q�9�#���\�\�\��\�Zta\r�̝Y\�vP�L\�L\��\�uc�G\�t\��H��%�c	m��F�\�P\�R\�\�|,iME\�\�ᶎ	&2��5�\�r>+l\�\�\���l���\"�\�\�L/\�\�>\�߯�5��\�g,MG\�T\�Q��Q\�I1�Ih�[os\�I\�\�)\�)[����\�\\�ŉ\�8\�R�\�\�7r���J�����쌛��s����@.\�3�\�\�,WJ��\�T�F�ݩ|����@M[̢nVJ��x\�KVOqIc�I\�o �\�6��)�4RJ��c*ǍɇHV�\�\�́�NrD\�Z�ĸ4PW��ƺ�\�vG_\�m�K�ɎӫF��\�/�6\�N(\�\�aƕ�: �#q�\�*Fj�3��r.T \0�\0,\�O-�fmT;\�\�ȍ�IӌդZ�Ѽ���L\n��\�N�\�\�9u��\�\�R��\�8�\�\�\0\0 \0�w�X�u�袩�S\��\�\�S\�r�3O\�M\0\0@��eUk�\�i�|C�\�\'fK3E`\�@\�(��\�V\�J\�JT�=m[\nw\�]D��rT`Ģ\�\�jD:\����7��*r�T���5�`�U�\�A\�3(҉��v�\�$_=6�[[�4J\'��=\�2��m��\\���\\Ӟx�k�ѤjH\���gx��\�\�%RN+���2�E\����~􀪄Y\\@�a\�\�fԡI\�*ړKŲ��S\��uVH�\��A�Pq�\�\�w�{�q\�M�GB�ɐ�G\�8r����\�y\�|v:;\Zӳ�\0�\�a�|D:�w�\�#\�6\�ݶ�����ٖ:�x�\�\Z\�4�HU�\�d�r�B�\��e�\��\�ZK��j�ޢ��[Ǳ\�$�m\�����\0Zk|\�O��D\�b��\nu�_\�1eґn\�|\�/���g�#F\�Ֆn]\�bw�\'ƀ�S\�ӟ\�\�\�3+Ӓf�Ju$-��\'r�k���%u�1i\�\�l�{i\�Co�Aϒ���|/\�Oퟹ��x�@\0\�=���\�I>�hG4�+\'h�r�c�q��\0@\02ʔh�I�m-���N\�ʄ�ÉJ\��0J��\�)\�\nB��gba	Ϻ��\�\�\�d��N\�r�\0�>$Dd�>\�\��~E���@��\'@\ZY�\�E��4_��k\�\�E3�b�\ny�\�K?�1+?佉�\Z/m}M�\��h\�*]\�h�\��h\�4\�|Ej(�vv=\�ʘ���LˍI��RU�\�Z�����_IbT)\�ź4\�U�\r\�\0eM\�\�<��gM��\�RT%6\��\\(�Z[AJz�*��!i�T��.�bz��ŭ�m.	@�-S�\'aL\���[N�\�\��{\�S�]\�\�JR�%%�\�\�\�	[*q+	=R3W-7�\�5\��(ԕ֜\�qU!Vy��\�\�8�Q�TF9u����	�Ҝu�ص�;�A�\�Z|�\�2������z+HPWq�`�v��>��ǁ\���f��f�\�L�Բ�\0�޴Nc\�o!\�.2�e��\�\�U\���\�\�f\�n��d|\�\�|#\�Q��/J\�]7\�t\r�\�\�j\r�jٰ\'\�;��\�bp\�K4w��a븼�\�\�G0\�\0\'\�ǲʯ\�Q>&�\�\�Xު0\�;Sg:��\�\0�(�\�(\rJ�~�\���K(\�H���\�\�\�u\�숓\�\�C\�@�eħ:�u�\�PRw[�R���l�\�i�d0Z�	̣��\�<Τ��\ZB1�uJ���m���V��^\�]V嘙�B�AYI\�ݑNʹث�B�<+)(\�\�*O�!�۩DY*S�҈I��\n\��\�ӏ�G\r7��\�\'\'�m�\�˝O\�9V���\�r�])eh�z��V����+`�Hni\�Q	Ȅ\�vPI��N�\�ת\�N7~\"��C�]\�\�ٶ\�%2TЈ^+��{�\�D�U�u1kKR�\�̳~�\�\'�\����r{#XѨ���\�m\��Ԣ2\�\0A#���\�Ӝm3\�z!�`ZY�����~�ɦ���[MH\�?ED�QN\�_̕WZ1o\���\"N\�\�\n��7\r=���Y�D����\�\�{\�^����3rd�OH\�4K\�\�\�46\�U\n���|��zU\�\�\��3i\'2�[K���7²���9󋄜e�q\�%\�\�Ax2�\�2�\�=tu�A��f\�1\�\�\�L;\�r��\�yC\�\�\�s�P�h��\��:�˂\�ζ:�J�\�d6���˅gT�\�\�ʝ\' \'rQZS��\�!*���ya\�Gf0�U�yZ\�\�J1F\�\�eImԶ�͇T[^�b�.�\�\�3��JԂ�Z-2\�ZR���@x�*#֧utqZ��:f\�Ne\�M\�:��[\�H\�b!��GV��A1�`lf6�\�4\�x��\0�a\�\�7\�)�}���d��{D^��\�\�js�`��N:�\�I���Ml#9VI\�vFy��m5D��\�Mn\�\�F��\�W���Tw���\�b8Y=d\�Von�Ӭ�\n\�J~)1gJ�$�K�$�Wڙ鱐%2��K\"�G �Pw\n\�V�\�\�^hGW�sH�l�Vovu}I$\�>&y �Ujn\�Z��v逬�%Ʌ\�I��*{��jc)\�mAa�k\�\��\�&�	�l��\���\�6�Jp�\�\�VUp��\0��_�\�}��B�0\�\��Ne\��\�Q̩���\�t�\�h��\�L2\�!��7��\�}�J[�)ל�\�\�\�&�*&\�2n\�-\�k,\�	\�[�\�h�t��y�\n*�\�F_v`b[h�\�2\�x ug\��\�@�����\�J=\�/O��\�N����\�Q�\�K��%�^p\�m�\'JНkFu*=e�\�c\rV�Y7)��V\�\�\�\��ÙI)\�и�k�Q�G�X+NRw\�e��R���[��\�\�\�`-9s\�\�\�\�\�jr�SlYJ\�\�+.d�����s�\�t0�W��\�\��}�j�x}ϻM�8��\��A.������nXJ�{WJ\��ڣ�C�J-Z6)l\�*\��\�r˱\�j+�C���\�\�jF5U����u��\�8$�\0�!.2�\�U�\r\�k��0�YFVg�t�	\�z1n\�̭+nFN�3��z)\���Nþ�\�$`�\�l�lw\r�S�g����\�Y�.A�l9QPy\�W��R.\n�7\�\0��҅z�u��Oo\�w��$t\�r��8T�\�]IJH�n	�4�\��\�­_�l�\0F�nP�\�s�@\�Ξ����Р!�<\�3�\�\"\�dXͭ`Y\�H�xf��s���X\�u=�w\�\�O\�?(S\Z�I�\�\�e�Q	\��]\���\�\n\�O\�G�H[&��\�N��P�T�[U|cj�j\�\r[�9\�3���9�`�dݦ}�?x\�u@;\�R���=�/�ѥMR^#)\\0��%T�M����3�f�n\�\�-�pA�\�u�\�̝y\\\�b;@��\�I7\�\�\\WP�\�\�*�\�8�li�?ѮW%���=Iliu]$��\�\r�0HJG|�;c\�\�\�NoAZ�\�vi+/��U˜���d\0>�\�Tmݳ�:����ȧ.TSKV3r3\�\�Z@R\�.�Dj\�ՖĞK�ڼbr\�pk�u2�R�\�\���e/���p\rEo���X�|�&�Qݻ��`С�\Z�u\��;�B)�){��\ZX\�<\��\�[�\�z X�s\�ިv��੯S���\Zj����\r�\�\�=Z\�T4�\�lF\�9\�\\Qk�J�\��27\'�\�\�s�0�\�Ԗ\�\��fԞw\�\�G�ε%�H�\��\�Ve\�\'(Q\�45\�Hr��7s\�ֆT�;/����c}�\�u���\�J��s��f�$��\�)���q\�\�l!�\'xX*�m\�E7{�*\�G(\�`6�Rn�$\�]���\�$�S^̼\�8DV�\�79�\��;;�\0G\\�a�(4�\�\�17֯�ר�ʔ?\�\�8�귗W���MS��6��\�QS\���B3;\Z�JJr\�\�[�\�\�2\�/�,|��a�����zz�ַᘏ\�\n\�\�\�\�5�.RrQWe|6L2\�\ZN�HOm�\��\�\��QG\ZR\�\�6�.�\n#�P�;h��\���\�\��x�e��\'%�\�DGG��\�\�\�c;\�\�\��B�.-I�h�֩=\Z;\n�Q�#�Z�:\�\�\�\�\\,-y�Kt=\�U_p��ps\�;>$��t�K�BnN�\ro^�+V�V�e\�1\'�%��B[�M�x\�f\�9r\�63���Y\�ì\�\��F�	�l��R\�Y�Vy���8*��\�\�V\�N��\"i\�)6�\�o�\�^�%:�[p�ح�� �\��\�R�\�_)l�֥	��\�%\"m���Q\��X�2\�g\�%B���O+��,Ib��J \Z�&�\�z\"�M��\�L��u%7+\0�*4>\�Ƙh�TW\�\�\n\nu��k\�mpY7a�\�l�\0w^��\���9f�fuju�r|J��j\r�e3��h�yk�R\�;\0�\�:���\�::9!\��4IG)WH8U<c\�\�U���ǥN�N�&\�\�\��\'(݈֊������@\�	6\���D���PPxG�\�Z�,��lm\Z8�R\��f\�e��wF\�M7��%#�4�\�\n\r\�\'�qm�x\�;3:�ZanP��A\0\�<-Aؑ\�N1�ב\�\�I\�D\�\�:\"F�`\\j��S��\�\�\�u�`\�\�\�B�#l\\\�4�pBO��t���\��9���\0����W�\�L�*RT�\r\�\�B7P�q�(�\�\��dFk�PFeXR�6\�K\�4zZ\�\�\�\�8�I\�\�g$٫�g\"�!�S�G����\0�\�bsT#�Z\�\�j\�e�XKM�\0\\�\��\�Ǟ�ܵ�\�\Z�G7y;���YF\�Ɍ\�3m2iqZ�����\��(\�9r�(\�T񮔋��e���\��\�I\�Pk\��\�$�$�\�I�< \"\�\�	\�4�4���I$�\Zi�I\�J�\"�7�9�-��a\�	tgC+K�5!A�Sʕ�j\�\Z=Z�ޯO#��\�\�\�Rݦ�<N���\�&Y\"�MAI\�*\�/R�95\�J��\�(�X�:�ڵ�c9�FS�\�9+R9e\��\�\��\0��\��d>1\�q�;x���\�~F\�I�\�*\n\�2��PG6Ri�c�c�\�4Kͤ�EM��\\\�sQwX�\"\r���a�,\�BT�A\'�CM��\�ocE��,\�\��R\�\�)\�D$��y҂�TZ�%B���%\�\�\�\�\0��\�\�(\�rĲ\���(b�6\�\��Q��g\�\�\�O�K\�\�/�p!#��\�u:�.�\�花N\�P9<�jM.\�H�\�\�Ay/�\�\�w�\�\�xt��\r�RF�\Z�,oI��r�t̴�\�ii-���S�	�-[PZ}�\n��-���EK\�f�*��.9\�\�#n\��\�<QO~�\��>\�^\'?�I��pP\�\�L⦟,��i��U\"�.$1\�\�\�#��\�b4a\�\�\�{,\Ze������Mt\"\��&<�J�N\�\�U�)\�\�\\J��5U8[H\�\�rv�.PS}bR\�\"��%J�FƚvŶж\��csr>qb\�D�}R��v@\��3\�He�:\�P�I�\\�\\��hE\�Yc�\�q\\U\���Ȗ\�[\Z���O��uER\�nzlGƚ\�-\�X;)M\0HB�FRi��Q��β�%\�$�9�	4k:i\�7[5\"\����\�\�)�x���h�)L�N\\y\�\�\�K�\�e��ɰ\��<\"d��ºFx�sT\�\�I�+���)f�g��<��\�!T�o\�e8R�۠t\�N\���ٮ�\�h\�\�\�ѭu�F\�K&\�8�H5\�\0�\�tr\�Qkǐɋ\��HW�a\�;��\�qU\�\�~���o�WD�}\�\�V��:���N�\�y�.\�����\�;\�O\ns�X�^Zh�\�\�\�2�M\�#\ZQR�����L��\�6m�\�Q�Gz���8�5A\���\�KK&e\�Wf�]Q4�	%>$�4-\�u��uq\�: \�\�\�O�\"�}j׬~�\�z*6��>�X��(\�г>�\�\�\�p&�kMt�R�G0�\��\�q���D\�/��4%�y�j���)����y��\�n<\�\�p���xX됸ɐ\�\��\��1��\������3�\�6�\Z�V\n�wVr�5���r\��o�k\�\�\�~\"�i疻6g\nL��+�\�v\�\�\�*��C_��ıx��Zl�%B�n���B�nǩ`s!孢\��\�U	A\�)\�Swn��і[�F�,H-5����A�\0>�\0�h9���\�g�(��t�+�(��2��\0\0<4*.L���=79n/��s��V\�U45qC\�#�/�a��\��G�\�\�V�O\�>\�Yl5m)9R���\�@�\�\r}\�\�2C�{1\r�rc�*\�QӨO`�\0=Ѭh\��~�Y\�>縯~q�R\�\���)I�;\�)&4�\�˼�\�t|*�\�Q��ܾ\��\0\�`)>\�\'\�	#\�#\'B\�s�[�\Z\�q<ܮ(\��t�0\�Zj��0��u\�\�FS��Bq�S	+8\��~Eo\�\�[蟙ba�\�\"RT��B�y�i�\�b\�\�8�J�\�1�aHk�\0&i]В�7\n�W�c�N_㎬\���\�\�)\�\�\�i��l�H\�\�Ѡ��\�\�ظ�w��\�ijB}#�mu)\��\�ҧ%\�wgci�\rI�RV5.6�j\�\�\� �\��S\�;V#<\�\��6\�I2�\�i\��,p����\�u��ȫ\�f.�:AnPe\r���E���<㓃üE^\�ۿ\�L.�\�RV*V3*�ԟ��\\=.��\\w\�ͦ�`�\�qd�ݩ&\�S��pe����^�\�WN\�Q\�y}�.���U�.MB�^�(\�\�M�\Z��ǩ\�aQ�-��\�4~�#�/�\��\�q����Y(7nU�\�\�\�h~%\'�aN��m�\�V���;|e�\�̩$\�X�g}2\�.�\�!\�&Ŭ�<h!5)@I�\�\�B\�\�\�PPD��$)`\���\0}@�\Z\�5>�5�����D��bS\�B3)iJ*�\�\no<\�u���\\��s<o^ ���Ȕ\�\�ܚ�M\��%w\���\�\�=�\�\\LԴ�*V�\�\�Ct;�]»\�d\�Χ+q�םHSZ�\�[��t�\�KiM\n�<z\�z��Z7�\Zt��:�S��l6bE��%)\�\�8Z��wՎ\��//�KT\0\�5F\��\�N\�\�p�غRIjU]�\�\�\�	��C�8\Z}$\�;�8+wˏ-#V��j�k\�aV6��\0\�#2�!�\�L5\�L۳\'�ws�\�\�ɪzS��\�.jH\�b[їj�|�EI\��\�\�)*�K��\�E�\�\�=�Z�{y�NM)@O�((�j�\�\�J���\�\Z��E�\Z��՞\�ϔ}�N�Kc<\�\�V[��r\�t͸ӧ���t�!�6/c��\nzmc�t��(�\Z��y\\�Ğ\�FujE.\�Ȅ`q\�`⳽(��c�\�M�h�&ǐ	c\�tGG�j\��)9g1JK:�5\0 �\�6\�.{�����\r�\�*�\�M�\�a	��@s��u\�Q�+^\�Ƃ\�ǌ\'J\�\�)Z}d�ا�m����9y`\�~�\�=\��!|T����6�u�\\dE�l\�\'\�RO�~p\�\rڥ��b�\�#f]%\���+x�~�\Zx���\�[\�\�t��a\�\�QZu��Y\�\�a��v\nKH�%-Ft���`�}���\'Ji\���v�\�\�.\�v�^V\�y(P\�eH��!Nr\�jT*\�\�\�Nm\�\�J��jK��Gm�\�r;#eA.��:Tz&ry��\0<̴\�\�L�f�\\\�,��x[�T\�\�e]�ot\�\�\�\�\��\0=\�\r\�\����i:S��|�Ԍjp�\0\��\n\�:b\�\Z�}�\�|7f]\nu)Q)\nR���\�T\�wt-:��\�q*\�+W�V�\�fp&\�\�\\r\�N,�U����;Vɨ�G�\�\�P�e�cM�M��\�`r�X\�	~��\"\�\�\'5v+���u9\�ŐMB3���vA*Ԡ\�\�~���r\�\�&%h�\�^`�R(�V��\rߪ\�\�0\�W��\�5�\�H\�y^�|\�}\"�`�ؤ\��#);\'s�.�#\�N6�ҡ̪�\0RT\r�\��0\�:\�\�Kf���#V��k|�\���\\U����;�]#��\�Lr��z\�\�-����b���\�O%f\�\\\��Re̾\��P����������A�\�\Z�\��k|6\Zn7�ex�؛c�M��3\�\�<\�\�R748p�\0i\�\�q=g�hwx�d<�\�\Z\�\��\0Mad٬\�Ǭ�\��\'��#�Ѹ*��sw�s�\"Nȧ�\��HKh\�iǗi�W\�QB��[�p��\�ؗ#쥀y\�5�S\�H\�R�B嚢���\�\�r\�:8�Ⲡ�+�\0�q�=��S\�U�$L�\�\�6��?����w(�9s�i6w!�#�\�KqƳK�>⏀�\�5�\�R/\�β�\Z�?7\�:\�D\��ef\�h#���MN\'I���4�wYg*EE\�M!1��әA \�|Y[�ek\�]$e\0Ń�O\�\�aui@\�By�\�FN\�\ZB�\�.ʹ���\ne�[\�x�\�o\�H\�a�\�\n=Rz\�O\�a.!?<�&a�.\����G\�\ZES]\�vuh�e\ZZ\�\��H0�i+.]W��{os\�\�NV�I[���\�\�_�A\�6t��m\�\r���\��-,U���\�\�\�\�:^�M!��\�aĶ��\� \0/D��ZS��\�\�2UeS�\�3��+�\�P\�s�U+��Q\�E%2����o\�D��1�W���k5h��͘y-��\�\�7�*\�&{j}ĉ1L���)��#Z�)��1o�U)4s�ZRT�55~U)A]!�\��\�Q\�<\�i�{:RX��հ\�\�i\'unc4#,�q\�g�:P�F	9J�(\�Bhu< �5\�I|�mO��vʯ&NK\�y\�UO`TO}�+Fjꭢ�fX���� \�t4\�p\0p�\nwwb\�ch6A�ի<ۂ[@\�I:_2�sw�\�w��\�����\r6Wd��i��uQ\�(�j7\��Z��˶\�\��,}�\�鲿fG�;T2��;\�N	�\�)�pXIN�\�\�\�\�\r��gp�\�@7u\�U�I\'q<��\�0\�U([��\�Ś\�7i.u�\n�d\�Azr�k\'ev%S4�mnI\�L��\�:�.����@�\0qP\�̦\�y>$b���\��\�S\'\�\�S,\�:G\������[�\Z\�yi\�n\�\r|\�e�\�6NEI�\�~�h\�9Л��{\�\Z�ұТܠ�\�f�|@}���_�yy��u��\�L��\�\�+;d\�ܧ\�O\�an�\�5,\�\��r1�eR�j%�\�\�\�H\�!D&�\�Ɖʎ\��kK�й�\�v�Ip\�Ʉ�\�\��[<\�}Ü3N��i\�t�}\Z\�ZS\�K�\�)}Ź6�(��Kt\�Z\�\�\�j��h/Dz:Xzt\�k�I��9TU)\'\�\�@\�\�@\�i\�7�.\"����E[�\�CgM�S\�\��SN	\�\��&OH\�=�\�*\�]���\�ied�P�\0\"ƛ�F\n-�Y�\�%0��+�\��z1)H�ѩP���tY�HE\ri�8	��m\Z\�#0�_\�!_(ڃ�D3�v�3�6V\�J@̤�R\�ik�\��\�M\�\Z�1�)��P�\���J�D��\�BO/��\�V\�b��F�r�\���.O&\�\�j���^)\�F����\�jt=E\'�bGژ\�V�\�,&�\�sf+#L��ou�\�Ei�\�\�\�\�5Q\�MݚL:X��T(z��kw\�²��\�^�γ2[;7\�\�u3r\��\'\n�=\�D�\�\'��\�\'�\���<ҹإ��H)�kXh7\�8�(ZCi$);�h�KtIW\�dS憎\�Z�%#y�ɴ5��\�;E\�]�{�3�*E�orS�\�]\�\�JG�\�\�\n�\���cˁ���ZS�P8i�\����b�\��\�{\�eL\�*JUMȭ\�\�c\�	\�i�n��E~�l��Ը��Cm��hJp�O}�\�\�B�\�ٔD�N$�\�%�v:�\Z�\�9�ؚx��BS�w�6\�jYy��նh\�U�\��0�#�Yѡ�hg�V��8������*U�z\��W�q�\�\�F�l\�p\�kNG��Ƌ�S\��l~�w+\�{���X�;�3��\��r\��\�o1\�2�\�&�;�j�oW\"t�Z\�\�Kd_�\�v�E]�\�[�aO����c\�4���\")^r�����(Ӊ\�rt��	\�ب)h\n�.rߗ�G(\�\�\�~|��=�\�FۧK\�h��%)lZ��\r\0![��8-�;�\�2b�k�,�\n%�����=ZvE�dzEjj\0\�x�I2)z��H;�<�\0��)ZT�\\(�r\"�\���\�M�tr�&\\�yr�E+B�l\�\r\�׼���i%4{uV\nƭ\�)Z\�W�\"\�S�a}ǋ�\�\�w/W�/@t�m �\�Y\0)\nI\�*�ZBҔ�j�>�)\�\\aj\�$\�\�TG�\�Ԟ�Q��Y.\���L]xуl�\�CN�\�A.%�w�*=\�\�-Q\�\"|���\'()>&\�ohe�n� \�YKT\�r\�|#�F�I\��jeq�+,�E(T\�Ε\nJ:S1�v\�S\�5g.֞VCv2S-=:\�~y]\"�F�����\�X��.��B:�\�)L\�Ka\nP���zGO0���\�b\�Je�[Aq\�%	\�a^CRxԓ�n�ج��б�\�dA�y]4\�\�RǢ�\�����FM�-ɫW?f:$3wZ�RiC\�\��i;�Y�BN���V��\�\�U�Z�����@)\�9T%\�s�D�DWp&��2��9R\�\Z�1\�`��iR&�UE�\�<G����e�ª����FJiӨ�\�!\�̭n�qiIB�l(	\�~�)SYm�:X82�G8�̸Z�Ĳ�\Z8\��\�\r\�a7%\�o/��\0b=-�\��8\�\�L@��\�C\�=˥�����i�wE�\�M���r�zC\��{s$l��5�I6=�c\�Gp�����\�iH.\��JA\�\r�\�q:�\�\�L\"\��<h�[�\��w�U\�\�t\�0��Ҟ��_c\�&仉n}��\�\�\�hB�<�\�On\�)kzj\�T[�x����d\�}���X�@\�2�\'�C}d\�5\�Y�OB�㪭4���\�\�\�S�՛��\\�\�s��\�\�`X�+q��16\�\�]���S˷.�pԎum�\�\��6\"��\��v\\��\�\�t\�3\ni+ue\�6H\nUo�ҽ�C�z65��\�k�\r\�h%�\�vq�z�\�\Z����0��\��`\�\��i\�J[Oe+\�#d��x�� Z%k����\r\�ʥU>\�,	��:R�^�\�c>�\�\�\�r \���	\0h4\",�\� �\rLW�\"@\�\�ye\�q\'\�W\�Z\��%\�u\�Ay\�#@�.\�02��\nV ��K\�H\�\�%��^G7�u\�\�O�\"eRi.E��\0̛z\�Q\�zT֕V\�\�g�\��1&?���J�*���7\����N��)W\�S<\�7+\�a�QRr:�S��H\�b:\"5\�&�\n5\�\�拟보\�e\Z$z�t�\0�a7\�\�\��B\�h�g���IB�K�\�O\�\�\�\�g\�\�h�4W6Ff1#s6�\�6�ll�>2\�C��\�\���4y�?\0�$|b�\�~V�d<\�ޥ�]�q�<\�]I�\�u��?8B�Fԇ\��/S��ܗ��\�6�Vd�MO�z��V�ՄgFq\�\�\�U��F�\�\�R�-Q\�#\'2�]�۱]	@V�}b�\�x��&��e�t \�Mb󍞆� �\�\�\�\��Mto�\�\�\�\�I\�\�:v�F�\�\�V\�\�oF�I\�\�\�\�\�S�w�\Z�*ܪ=�TTG\�þ;8|\rg\�~�\0�:ԣ(w�\�jGe�E\�W;�<8恵���\0\0\0�\nCi%�*Q\�	5�-�\"��g]o\n\�%�;���*F2�62\�\r����^\�\�7m�ʬߑe\�HRFRk[�\�\�E4\�\�]\�(UI\�k��0�I\�\�D�T���ݩưQRZnL�\�BJ�����M\0��(�n\���$ڒ\��TM`�;�\0�6!?|��Y\�o��i\�AY\��b\�9@Jh\�6<Og�iO{�̐�*!#R@�ѳvԲW\�\�-#(\0h\0�n\�\�i++�	\0C�R]#��g7v�\�\�\ra\'�v\�-��h_���e��Pˡd�U*IʦչHV\�v�,ͳ�J.\�\�\�M$ѩ\�\r\�2��\�PS_\�=U\�\�\� ���\"V\�b?��S\�ӟ��\�j�\�R\��\�l\�$R�\'E4\�|A�|J�UpF��;Y6�oHN\"��\�G�J��o���˪���Щ8�!\���\�w>���)�i\\�G\�myd�\��Q��c2� \�\�w\�\�\�\Z\��\��\�4;�+��q	�\�[\"�\�M$�(4?*�\�t)OtiDy�#6ř�u h�`;���jt\\%���	S�S��=��J��@z6�\0\��\0�\�\��)�?�\��čbss/tBm	R\�\�\��4\�hΖ\n��e���\�Q��T;�\�\Zd\�\n�\�\Z��\�\�,=:{\"\�r\ZV��i��\�i/�*�S�X�ayb,�E��X\�\�\0����U�k�ޥE\�,�(Q&\�$�\�nQ�!|2�$N&W��\�_m�+\�6��}!��3�g%e�\�a✴IUM-��F)\�\� ��\�\Z�lFbM\�Rո$|\�:�U5viJ2�\�\�[$\�Ғ�$:��(=�.�J\� �\�Q\�~\�άa٧\��5��h97R�/�\�~�F�j\��b\'^\'�RR\r��R[�\Zl��I0�tGX�i\��F�\��t\�C4�����\0@\�$A�60l6\��.\���.�\��UH߉ɭK���\�mŐ������\�qZ\�{�\�JJɥF\�~�FH�{�a�Iܥ]BJ\�\���WN7�D�\�\�RJz\�\�p튺:\�FS\�}���8\�RiR��*\�N��x��\"�\0\�2J9\�\�\���å#�T;�uy^�F\�=�g\�6{Jg\r@\�yo\�@.{!�U{3,\�be�	�\rGl0]I���:�@H��w�	7�3�A�-\�	$4\�؊\n$\n\�~#nQ�e)���u�\�]\�\�\r��vuM��\�,b�\�Fxt�\��{��\��5��\0\0��hVR�Б*B� ��\"�|��e#)��Ė�r\�Q~Є��VR��ZR\��\�CMŤkC3\�=�c)m�Н��ƀo��r��\�\�M�G�M\�SR\n�6�[\�2F\�P\r\�F�D՝��\�2�(���ʹ��G\�q�J�\Zn��7-v\\\�[%�*C�w:C�G\�H�Y=��*ٷ�[�<��Y[$6����R��Z\�\�]\�`�\�J�U	\�\n\�n��D�mvZ��\�iT�S\�n6W���=�\�D$8\0@\0�l53\r�+�\'�\�\ZR�\�\�\�Υ58ٜ\�~Ul���Pב\�Dv!8\�fG*ppvel����T�\�	4\�f\�ܩ$�\�+$\�\"�$\�y�<�U���\�G�*��v�\0\�\�$�M\�J�J�QB\�V���W\�AVe\�X���+zPq�$^)q$���%^�A��T�ؐ�\���q[\���>4��\�.\�~,\�\�^H\�H`����BE*kZRrܒ ��hPm\�%6�+��I�S\\�\ZXV�\�H3p,\�\�m��;8\�)\�\�҄�Q����\�\�X\�\�\��bno��vO\�\�I\n#�m����w-#��~\�;ھK�g\�\�\�\�J\�\�B\�\�\�wi몇 \�7n�\�\�tZ�\�3����,#�z�bO�T\�0���8�j�b��E�NS}��p��׫��\�x(\Z�\0@MR\�p�:��DdD[ CC��]lI�\0�$\08�h�\�W`��GP\�\�\�-!�d_�\��\�Ry\�\�v)\�$R-\��\0\0@\01\�3(����G�mF���kRU��q�e���S\�P\�\�GUJc�̝7i\�\�\�N\�<�Aʴ��\�\�j9F��(�\�\�$���cV�\�7-��.�y�	\�w�֒\�\\\�d�y�׏��fe��\�s\�D_\�!\�\�зR\�\�?RA�L\�V�ƴC.�_\�u�^��{��<2��\�\�$T��^aik��TC\�I�c\�)ǽ/b\�)>\��7�]��\0�w�Fnu\�\�D:��v7�/3�,~�駫\���<�+���\�U\�%�%\�\\��p��-4=cU+ĒcH\�kT���%�\�0a�e)�q\�^\�)\�);�,�D�\�fd�@�\0,I:���\���1Y&ք2\�#/\'0��ZI\'-:�\�*�A\0�\�\�E\�ٓN;2�2R�_,!��gU��\�,��裥\��,\�R}��\nU�N\�d�Dt\�*m`�F��&��\�Ld\�H{�up���|�\�#�\�RR\�J\�U\�⮵z�[��\�5�@\�u%Q\�h:��\0hB\nF�\�pŜ6Ԧ\�\�\Z�=)\�@�\r�+\�\r^\�`�����?\��qu�\0d}Gp\�{5�\�\0 \0�\0\0 �T8��HP\�G\�h\�\�\�2����3�[0�\�\\���#��!�RV�\�\'W	�D��u�Q\�\�)ḈiBW��7�\�c	Q*�@ݿ�*\�4�S),\� \ZP�\�R+9-J\�Y�hD\n�5\�\�4y��UsYI��\"ʒ�9QU׽�Թ\'(�D�HL��\'5G/�D\�\��\�ʪ7��|�\0=�\�R�F�!�+�Jis-�I2O,\�SE��\r\�\�T�{p�.��\�ԫ\\Eӕ�.���ÉHʒTb�\'ݏ�l��zW�JlJ*\�^3*M҃\�e��~�#�n�ԇ^\�SV��\'q �\�\0p�@7f���\�^&\�TT��*F���U���^,I<��\�VV\�Ty|\�謧�ɖ�����6Y-\�n\�kܟT}Ls�bܴ��~�GYj\�$&4\0@\0\0\0@\0\0̥b�HP\�@1*M;�\ZOF&�\�Iu�!H?t��}\�\�qu��KM\���݌$�ڽ���\�\�cW�1x>L�݋p��=���Ycc\��r\�\�\�df+\�v\�?H�\�S�)�<mlR�g@\�I?#7�\\���,�\�ŷ\�8�\�>��\�K�,�q\�ɓ�\�{N��\0\�+�9\"\�	\�{-,=B{T����]^e�\Z�\"d\�\��\�\�*?8�\�\�\�[\�\��<��r\��\���Uq!\�\�g0�ٻQ�\�\�\�)\Z,J}\�\�a,���1�M\�\�h\'\�#H֥\�\�?2��~miʢ�V�U�\�4��)\�\\���7��\�q�1����ˬ��\\gb\�\�O\�H\Z\�9�E\�\rqc	}��N�*�J?A\�U�p\�\��%�HH\�\0��\�f\�)h�\"	\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0�\0\0 \0��\�','Architecture','2230039929','Jhon','$2a$10$khTIrJb81Pzdp8GeTAlNoec4Ai7QCZAt7yYOLwclvykeoC/ss/o2a',NULL,'BTECT','23424','openclose'),(5,'NA','London',29,'yes','admin','synergisticitsessionusc2@gmail.com','Test','Male','','ccc','7788778878','Kuro','$2a$10$B4glxEj7mWwWEolgFeIs9uNoMApmIE9V0IdSgtsmoRAqDNggTtw8i',NULL,'aaa','1234','test'),(6,'NA','London',33,'yes','admin','leoniiz.t@gmail.com','aaa','Male','','ddd','7788778878','Tiz','$2a$10$olr7V9Y88fckArQgw7xeMO8RmcQb9.I5InNucie5zejdtrfrWrcQK',NULL,'sss','1234','tiz');
/*!40000 ALTER TABLE `customer_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_transaction_history_tbl`
--

DROP TABLE IF EXISTS `customer_transaction_history_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `customer_transaction_history_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `date` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `fromAccountNumber` varchar(255) DEFAULT NULL,
  `loginId` varchar(255) DEFAULT NULL,
  `toAccountNumber` varchar(255) DEFAULT NULL,
  `transactionId` varchar(20) NOT NULL,
  `transactionMode` varchar(255) DEFAULT NULL,
  `transactionType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12253 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_transaction_history_tbl`
--

LOCK TABLES `customer_transaction_history_tbl` WRITE;
/*!40000 ALTER TABLE `customer_transaction_history_tbl` DISABLE KEYS */;
INSERT INTO `customer_transaction_history_tbl` VALUES (10001,1000,'2018-09-11 10:10:14','ATM-Withdrawal','0','poieioe29292','0','T292222','ATM','Withdrawal'),(10002,4000,'2018-09-25 10:13:18','ATM-Deposite','324324324324','poieioe29292','234324324','T929292','Online','credit'),(12233,4000,NULL,NULL,'324324324324',NULL,NULL,'T929292','Online','debit'),(12234,10,'2018-12-25 17:22:34','Test1','00315112233449','test','00315112233450','101','transferred',NULL),(12235,10,'2018-12-26 15:53:36','test2','00315112233449','test','00315112233450','102','transferred',NULL),(12236,10,'2018-12-29 17:50:00','Test pay later 2','00315112233449','test','00315112233450','103','transferred',NULL),(12237,10,'2019-01-01 16:35:00','Test pay later','00315112233449','test','00315112233450','104','transferred',NULL),(12238,15,'2019-01-02 18:56:19','Test transfer money back','00315112233450','tiz','00315112233449','105','transferred',NULL),(12239,20,'2019-01-03 18:56:53','Test transfer money back AGAIN!!!!','00315112233450','tiz','00315112233449','106','transferred',NULL),(12240,5,'2019-01-04 18:57:15','Test transfer money back AGAIN AND AGAIN!!!!','00315112233450','tiz','00315112233449','107','transferred',NULL),(12241,1,'2019-01-07 17:15:19','To make account balance beautiful :)','00315112233449','test','00315112233450','108','transferred',NULL),(12242,0.99,'2019-01-07 17:22:11','more beautiful!!!!!!!!!!!!!!!!!!!!!!!!!!!','00315112233450','tiz','00315112233449','109','transferred',NULL),(12243,15,'2019-01-08 21:25:16','Populate data xxxxxx!!!!!!!!!!!!!','00315112233450','tiz','00315112233449','110','transferred',NULL),(12244,5,'2019-01-08 21:25:25','Populate data yyyyyyyyyyy!!!!!!!!!!!!!!!!!!!!!!!!!!!','00315112233450','tiz','00315112233449','111','transferred',NULL),(12245,10,'2019-01-09 21:26:25','Populate data zzzzz!!!!!!!!!!!!!!!!!!!!!!','00315112233449','test','00315112233450','112','transferred',NULL),(12246,5,'2019-01-09 21:26:38','Populate data aaaaa!!!!!!!!!!!!!!!!!!!!!!!','00315112233449','test','00315112233450','113','transferred',NULL),(12247,20,'2019-01-11 21:26:47','Populate data bbbbb!!!!!!!!!!!!!!!!!!!!!!!!!!','00315112233450','tiz','00315112233449','114','transferred',NULL),(12248,1,'2019-01-12 21:26:55','Populate data !cccccc!!!!!!!!!!!!!!!!!!!!!!!!','00315112233449','test','00315112233450','115','transferred',NULL),(12249,5,'2019-01-13 21:27:01','Populate data !!!!!!!!!!dddd!!!!!!!!!!!!!!!!!!!!','00315112233450','tiz','00315112233449','116','transferred',NULL),(12250,2,'2019-01-14 21:27:13','Populate data !!!!!!!!eeeeee!!!!!!!!!!!!!!!!!!','00315112233449','test','00315112233450','117','transferred',NULL),(12251,13,'2019-01-15 21:27:24','Populate data !!!!!!!!ffffff!!!!!!!!!!!!','00315112233450','tiz','00315112233449','118','transferred',NULL),(12252,7,'2019-01-16 21:27:34','Populate data !!!!!!!!!!!!!!!!!!!!!!gggg!!!!!','00315112233449','test','00315112233450','119','transferred',NULL);
/*!40000 ALTER TABLE `customer_transaction_history_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location_tbl`
--

DROP TABLE IF EXISTS `location_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `location_tbl` (
  `cid` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_tbl`
--

LOCK TABLES `location_tbl` WRITE;
/*!40000 ALTER TABLE `location_tbl` DISABLE KEYS */;
INSERT INTO `location_tbl` VALUES (1,'London'),(2,'Los Angeles'),(3,'Boston'),(4,'Queen'),(5,'New Jersey'),(6,'New York');
/*!40000 ALTER TABLE `location_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payee_informations_tbl`
--

DROP TABLE IF EXISTS `payee_informations_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `payee_informations_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` varchar(255) DEFAULT NULL,
  `doe` datetime DEFAULT NULL,
  `dom` datetime DEFAULT NULL,
  `payeeAccountNo` varchar(30) DEFAULT NULL,
  `payeeName` varchar(100) DEFAULT NULL,
  `payeeNickName` varchar(100) DEFAULT NULL,
  `remarks` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `urn` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payee_informations_tbl`
--

LOCK TABLES `payee_informations_tbl` WRITE;
/*!40000 ALTER TABLE `payee_informations_tbl` DISABLE KEYS */;
INSERT INTO `payee_informations_tbl` VALUES (1,'test','2019-01-10 17:21:49','2019-01-10 17:21:49','00315112233450','tiz','tiz','NA','APPROVED',557664),(2,'tiz','2019-01-15 18:54:01','2019-01-15 18:54:01','00315112233449','test','test','NA','APPROVED',287073);
/*!40000 ALTER TABLE `payee_informations_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registation_links_tbl`
--

DROP TABLE IF EXISTS `registation_links_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `registation_links_tbl` (
  `lno` int(11) NOT NULL AUTO_INCREMENT,
  `comment` varchar(200) DEFAULT NULL,
  `doe` datetime DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `exphrs` int(11) NOT NULL,
  `linkexpiredate` datetime DEFAULT NULL,
  `linkurl` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`lno`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registation_links_tbl`
--

LOCK TABLES `registation_links_tbl` WRITE;
/*!40000 ALTER TABLE `registation_links_tbl` DISABLE KEYS */;
INSERT INTO `registation_links_tbl` VALUES (7,'Link is generated','2018-09-24 11:11:17','synergisticit2020@gmail.com',57,'2018-09-26 20:11:17','1537812677201-f8857238-d955-4b0f-b6d0-28be4b980e6f'),(8,'Link is generated','2019-01-06 10:59:20','synergisticitsessionusc2@gmail.com',57,'2019-01-08 19:59:20','1546801160275-1237ffda-8acd-464a-af22-7721ddbfbfae'),(9,'Link is generated','2019-01-06 11:02:22','synergisticitsessionusc2@gmail.com',57,'2019-01-08 20:02:22','1546801342383-f0fe885e-08a5-4a00-ad41-84e823581292'),(10,'Link is generated','2019-01-08 16:10:03','synergisticitsessionusc2@gmail.com',57,'2019-01-11 01:10:03','1546992602602-594e677d-6894-47b3-a6ec-d07276e81ad6'),(11,'Link is generated','2019-01-10 16:21:05','leoniiz.t@gmail.com',57,'2019-01-13 01:21:05','1547166064814-8b7edc89-dd0c-4db1-8100-75f32b99cdf3');
/*!40000 ALTER TABLE `registation_links_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reject_customer_saving_enquiry_tbl`
--

DROP TABLE IF EXISTS `reject_customer_saving_enquiry_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `reject_customer_saving_enquiry_tbl` (
  `csaid` int(11) NOT NULL AUTO_INCREMENT,
  `customerName` varchar(100) DEFAULT NULL,
  `doa` datetime DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `reason` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`csaid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reject_customer_saving_enquiry_tbl`
--

LOCK TABLES `reject_customer_saving_enquiry_tbl` WRITE;
/*!40000 ALTER TABLE `reject_customer_saving_enquiry_tbl` DISABLE KEYS */;
INSERT INTO `reject_customer_saving_enquiry_tbl` VALUES (1,'Kuro','2019-01-06 10:20:57','synergisticitsessionusc2@mail.com','London','7788778878','Just not good enough!!! Sry...'),(2,'Linda','2019-01-06 10:24:30','synergisticitsessionusc2@gmail.com','Los Angeles','2222222222','Sorry Linda for the reason of no reason.'),(3,'Trummy','2019-01-06 10:27:21','synergisticitsessionusc2@gmail.com','Boston','7766554433','Sorry Trummy, for the reason of no reason.'),(4,'Scott','2019-01-06 10:46:47','synergisticitsessionusc2@gmail.com','Queen','+11111111111','Sorry Scott, for not sorry...'),(5,'Kuro','2019-01-06 10:49:07','synergisticitsessionusc2@gmail.com','London','7788778878','Sorry for not sorry...'),(6,'Kuro','2019-01-06 10:55:00','synergisticitsessionusc2@gmail.com','London','7788778878','Sorry for not sorry...'),(7,'Kuro','2019-01-06 10:57:59','synergisticitsessionusc2@gmail.com','London','7788778878','Sorry for not sorry...');
/*!40000 ALTER TABLE `reject_customer_saving_enquiry_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scheduled_fund_transfer_tbl`
--

DROP TABLE IF EXISTS `scheduled_fund_transfer_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `scheduled_fund_transfer_tbl` (
  `cid` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` float NOT NULL,
  `date` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `fromAccountNumber` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `toAccountNumber` varchar(255) DEFAULT NULL,
  `userId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scheduled_fund_transfer_tbl`
--

LOCK TABLES `scheduled_fund_transfer_tbl` WRITE;
/*!40000 ALTER TABLE `scheduled_fund_transfer_tbl` DISABLE KEYS */;
INSERT INTO `scheduled_fund_transfer_tbl` VALUES (3,10,'2019-01-14 16:35:00','Test pay later','00315112233449','TRANSFERRED','00315112233450','test');
/*!40000 ALTER TABLE `scheduled_fund_transfer_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_id_generator_tbl`
--

DROP TABLE IF EXISTS `transaction_id_generator_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `transaction_id_generator_tbl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `transactionId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_id_generator_tbl`
--

LOCK TABLES `transaction_id_generator_tbl` WRITE;
/*!40000 ALTER TABLE `transaction_id_generator_tbl` DISABLE KEYS */;
INSERT INTO `transaction_id_generator_tbl` VALUES (1,119);
/*!40000 ALTER TABLE `transaction_id_generator_tbl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_login_tbl`
--

DROP TABLE IF EXISTS `user_login_tbl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_login_tbl` (
  `loginid` varchar(255) NOT NULL,
  `llt` datetime DEFAULT NULL,
  `locked` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `noOfAttempt` int(11) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `passwordExpire` datetime DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`loginid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_login_tbl`
--

LOCK TABLES `user_login_tbl` WRITE;
/*!40000 ALTER TABLE `user_login_tbl` DISABLE KEYS */;
INSERT INTO `user_login_tbl` VALUES ('admin',NULL,'no','amog',0,'cool',NULL,'admin'),('nagen@synergisticit.com',NULL,'no','amog',2,'cool',NULL,'employee'),('openclose',NULL,'no','Jhon',0,'test',NULL,'customer'),('poieioe29292','2018-09-24 11:47:47','no','King jack',0,'cool',NULL,'customer'),('test','2019-01-08 16:27:56','no','Kuro',1,'$2a$10$.nwoS2xFF8A0G3NrURppJOtGuhF/FiesuvKAH3dEXASEp1ZGBPzjK',NULL,'customer'),('tiz','2019-01-10 16:22:35','no','Tiz',1,'$2a$10$.nwoS2xFF8A0G3NrURppJOtGuhF/FiesuvKAH3dEXASEp1ZGBPzjK',NULL,'customer');
/*!40000 ALTER TABLE `user_login_tbl` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-29 10:58:01
