package com.desi.bank.util;

import java.sql.Timestamp;
import java.util.Date;

public class DateUtils {

	public static Timestamp getCurrentDateTime(){
		return new Timestamp(new Date().getTime());
	}
}
