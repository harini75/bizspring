package com.desi.bank.constant;

/**
 * 
 * @author nagendra
 *
 */
public enum RoleIdConstant {
	
	ROLE_ADMIN(1), ROLE_EMPLOYEE(2), ROLE_CUSTOMER(3);
	
	private int rid;

	private RoleIdConstant(int rid) {
		this.rid = rid;
	}

	public int getValue() {
		return this.rid;
	}
}
