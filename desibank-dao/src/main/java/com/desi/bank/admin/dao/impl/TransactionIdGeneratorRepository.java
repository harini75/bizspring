package com.desi.bank.admin.dao.impl;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.common.dao.entity.TransactionIdGenerator;

@Transactional(propagation=Propagation.REQUIRED)
@Repository
public interface TransactionIdGeneratorRepository extends CrudRepository<TransactionIdGenerator, Long>{
	


}
