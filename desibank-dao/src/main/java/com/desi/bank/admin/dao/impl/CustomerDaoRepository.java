package com.desi.bank.admin.dao.impl;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.common.dao.AppDaoConstant;
import com.desi.bank.common.dao.entity.Customer;

//@Transactional(propagation=Propagation.REQUIRED)
@Repository
public interface CustomerDaoRepository extends CrudRepository<Customer, Integer>{
	
	 @Query("select c from Customer c where c.login.role = ?1")
	public List<Customer> findCustomersByRole(String role);
	
	 @Query("select c from Customer c where c.approved='0'")
	 public List<Customer> listUnapprovedCustomers();
	 
	 @Query("select c from Customer c where c.login.loginid = ?1")
	 public Customer findCustomerByLoginid(String loginid);
	
	 @Query("from  Customer where approved='"+AppDaoConstant.NO_STATUS+"' OR approved='0'") 
	 public List<Customer> findPendingSavingAccountApprovalRequests();
	 
	 
	 @Query("from  Customer where   approved='"+AppDaoConstant.YES_STATUS+"'")
	 public List<Customer> findSavingApprovedAccount();
	 
	 public Customer findByEmail(String email);
	 public Customer findByMobile(String mobile);
	
	 
}
