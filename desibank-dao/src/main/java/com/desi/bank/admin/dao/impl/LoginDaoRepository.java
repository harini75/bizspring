package com.desi.bank.admin.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.common.dao.entity.Login;

@Transactional(propagation=Propagation.REQUIRED)
@Repository
public interface LoginDaoRepository  extends CrudRepository<Login, String> {
	
	 @Query("select l from Login as l where l.locked = 'no'")
	 public List<Login> listUnlockedCustomers();
	 
	 public Optional<Login> findByLoginidAndPassword(String loginid,String password);
	 
	 public Optional<Login> findByToken(String token);
	 
	 
}
