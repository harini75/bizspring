package com.desi.bank.creditcard.dao.entity;

import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="credit_card_application_tbl")
public class CreditCardApplicationEntity {
	private long id;
	private String appRef;
	private String applicationStatus;
	
	
	private String cardType;
	//personal info
	private String firstName;
	private String lastName;
	private Date dob;
	private String citizenship;
	private long phoneNumber;
	private String email;
	private long ssn;
 
	private byte[] passportPhoto;
	
	
	//housing info
	private String address;
	private long zip;
	private int monthLived;
	private String housingStatus;
	private double monthlyPayment;
	
	//income
	private String employmentStatus;
	private double annualIncome;
	
	private Date applyDate;
	
	private long cardNumber;
	
	private String otp;//one time password
	private Date otpExp;//otp expire date
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	
	
	public String getAppRef() {
		return appRef;
	}
	public void setAppRef(String appRef) {
		this.appRef = appRef;
	}
	public long getZip() {
		return zip;
	}
	public void setZip(long zip) {
		this.zip = zip;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getCitizenship() {
		return citizenship;
	}
	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}
	public long getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getSsn() {
		return ssn;
	}
	public void setSsn(long ssn) {
		this.ssn = ssn;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getMonthLived() {
		return monthLived;
	}
	public void setMonthLived(int monthLived) {
		this.monthLived = monthLived;
	}
	public String getHousingStatus() {
		return housingStatus;
	}
	public void setHousingStatus(String housingStatus) {
		this.housingStatus = housingStatus;
	}
	public double getMonthlyPayment() {
		return monthlyPayment;
	}
	public void setMonthlyPayment(double monthlyPayment) {
		this.monthlyPayment = monthlyPayment;
	}

	
	
	

	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public Date getOtpExp() {
		return otpExp;
	}
	public void setOtpExp(Date otpExp) {
		this.otpExp = otpExp;
	}
	public String getEmploymentStatus() {
		return employmentStatus;
	}
	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}
	public double getAnnualIncome() { 
		return annualIncome;
	}
	public void setAnnualIncome(double annualIncome) {
		this.annualIncome = annualIncome;
	}
	public CreditCardApplicationEntity() {
		super();
	}
	
	
	
	@Lob
	@Column(columnDefinition="LONGBLOB") 
	public byte[] getPassportPhoto() {
		return passportPhoto;
	}
	public void setPassportPhoto(byte[] passportPhoto) {
		this.passportPhoto = passportPhoto;
	}
	public Date getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}
	
	
	
	public long getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	
	
	public String getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	@Override
	public String toString() {
		return "CreditCardApplicationEntity [id=" + id + ", cardType=" + cardType + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", dob=" + dob + ", citizenship=" + citizenship + ", phoneNumber="
				+ phoneNumber + ", email=" + email + ", ssn=" + ssn
				 + ", address=" + address + ", monthLived="
				+ monthLived + ", housingStatus=" + housingStatus + ", monthlyPayment=" + monthlyPayment
				+ ", employmentStatus=" + employmentStatus + ", annualIncome=" + annualIncome + ", applyDate="
				+ applyDate + "]";
	}


	
	
	
}
