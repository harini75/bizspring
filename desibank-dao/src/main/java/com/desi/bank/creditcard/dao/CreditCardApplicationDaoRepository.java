package com.desi.bank.creditcard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.desi.bank.creditcard.dao.entity.CreditCardApplicationEntity;

@Repository
public interface CreditCardApplicationDaoRepository extends JpaRepository<CreditCardApplicationEntity, Long> {

	CreditCardApplicationEntity findByAppRef(String appRef);

	@Query("select app.passportPhoto from CreditCardApplicationEntity app where app.appRef=?1")
	byte[] findPassportPhotoByAppRef(String appRef);

	CreditCardApplicationEntity findByCardNumber(long cardNumber);
	
	@Query("select card.email from CreditCardApplicationEntity card where card.cardNumber=?1")
	public String findEmailByCardNumber(long cardNumber);

	
}
