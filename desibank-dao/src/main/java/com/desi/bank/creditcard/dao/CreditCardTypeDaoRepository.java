package com.desi.bank.creditcard.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.desi.bank.creditcard.dao.entity.CardTypeEntity;

@Repository
public interface CreditCardTypeDaoRepository extends JpaRepository<CardTypeEntity, Long> {
	List<CardTypeEntity> findCardTypeEntityByCategory(String category);
	CardTypeEntity findByCardTypeName(String cardTypeName);
	
}
