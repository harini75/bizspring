package com.desi.bank.creditcard.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.creditcard.dao.entity.CreditCardEntity;


@Transactional(propagation=Propagation.REQUIRED)
@Repository
public interface CreditCardDaoRepository   extends CrudRepository<CreditCardEntity, Long>{

	@Query(value="select c from CreditCardEntity c where c.customer.id=?1")
	public List<CreditCardEntity> findByCustomerId(int id);

	
}
