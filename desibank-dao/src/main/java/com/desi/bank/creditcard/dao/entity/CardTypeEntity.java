package com.desi.bank.creditcard.dao.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="credit_card_type_tbl")
public class CardTypeEntity {

	private long cardTypeId;
	
	private String cardTypeName;
	
	private String category;

	private Set<CreditCardEntity> creditCards;
	
	private Set<CardFeatureEntity> features;
	
	private String defaultCardNumber;
	
	private String defaultName;
	
	private String defaultExp;
	
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getCardTypeId() {
		return cardTypeId;
	}

	public void setCardTypeId(long cardTypeId) {
		this.cardTypeId = cardTypeId;
	}

	public String getCardTypeName() {
		return cardTypeName;
	}

	public void setCardTypeName(String cardTypeName) {
		this.cardTypeName = cardTypeName;
	}

	
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="cardType")
	public Set<CreditCardEntity> getCreditCards() {
		return creditCards;
	}

	public void setCreditCards(Set<CreditCardEntity> creditCards) {
		this.creditCards = creditCards;
	}
	
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name = "creditcardTypes_Features", joinColumns = {@JoinColumn(name = "cardTypeId")}, inverseJoinColumns = {@JoinColumn(name = "featureId")})
	public Set<CardFeatureEntity> getFeatures() {
		return features;
	}

	public void setFeatures(Set<CardFeatureEntity> features) {
		this.features = features;
	}

	public CardTypeEntity() {
		super();
	}

	
	
	
	public String getDefaultCardNumber() {
		return defaultCardNumber;
	}

	public void setDefaultCardNumber(String defaultCardNumber) {
		this.defaultCardNumber = defaultCardNumber;
	}

	public String getDefaultName() {
		return defaultName;
	}

	public void setDefaultName(String defaultName) {
		this.defaultName = defaultName;
	}

	public String getDefaultExp() {
		return defaultExp;
	}

	public void setDefaultExp(String defaultExp) {
		this.defaultExp = defaultExp;
	}

	@Override
	public String toString() {
		return "CardTypeEntity [cardTypeId=" + cardTypeId + ", cardTypeName=" + cardTypeName + ", category=" + category
				+ ", creditCards=" + creditCards + ", features=" + features + "]";
	}
	
}
