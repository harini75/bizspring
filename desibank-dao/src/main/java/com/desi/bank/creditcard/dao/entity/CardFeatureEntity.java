package com.desi.bank.creditcard.dao.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="credit_card_features_tbl")
public class CardFeatureEntity {

	private long featureId;
	
	private String feature;
	
	private Set<CardTypeEntity> cardTypes;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getFeatureId() {
		return featureId;
	}

	public void setFeatureId(long featureId) {
		this.featureId = featureId;
	}

	
	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

	
	@ManyToMany(cascade=CascadeType.ALL,mappedBy="features")
	public Set<CardTypeEntity> getCardTypes() {
		return cardTypes;
	}

	public void setCardTypes(Set<CardTypeEntity> cardTypes) {
		this.cardTypes = cardTypes;
	}

	public CardFeatureEntity() {
		super();
	}
	
	
}
