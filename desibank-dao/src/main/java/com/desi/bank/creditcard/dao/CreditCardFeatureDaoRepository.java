package com.desi.bank.creditcard.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.desi.bank.creditcard.dao.entity.CardFeatureEntity;
@Repository
public interface CreditCardFeatureDaoRepository extends JpaRepository<CardFeatureEntity, Long> {

}
