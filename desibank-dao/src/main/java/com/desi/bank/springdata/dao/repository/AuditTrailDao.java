package com.desi.bank.springdata.dao.repository;

import com.desi.bank.springdata.dao.entity.AuditTrailDocument;

public interface AuditTrailDao {

	public String save(AuditTrailDocument auditTrailDocument);

}
