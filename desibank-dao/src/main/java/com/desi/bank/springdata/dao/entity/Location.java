package com.desi.bank.springdata.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "location_tbl")
public class Location {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long cid;

	@Column(length = 150)
	private String name;

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Location [cid=" + cid + ", name=" + name + "]";
	}

}
