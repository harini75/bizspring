package com.desi.bank.springdata.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.desi.bank.springdata.dao.entity.Location;
import com.desi.bank.springdata.dao.entity.ScheduledFundTransfer;

@Repository("ScheduledFundTransferDaoRepository")
public interface ScheduledFundTransferDaoRepository extends JpaRepository<ScheduledFundTransfer, Long> {

	List<ScheduledFundTransfer> findByStatus(String status);
	
}
