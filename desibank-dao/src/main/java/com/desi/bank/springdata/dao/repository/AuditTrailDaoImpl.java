package com.desi.bank.springdata.dao.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.desi.bank.springdata.dao.entity.AuditTrailDocument;

/**
 * 
 * @author Nagendra
 *
 */
@Repository
public class AuditTrailDaoImpl implements AuditTrailDao {

	   @Autowired
	    private MongoTemplate mongoTemplate;
	   
	   @Override
	   public String save(AuditTrailDocument auditTrailDocument){
		   mongoTemplate.save(auditTrailDocument);
		   return "saved";
	   }
}
