package com.desi.bank.employee.dao.impl;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.employee.dao.entity.RejectSavingRequestEntity;


@Transactional(propagation=Propagation.REQUIRED)
@Repository
public interface RejectSavingRequestRepository extends CrudRepository<RejectSavingRequestEntity, Integer>{

}
