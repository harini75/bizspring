package com.desi.bank.employee.dao.impl;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.employee.dao.entity.RegistrationLinksEntity;


@Transactional(propagation=Propagation.REQUIRED)
@Repository
public interface RegistrationLinksRepository extends CrudRepository<RegistrationLinksEntity, Integer>{
		public Optional<RegistrationLinksEntity> findByLinkurl(String linkurl);
}
