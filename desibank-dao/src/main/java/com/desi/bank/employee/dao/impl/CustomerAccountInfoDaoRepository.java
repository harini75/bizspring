package com.desi.bank.employee.dao.impl;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.common.dao.entity.CustomerAccountInfo;

@Transactional(propagation=Propagation.REQUIRED)
@Repository
public interface CustomerAccountInfoDaoRepository extends CrudRepository<CustomerAccountInfo, Integer>{
	
	@Query("from CustomerAccountInfo a where a.customerId=:customerId")
	public List<CustomerAccountInfo>  getCustomerAccountsDetail(@Param("customerId") String customerId);
	
	@Query("from CustomerAccountInfo a where a.accountNumber=:accountNumber")
	public CustomerAccountInfo  findAccountByAccountNumber(@Param("accountNumber") String accountNumber);
	
	
}
