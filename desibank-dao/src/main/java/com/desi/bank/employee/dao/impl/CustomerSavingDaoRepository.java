package com.desi.bank.employee.dao.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.common.dao.AppDaoConstant;
import com.desi.bank.common.dao.entity.CustomerSavingEntity;

@Transactional(propagation=Propagation.REQUIRED)
@Repository
public interface CustomerSavingDaoRepository extends CrudRepository<CustomerSavingEntity, Integer>{

	@Query("from  CustomerSavingEntity where status='"+AppDaoConstant.PENDING_STATUS+"'")
	 public List<CustomerSavingEntity> findPendingCustomerSavingEnquiry();
	 
	 public Optional<CustomerSavingEntity> findByEmail(String email);
	 public Optional<CustomerSavingEntity> findByAppref(String appref);

}
