package com.desi.bank.common.dao.impl;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.desi.bank.common.dao.entity.AccountProcessingStepEntity;


@Repository
public interface AccountProcessingStepRepository extends CrudRepository<AccountProcessingStepEntity, Integer>{
	

}