package com.desi.bank.common.dao.impl;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.desi.bank.common.dao.entity.SalutationEntity;


@Repository
public interface SalutationRepository extends CrudRepository<SalutationEntity, Integer>{
	

}