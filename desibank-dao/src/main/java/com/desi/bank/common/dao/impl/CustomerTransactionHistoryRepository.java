package com.desi.bank.common.dao.impl;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.common.dao.entity.CustomerTransactionHistory;



@Transactional(propagation=Propagation.REQUIRED)
@Repository
public interface CustomerTransactionHistoryRepository extends CrudRepository<CustomerTransactionHistory, Long>{
	 
	public List<CustomerTransactionHistory> findByLoginId(String loginId);
	public List<CustomerTransactionHistory> findByFromAccountNumber(String fromAccountNumber);
	public List<CustomerTransactionHistory> findTop10ByLoginIdOrderByDateDesc(String loginId);
	public List<CustomerTransactionHistory> findByFromAccountNumberOrToAccountNumber(String accountNo,String accountNo2);
}
