package com.desi.bank.common.dao.impl;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.desi.bank.common.dao.entity.RoleEntity;


@Transactional(propagation=Propagation.REQUIRED)
@Repository
public interface RoleRepository extends CrudRepository<RoleEntity, Integer>{

}