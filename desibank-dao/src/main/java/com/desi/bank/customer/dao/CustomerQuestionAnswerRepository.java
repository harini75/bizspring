package com.desi.bank.customer.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.desi.bank.common.dao.entity.CustomerQuestionAnswer;

/**
 * 
 * @author nagendra
 * This is Repository for CustomerQuestionAnswer
 *
 */
public interface CustomerQuestionAnswerRepository extends JpaRepository<CustomerQuestionAnswer, Integer>{
	
	@Query("from CustomerQuestionAnswer cq where cq.login.loginid=?1")
	public List<CustomerQuestionAnswer> findCustomerQuestionsByUserid(String loginid);
	
}
