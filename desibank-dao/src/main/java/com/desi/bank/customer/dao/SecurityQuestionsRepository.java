package com.desi.bank.customer.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.desi.bank.common.dao.entity.CustomerQuestionAnswer;
import com.desi.bank.common.dao.entity.SecurityQuestions;

public interface SecurityQuestionsRepository extends JpaRepository<SecurityQuestions, Integer>{
	public List<SecurityQuestions> findByQidGreaterThan(int qId);
	public List<SecurityQuestions> findByQidLessThan(int qId);
}
